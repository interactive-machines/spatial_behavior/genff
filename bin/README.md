
# Usage of Helper Scripts

Here are various scripts that are useful for generate, augmenting, and plotting data. 

- **clear_notebooks.sh**: Script to clear the output from Jupyter Notebooks; run in notebooks directory
```bash
../bin/clear_notebooks.sh
```

- **make_cocktail_data.py**: Script that makes the cocktail party dataset for training. Run make_cocktail_data.py with the following command line arguments:
  - --output-file: output data file path in .h5 format (dflt = "data/cocktail_party/output_data.h5")
  - --groups-file: input data file path in .txt format (dflt = "data/cocktail_party/groups_train.txt")
  - --features-file: features data file path in .txt format (dflt = "data/cocktail_party/features.txt")
  - --env-file: environment map file path in .txt format (dflt = "data/cocktail_party/env_cocktail_party.txt")
  - --crop-radius: radius for cropping environment images (dflt = 3.0)
  - --seed: seed for random data generation (dflt = 1)  
ex: 
```bash
python bin/make_cocktail_data --output-file data/cocktail_party/cocktail_party_train.h5 --groups-file data/cocktail_party/groups_train.txt --features-file data/cocktail_party/features.txt --env-file data/cocktail_party/env_cocktail_party.txt
```

- **make_igibson_env_maps.py**: Script to make environment maps for interactive environments from iGibson. Run make_igibson_env_maps.py with the following command line arguments:
  - --ig-dataset: path to igibson dataset folder (dflt = data/igibson_maps in this repository)
  - --output-dir: output-directory (dflt = data/igibson_json in this repository)
  - -v, --verbose: print info?
  - -p, --plot: plot maps as they are being generated?
  - --resolution: resolution for output map (in meters) (dflt = 0.25)  
ex: 
```bash
python make_igibson_env_maps.py --ig-dataset ../data/igibson_maps/ -v --output-dir ../data/igibson_json/
```

- **make_sim_data.py**: Script to generate simulated data for training, in the same format as the Cocktail Party dataset.
  - --output-file: output data file path in .h5 format (dflt = "data/simulated/output_data.h5")
  - --env-file: environment map file path (dflt = "")
  - -n, --num-examples: number of samples (dflt = 1000)
  - --people_lo: Min group size (dflt = 2)
  - --people_hi: Max group size (dflt = 6)
  - --seed: Seed for random data generation (default = -1)
  - --personal-space: min personal space in meters when generating groups (dflt = 1.0)
  - --crop-radius: radius for cropping environment images (dflt = 3.0)
  - --r-range: Radius range in meters for generating groups (default = [0.8, 1.5])  
ex: 
```bash
python bin/make_sim_data.py -n 200 --seed 1 --env-file data/cocktail_party/env_cocktail_party.txt --output-file data/simulated/simulated_output_data.h5
```

- **make_sim_data_on_igibson_env.sh**: Script that runs make_sim_data.py on the entire igibson_json directory.
```bash
make_sim_data_on_igibson_env.sh
```

- **split_cocktailparty_groups.py**: Script to split the Cocktail Party groups dataset into training and testing sets. Run split_cocktailparty_groups with the following command line arguments:
  - -g, --groups-file: groups file path
  - -o, --output-dir: output directory (default: same as groups file)
  - -v, --verbose: Verbose output?   
ex:
```bash 
python bin/split_cocktailparty_groups -g data/cocktail_party/groups.txt -o data/cocktail_party -v
```

- **warp_data.py**: Script that warps data in h5 format. Run on a single file with the following command line arguments: 
  - -i, --input-h5: input path to h5 file
  - -o, --output-h5: output path (dflt: input-h5 basename with _warped.h5 extension)
  - -v, --verbose: print info?
  - -p, --plot: plot maps as they are being generated? 
  - --no-dyads: skip dyads when making warped samples  
ex:
```bash
python bin/warp_data.py -i data/cocktail_party/cocktail_party_test.h5  --no-dyads --plot
```

- **train_igibson_gan.sh**: Script to train 2 GANs, one on iGibson data (original) and one on warped data
```bash
bin/train_igibson_gan.sh
```

- **visualize_env_json.py**: Tool to visualize environment map in json format; pass a json or json-like env map to plot it.  
ex:
```bash
python bin/visualize_env_json.py data/cocktail_party/env_cocktail_party.txt 
```

- **print_file_skipping_lines.sh**: Used in [rule_based_eval_train.ipynb](https://gitlab.com/interactive-machines/spatial_behavior/genff/-/blob/master/notebooks/rule_based_eval_train.ipynb) to make evaluation faster, using only every third group in the training file for evaluation:
```bash
$ cd ../bin
$ ./print_file_skipping_lines.sh ../data/cocktail_party/groups_train.txt > ../data/cocktail_party/groups_train_skip3.txt
```

- **make_generated_data.py**: Script to make simulated data in the same format as for the Cocktail Party dataset
  - --output-file: output data file path in .h5 format (dflt = "data/simulated/output_data.h5")
  - --env-file: environment map file path (dflt = "")
  - -n, --num-examples: number of samples (dflt = 100)
  - --people_lo: Min group size (dflt = 2)
  - --people_hi: Max group size (dflt = 6)
  - --seed: Seed for random data generation (default = -1)
  - --num-generated: number of generated people (dflt = 2)
  - --model-path: models logfile (Format: <datetime>-<iteration>.pt)
  - --personal-space: min personal space in meters when generating groups (dflt = 1.0)
  - --crop-radius: radius for cropping environment images (dflt = 3.0)
  - --r-range: Radius range in meters for generating groups (default = [0.8, 1.5])  
ex:
```bash
python bin/make_generated_data.py --output-file data/simulated/output_data.h5 --env-file data/cocktail_party/env_cocktail_party.txt --num-generated 6 --model-path 20210412_192707-176.pt 
```
