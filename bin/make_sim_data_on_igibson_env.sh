NUM_GROUPS=2                               # number of groups to generate per environment
JSON_FOLDER=../data/igibson_json/*.txt     # input directory with iGibson json maps
OUT_PREFIX=../data/h5/env_                 # output prefix for the generated h5 datasets

# make output directory if it does not exist
OUT_DIR=`dirname ${OUT_PREFIX}`
mkdir -p ${OUT_DIR}

# generate data
for i in ${JSON_FOLDER}; do
  name=`basename $i .txt`
  echo "Processing ${name}..."
  python3 make_sim_data.py -n ${NUM_GROUPS} --seed 1 --env-file $i --output-file ${OUT_PREFIX}${name}.h5
done