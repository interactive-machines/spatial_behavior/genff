# Script to make simulated data in the same format as for the Cocktail Party dataset
import numpy as np
import os
import cv2
import sys
import time
import math
import json
from typing import Tuple
import argparse
import torch
import matplotlib.pyplot as plt
sys.path.append("..")
from genff.gan_method.networks import GenNet
from genff.data_handling.transform import RectangularAngle
from genff.visualization.plotting import plot_sample_rect


def generate_groups(generator: torch.nn.Module, num_generated: int, group_data: list,
                    env_data: dict, crop_radius: float, x_index: int = 0, y_index: int = 1,
                    theta_index: int = 2, cos_index: int = 2, sin_index: int = 3) -> Tuple[list, list]:
    """
    Helper function to make centered data using generator
    :param generator: generator network
    :param num_generated: number of additional generated people in final group
    :param group_data: 2D list of x,y,theta data for people in group
    :param env_data: env dictionary
    :param crop_radius: float radius for cropping env around group
    """
    generator.eval()
    transform = RectangularAngle(angle_index=2)

    with torch.no_grad():
        # Get number of pre-existing people in context
        context_people = len(group_data)

        # Preserve x,y,theta for real coordinates
        real_context = torch.zeros(1,context_people,3)
        real_context[:,:,:] = torch.tensor(group_data)
        real_center = torch.mean(real_context, dim=1)

        # Calculate centered data
        centered_context = torch.zeros(1,context_people,3)
        centered_context[:,:,theta_index] = real_context[:,:,theta_index]
        centered_context[:,:,[x_index, y_index]] = real_context[:,:,[x_index, y_index]] - \
                                                   real_center[:, [x_index,y_index]]
        centered_mask = torch.ones(1,context_people)
        centered_env = torch.tensor(crop_env(real_context.squeeze(0).numpy(), env_data, crop_radius)).unsqueeze(0)

        groups = []
        env_images = []
        for i in range(num_generated):
            # Generate sample using centered data
            _, context, mask, env = transform((centered_context, centered_context, centered_mask, centered_env))
            new_ind = generator(context, mask, env)

            dist_samples = []
            for j in range(20):
                dist_samples.append(generator(context, mask, env))

            dist_tensor = torch.cat(dist_samples)

            fig = plt.figure()
            plot_sample_rect(dist_tensor.squeeze(1), context.squeeze(0), mask.squeeze(0), env_cropped=env[0],
                             env_res=env_data["resolution"])
            plt.gca().set_aspect('equal')
            fig.savefig("../images/{}_before_center.png".format(timestamp))

            # Convert from 4-feature generator output to 3-feature for final data output
            num_features = context.shape[-1]
            centered_samples = torch.zeros((1, 1, num_features-1), device="cpu")
            centered_samples[:, 0, [x_index,y_index]] = new_ind[:, 0, [x_index,y_index]].cpu()
            centered_samples[:, 0, [theta_index]] = np.arctan2(new_ind[:, 0, [sin_index]].cpu(),
                                                               new_ind[:, 0, [cos_index]].cpu())

            # Calculate real coords in world
            real_center = torch.mean(real_context, dim=1)
            real_samples = centered_samples.clone()
            real_samples[:, 0, [x_index,y_index]] = real_samples[:, 0, [x_index,y_index]] + \
                                                    real_center[:, [x_index,y_index]]
            real_context = torch.cat((real_context, real_samples), dim=1)

            # Re-center centered data given new data point
            real_center = torch.mean(real_context, dim=1)
            # centered_context = torch.cat((centered_context, centered_samples), dim=1)
            centered_context = real_context.clone()
            centered_context[:, :, [x_index, y_index]] = real_context[:, :, [x_index, y_index]] - \
                                                         real_center[:, [x_index,y_index]]
            centered_mask = torch.cat((mask, torch.ones(1, 1)), dim=1)
            centered_env = crop_env(real_context.clone().squeeze(0).numpy(), env_data, crop_radius)
            env_images.append(centered_env)
            centered_env = torch.tensor(centered_env).unsqueeze(0)
            groups.append(centered_context.clone().squeeze(0).numpy())
            _, plot_context, plot_mask, plot_env = transform((centered_context, centered_context, centered_mask, centered_env))
            fig = plt.figure()
            plot_sample_rect(None, plot_context.squeeze(0), plot_mask.squeeze(0), env_cropped=plot_env[0],
                             env_res=env_data["resolution"])
            plt.gca().set_aspect('equal')
            fig.savefig("../images/{}.png".format(timestamp))


        return groups, np.array(env_images)


def centered_group(num_people: int, r_range: Tuple[int, int], personal_space: float,
                   x_index: int = 0, y_index: int = 1) -> np.ndarray:
    """
    Helper function to make centered group features.
    The data is generated first defining a circle around (0,0) and then placing people around it.
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    :return: Px3 numpy array where P in [people_lo, people_hi] and the columns are x, y, theta
    :note Example value for personal space: https://en.wikipedia.org/wiki/Proxemics#/media/File:Personal_Space.svg
    """
    # step 1. sample radius for all examples
    r = np.random.uniform(low=r_range[0], high=r_range[1])

    # step 2. sample number of people per group (we add 1 to high limit because randint does not include it)
    # num_people = np.random.randint(low=people_lo, high=people_hi+1)

    # step 3. generate angles for the people in the group
    min_ang = np.arcsin(personal_space*0.5 / r) * 2.0
    offset = np.random.uniform(low=0.0, high=2*np.pi/num_people)
    max_ang = (2*np.pi-offset) / num_people
    angles = np.random.uniform(low=min_ang, high=max_ang, size=(num_people, 1))
    angles[0, 0] += offset  # add offset to the first angle to reduce bias towards zero
    angles = np.cumsum(angles, axis=0)
    np.random.shuffle(angles)

    # step 4. create position (x,y) values and angle towards the center (0,0)
    x = np.cos(angles) * r
    y = np.sin(angles) * r
    theta = np.arctan2(y, x) + np.pi

    data = np.concatenate((x, y, theta), axis=1)
    if data.shape[0] == 1:
        center = np.mean(data, axis=0).reshape(1,3) # get center of context
    else:
        center = np.mean(data[1:], axis=0).reshape(1,3)
    data[:, [x_index, y_index]] = data[:, [x_index, y_index]] - center[:, [x_index, y_index]] # center to context

    assert data.shape[0] == num_people
    assert data.shape[1] == 3

    return data


def load_environment(environment_file: str) -> dict:
    """
    Function that loads the environment annotations from disk
    :param environment_file: path to environment file
    """
    with open(environment_file, 'r') as fid:
        env_data = fid.read()
        environment = json.loads(env_data)
        environment['grid'] = np.asarray(environment['grid'])
        return environment


def env_group(people_lo: int, people_hi: int, r_range: Tuple[float, float],
              personal_space: float, env: dict, crop_radius: float, x_index: int = 0,
              y_index: int = 1) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

    """
    Helper function to make centered group features given environment constraints
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    :param env: dict containing env data
    :param crop_radius: float radius for cropping env around group
    :param x_index: index containing x data in dataset
    :param y_index: index containing y data in dataset
    """
    res = env["resolution"]
    left = env["left"]
    top = env["top"]
    grid = np.fliplr(env["grid"])  # flip to make it easier to reason about x

    # range based on grid limits
    Xrange = [left, left + grid.shape[1]*res]
    Yrange = [top, top + grid.shape[0]*res]

    def world_to_grid(pt: np.ndarray, x_index: int = 0, y_index: int = 1):
        x1 = (np.floor((pt[:, x_index] - left)/res))
        y1 = (np.floor((pt[:, y_index] - top)/res))
        x2 = (np.ceil((pt[:, x_index] - left)/res))
        y2 = (np.ceil((pt[:, y_index] - top)/res))

        # all 4 pixels around pt
        xx = np.concatenate((x1, x1, x2, x2), axis=0)
        yy = np.concatenate((y1, y2, y1, y2), axis=0)

        return np.column_stack((xx, yy)).astype(int)

    def get_grid_value(pt_world: np.ndarray, grid_data: np.ndarray):
        pt = world_to_grid(pt_world)
        values = []
        for i in range(pt.shape[0]):
            c = pt[i, 0]
            r = pt[i, 1]

            if c < 0 or c >= grid_data.shape[1]:  # x-coordinate
                values.append(-1)  # outside grid
            elif r < 0 or r >= grid_data.shape[0]:  # y-coordinate
                values.append(-1)  # outside grid
            else:
                values.append(grid_data[r, c])
        return values

    def compute_midpoints(xy: np.ndarray, center: np.ndarray):
        n = xy.shape[0]
        previous_center = np.mean(xy, axis=0)
        xy = xy - np.repeat(previous_center, n, axis=0).reshape(n,2) + np.repeat(center, n, axis=0)
        center_repeated = np.repeat(center, n, axis=0)
        vecs = xy - center_repeated
        vecs_norm = np.linalg.norm(vecs, axis=1)
        vecs_norm = np.repeat(np.expand_dims(vecs_norm, axis=1), 2, axis=1)
        vecs = np.divide(vecs, vecs_norm)
        vecs1 = center_repeated + np.multiply(vecs, vecs_norm) * 0.75
        vecs2 = center_repeated + np.multiply(vecs, vecs_norm) * 0.5
        vecs3 = center_repeated + np.multiply(vecs, vecs_norm) * 0.25
        out = np.vstack((vecs1, vecs2, vecs3))
        if n == 1:
            out = xy
        return out

    # generate groups until one of them works out in the environment
    while True:
        res = env["resolution"]
        left = env["left"]
        top = env["top"]
        grid = np.fliplr(env["grid"])  # flip to make it easier to reason about x

        x = np.random.uniform(low=Xrange[0], high=Xrange[-1])
        y = np.random.uniform(low=Yrange[0], high=Yrange[-1])
        num_people = np.random.randint(low=people_lo, high=people_hi+1)

        if num_people < 2:
            group = centered_group(3, r_range, personal_space)
        else:
            group = centered_group(num_people, r_range, personal_space)

        adjusted_center = np.repeat(np.array([[x,y,0]]), group.shape[0], axis=0)
        adjusted_group = group + adjusted_center
        center = np.array([[x,y]])
        assert center.shape[0] == 1, \
            "Expected the center to have 1 row (got {})".format(center.shape[0])
        assert center.shape[1] == 2, \
            "Expected the center to have 2 columns (got {})".format(center.shape[1])

        # check center and midpoints from center to people
        xy = adjusted_group[:, [x_index, y_index]]
        midpoints = compute_midpoints(xy, center)
        values = get_grid_value(np.vstack((center, midpoints)), grid)

        # check tall obstacle
        all_good = True
        for val in values:
            if val < 0 or val > 0:  # non-free cell
                all_good = False
                break
        if not all_good:
            continue

        # check people
        people_values = get_grid_value(adjusted_group[:, [x_index, y_index]], grid)
        for val in people_values:
            if val < 0 or val > 0:  # occupied or invalid cell
                all_good = False
                break

        # # Double check the cropping didn't mess anything up
        # if num_people < 2:
        #     env_data = crop_env(adjusted_group[0], env, crop_radius) # Crop with respect only person
        # else:
        #     env_data = crop_env(adjusted_group[1:], env, crop_radius) # Crop with respect to context only
        env_data = crop_env(adjusted_group, env, crop_radius) # Crop with respect to context only
        left = -env_data.shape[0] / 2.0 * res
        top = -env_data.shape[0] / 2.0 * res
        cropped_values = get_grid_value(group[:, [x_index, y_index]], env_data[:,:,1])
        for val in cropped_values:
            if val < 1 or val > 1:  # occupied or invalid cell
                all_good = False
                break

        # if we got this far, we are done
        if all_good:
            break

    if num_people < 2:
        adjusted_group = adjusted_group[:num_people]
        env_data = crop_env(adjusted_group, env, crop_radius)

    return adjusted_group, env_data

def crop_env(group: np.ndarray, env: dict, crop_radius: float = 3.0, x_index: int = 0, y_index: int = 1) -> np.ndarray:
    """
    Helper function to crop the environment map relative to a group's context
    :param group: group tensor
    :param env: dict containing environment data
    :param crop_radius: float radius for cropping env around group
    :param x_index: index containing x data in dataset
    :param y_index: index containing y data in dataset
    :return: cropped map
    """
    resolution = env["resolution"]
    left = env["left"]
    top = env["top"]
    grid = np.fliplr(env["grid"])

    group_center = np.mean(group[:, [x_index, y_index]], axis=0, keepdims=True)
    assert group_center.shape[0] == 1, \
        "Expected group center to have 1 row. Got shape: {}".format(group_center.shape)
    assert group_center.shape[1] == 2, \
        "Expected group center to have 2 cols. Got shape: {}".format(group_center.shape)

    r = int(round(crop_radius / resolution))  # cropped map radius in pixels

    cx = (group_center[0, 0] - left) / resolution
    cy = (group_center[0, 1] - top) / resolution

    x1 = ((group_center[0, 0] - crop_radius) - left) / resolution
    x2 = ((group_center[0, 0] + crop_radius) - left) / resolution
    y1 = ((group_center[0, 1] - crop_radius) - top) / resolution
    y2 = ((group_center[0, 1] + crop_radius) - top) / resolution

    x1i = int(math.floor(x1))
    x2i = int(math.ceil(x2))
    y1i = int(math.floor(y1))
    y2i = int(math.ceil(y2))

    border_top = max(y1i, 0) - y1i
    border_bottom = y2i - min(y2i, grid.shape[0])
    border_left = max(x1i, 0) - x1i
    border_right = x2i - min(x2i, grid.shape[1])

    # separate grid into multiple channels
    grid = np.int8(grid)
    multichannel_grid = np.zeros((grid.shape[0], grid.shape[1], 3), dtype=grid.dtype)
    multichannel_grid[:, :, 0] = np.where(grid == 0, np.ones(grid.shape), np.zeros(grid.shape))  # free space
    multichannel_grid[:, :, 1] = np.where(grid == 1, np.ones(grid.shape), np.zeros(grid.shape))  # tall obstacles
    multichannel_grid[:, :, 2] = np.where(grid == 2, np.ones(grid.shape), np.zeros(grid.shape))  # short obstacles

    # add border to grid if necessary for cropping
    if border_top > 0 or border_bottom > 0 or border_left > 0 or border_right > 0:
        expanded = cv2.copyMakeBorder(multichannel_grid, border_top, border_bottom, border_left, border_right,
                                      cv2.BORDER_CONSTANT, 0)
    else:
        expanded = multichannel_grid

    # crop
    expanded = expanded.astype('float32')
    cropped = cv2.getRectSubPix(expanded, (2*r, 2*r), (cx + border_left, cy + border_top))

    # separate each layer into 1/0 channels
    # we would create 4 channels: unknown, free, occupied rigid, occupied movable
    im = np.zeros((2*r, 2*r, 4), dtype=np.float32)  # height (y), width (x), channels

    # fill in unknown
    im[:, :, 0] = 1
    im[border_top:2*r-border_bottom, border_left:2*r-border_right, 0] = 0

    # fill in other channels (free, tall obs, short obs)
    im[:, :, 1:] = cropped

    return im


def make_data_with_env(n: int, generator: torch.nn.Module, output_file: str, env_file: str, npz_file: str,
                       crop_radius: float, people_lo: int = 2, people_hi: int = 7, num_generated: int = 2,
                       r_range: Tuple[float, float] = (0.8, 1.5), personal_space: float = 1.2,
                       x_index: int = 0, y_index: int = 1) -> None:
    """
    Make simulated data with 2D position only. Outputs "features.txt" and "groups.txt" files to the given folder
    :param n: number of samples
    :param generator: loaded network generator
    :param groups_file: string containing relative path of saved groups data file
    :param env_file: string containing relative path of environment file
    :param npz_file: string containing relative path of saved npz file
    :param crop_radius: float for how wide env images should be cropped
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param num_generated: number of generated people
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    """
    env = load_environment(env_file)
    groups = []
    env_images = []

    for i in range(n):
        group_data, env_data = env_group(people_lo, people_hi, r_range,
                                         personal_space, env, crop_radius)
        generated_groups, generated_envs = generate_groups(generator, num_generated, group_data, env, crop_radius)
        for j, generated_group in enumerate(generated_groups):
            group_people = generated_group.shape[0]
            env_images.append(generated_envs[j])
            group = "%05d%05d %d" % (i, j, group_people) + " " + \
                    " ".join(["%.3f %.3f %.3f" % tuple(generated_group[p, :]) for p in range(group_people)])
            groups.append(group)

    with open(output_file, "w+") as f:
        for group in groups:
            f.write("%s\n" % group)

    numpy_images = np.array(env_images)
    np.savez(npz_file, numpy_images, env_file, numpy_images.shape[1])


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Generate simulated data. The data will get printed to stdout.')

    parser.add_argument('-n', '--num-examples', type=int, default=100,
                        help="number of examples (dflt: 100)")
    parser.add_argument('--people-lo', type=int, default=2,
                        help="Min group size (dflt: 2)")
    parser.add_argument('--people-hi', type=int, default=6,
                        help="Max group size (dflt: 6)")
    parser.add_argument('--num-generated', type=int, default=2,
                        help="number of generated people (dflt: 2)")
    parser.add_argument('--seed', type=int,
                        help='Seed for random data generation',
                        default=-1)
    parser.add_argument('--model-path', type=str, required=True,
                        help='models logfile (Format: <datetime>-<iteration>.pt')
    parser.add_argument('--output-file', type=str, required=True,
                        help='output h5 file path (dflt: "data/generated/output_data.h5")',
                        default="data/generated/output_data.h5")
    parser.add_argument('--env-file', type=str, required=True,
                        help='environment map file path (dflt: "")',
                        default="")
    parser.add_argument("--personal-space", type=float, default=1.0,
                        help="min personal space in meters when generating groups (dflt: 1.0)")
    parser.add_argument("--crop-radius", type=float, default=3.0,
                        help="radius for cropping environment images (dflt: 3.0)")
    parser.add_argument('--r-range', nargs="+", type=int, default=[0.8, 1.5],
                        help='Radius range in meters for generating groups (default: [0.8, 1.5])')

    args = parser.parse_args()
    return args

def convert_to_absolute(path):
    cwd = os.getcwd()
    absolute = os.path.join(cwd, path)
    return absolute

def load_args(path):
    # save params
    with open(path, 'r') as fid:
        return json.load(fid)

def load_generator(model_path: str) -> torch.nn.Module:
    date, time, iteration = model_path.split("_")
    stamp = date + "_" + time
    model_path = "../logs/{}/{}_{}.pt".format(stamp,stamp,iteration)
    args_path = "../logs/{}/args.txt".format(stamp)
    args = load_args(args_path)
    generator = GenNet(args["grid_res"], int(args["grid_w"]), int(args["grid_w"]), bool(args["combine_env_channels"]), sigma=args["plot_sigma"])
    print(args)
    checkpoint = torch.load(model_path, map_location=torch.device('cpu'))
    generator.load_state_dict(checkpoint['generator_state_dict'])
    return generator

def main(args):

    if args.seed >= 0:
        np.random.seed(args.seed)

    print("Making dataset with env images...")
    generator = load_generator(args.model_path)
    make_data_with_env(args.num_examples, generator, args.output_file, args.env_file, args.npz_file,
                       args.crop_radius, args.people_lo, args.people_hi,
                       args.num_generated, args.r_range, args.personal_space)

if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)
