#!/usr/bin/env python
# Script to warp data (in h5 format)
# To run it on all the iGibson h5 files within a directory, this script could be run as:
# $ for i in data/igibson_sim_data/env_*.h5; do echo $i; ./bin/warp_data.py -i ${i}  --no-dyads; done

import numpy as np
import os
import cv2
import sys
import math
import json
from typing import Tuple
import argparse
import deepdish as dd  # for saving Python dictionaries into h5 files
import time
from matplotlib import pyplot as plt
import deepdish as dd
import random
import progressbar

random.seed(42)  # uncomment to make the transform params nondeterministic


def make_transform(rows, cols, sx, sy, radians):
    """
    Compute 2x3 transformation matrix for cv2.warpAffine
    :param rows: number of rows in the environment map
    :param cols: number of columns in the environment map
    :param sx: horizontal scale (1 is no scale)
    :param sy: vertical scale (1 is no scale)
    :param radians: rotation in radians (0 is no rotation)
    :return: 2x3 numpy array for full env transform, and 2x2 array for centered pose transforms
    """
    T1 = np.array([[1.0, 0.0, cols*0.5],
                   [0.0, 1.0, rows*0.5],
                   [0.0, 0.0, 1.0]])
    S = np.array([[sx, 0.0, 0.0],
                  [0.0, sy, 0.0],
                  [0.0, 0.0, 1.0]])
    R = np.array([[np.cos(radians), np.sin(radians), 0.0],
                  [-np.sin(radians), np.cos(radians), 0.0],
                  [0.0, 0.0, 1.0]])
    T2 = np.array([[1.0, 0.0, -cols*0.5],
                   [0.0, 1.0, -rows*0.5],
                   [0.0, 0.0, 1.0]])
    SR = S @ R
    M = T1 @ SR @ T2  # matrix multiply
    M = M[:2, :]  # 2 x 3 transform matrix
    return M, SR[:2, :2]


def transform_env(M, env):
    """Transform environment"""
    new_env = cv2.warpAffine(env, M, (env.shape[1], env.shape[0]),
                             flags=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT,
                             borderValue=0)
    return new_env


def transform_poses(SR, radians, individual, context, x_index=0, y_index=1, angle_index=2):
    """
    Transform pose data
    """

    def transform_position(tt, input_tensor):
        out_tensor = np.copy(input_tensor)
        out_tensor[:, [x_index, y_index]] = np.matmul(out_tensor[:, [x_index, y_index]], tt)
        return out_tensor

    # rotate context
    tt = SR.T
    out_context = transform_position(tt, context)

    # rotate individual (not centered so we need to translate)
    T1 = np.array([[1.0, 0.0, individual[0, x_index].item()],
                   [0.0, 1.0, individual[0, y_index].item()],
                   [0.0, 0.0, 1.0]])
    T2 = np.eye(3)
    T2[:2, :2] = SR.T  # we transpose it here because we are doing right multiply
    T3 = np.array([[1.0, 0.0, -individual[0, x_index].item()],
                   [0.0, 1.0, -individual[0, y_index].item()],
                   [0.0, 0.0, 1.0]])
    T = T1 @ T2 @ T3
    tt = T[:2, :2]
    out_ind = transform_position(tt, individual)

    # angles
    out_ind[:, angle_index] = out_ind[:, angle_index] - radians
    out_context[:, angle_index] = out_context[:, angle_index] - radians

    return out_ind, out_context


def plot_sample(env, individual, context, env_res, x_index=0, y_index=1, angle_index=2, ind_color='b', con_color='c'):
    """
    Helper function to plot sample
    """
    plt.imshow(env[:, :, 1:], origin='lower', vmin=0.0, vmax=1.0)  # no need to flip here, because this map was flipped when cropped

    offset_x = (env.shape[1] * 0.5) * env_res
    offset_y = (env.shape[0] * 0.5) * env_res
    plt.quiver(  (offset_x + individual[:, x_index])/env_res,
                 (offset_y + individual[:, y_index])/env_res,
                 np.cos(individual[:, angle_index]),
                 np.sin(individual[:, angle_index]),
                 color=ind_color  )

    plt.quiver((offset_x + context[:, x_index]) / env_res,
               (offset_y + context[:, y_index]) / env_res,
               np.cos(context[:, angle_index]),
               np.sin(context[:, angle_index]),
               color=con_color)

    plt.plot(offset_x / env_res, offset_y / env_res, 'dw')


def warp_one_sample(env, individual, context, env_res, sample_no, args=None):
    """
    Helper function to warp one sample
    """
    # sample transform params
    scaling = random.uniform(0.8, 1.1)
    transform_horizontally = random.randint(0, 1)
    if transform_horizontally:
        sx = scaling
        sy = 1.0
    else:
        sx = 1.0
        sy = scaling

    radians = random.uniform(-30.0, 30.0) * np.pi / 180.0

    M, SR = make_transform(env.shape[0], env.shape[1], sx, sy, radians)

    # transform data
    new_env = cv2.warpAffine(env, M, (env.shape[1], env.shape[0]),
                             flags=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT,
                             borderValue=0)
    new_ind, new_con = transform_poses(SR, radians, individual, context)

    if args is not None and args.plot:
        plt.subplot(1, 2, 1)
        plot_sample(env, individual, context, env_res)
        plt.title("%d) Original" % (sample_no))
        plt.subplot(1, 2, 2)
        plot_sample(new_env, new_ind, new_con, env_res)
        plt.title("Warped (sx=%.1f, sy=%.1f, r=%.1f)" % (sx, sy, radians*180.0/np.pi))
        plt.show(block=False)
        plt.pause(3)
        plt.close()
        plt.pause(1)

    return new_env, new_ind, new_con


def create_warped_dataset(dd_dict, output_file, args):
    """
    Warp the examples in the dataset and save into new h5 file
    """

    # get number of examples
    n = dd_dict['cmaps'].shape[0]
    if args.verbose:
        print("Got {} samples".format(n))

    num_features = 3
    env_res = dd_dict['env']['resolution']
    out_cmaps = []
    out_groups = []

    bar = progressbar.ProgressBar(maxval=n, \
                                  widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()

    # process each example one by one
    for i in range(n):

        # extract data
        env = dd_dict['cmaps'][i]
        g = dd_dict['groups'][i]

        p = g[1]  # num people in this group
        if args.no_dyads and p < 3:
            if args.verbose:
                print("skipped {} (p={})".format(i, p))
            continue  # skip dyad

        features = g[2:]
        matrix = np.reshape(np.array(features), (p, num_features), order='C')
        individual = matrix[0:1, :]
        context = matrix[1:, :]

        # make warp
        new_env, new_ind, new_con = warp_one_sample(env, individual, context, env_res, i, args)

        # store result
        new_matrix = np.vstack((new_ind, new_con))
        num_people = new_matrix.shape[0]
        assert num_people > 0, "Got empty group! This should not be: new_matrix = {}".format(new_matrix)
        new_features = [g[0], new_matrix.shape[0]] + new_matrix.flatten('C').tolist()

        out_groups.append(new_features)
        out_cmaps.append(new_env)

        bar.update(i + 1)

    if len(out_groups) == 0:
        print("WARNING: {} resulted in an empty warped dataset. Skipping this output.".format(args.input_h5))
        return

    # write to disk
    dd_dict['groups'] = out_groups
    dd_dict['cmaps'] = np.array(out_cmaps)

    dd.io.save(output_file, dd_dict, compression=('blosc', 4))
    if args.verbose:
        print("Saved {}".format(output_file))


def main(args):

    # set up output path
    if len(args.output_h5) == 0:
        base = os.path.splitext(args.input_h5)[0]
        output_file = base + "_warped.h5"
    else:
        output_file = args.output_h5

    # load up h5 data
    d = dd.io.load(args.input_h5)

    # modify data
    create_warped_dataset(d, output_file, args)


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """

    parser = argparse.ArgumentParser(description='Warp data.')
    parser.add_argument('-i', '--input-h5', type=str, required=True,
                        help='input path to h5 file')
    parser.add_argument('-o', '--output-h5', type=str, required=False,
                        help='output path (dflt: input-h5 basename with _warped.h5 extension)',
                        default="")
    parser.add_argument('-v', '--verbose', action="store_true",
                        help='print info?')
    parser.add_argument('-p', '--plot', action="store_true",
                        help='plot maps as they are being generated?')
    parser.add_argument('--no-dyads', action='store_true', default=False,
                        help='skip dyads when making warped samples')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)
