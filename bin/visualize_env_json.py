#!/usr/bin/env python
# Tool to visualize environment map in json format

import sys
import numpy as np
from matplotlib import pyplot as plt

from make_sim_data import load_environment

def main():

    if len(sys.argv) < 2:
        print("Invalid number of arguments. Run the script as:")
        print("./visualize_env_json.py <path-to-json-file>")
        sys.exit(1)

    json_file = sys.argv[1]
    print("Loading {}...".format(json_file))

    env_dict = load_environment(json_file)
    print("Environment size: {}".format(env_dict['grid'].shape))
    print("Left, Top offset: {}, {}".format(env_dict['left'], env_dict['top']))
    print("Resolution: {}".format(env_dict['resolution']))
    print("Labels: {}".format(env_dict['labels']))

    im = env_dict['grid']
    im2 = np.zeros((im.shape[0], im.shape[1], 3), dtype=np.uint8)
    im2[:, :, 0] = (im == 0) * 255  # red - empty
    im2[:, :, 1] = (im == 1) * 255  # green - tall obstacles
    im2[:, :, 2] = (im == 2) * 255  # blue - short obstacles
    plt.imshow(im2)
    plt.show()

if __name__ == "__main__":
    main()
    sys.exit(0)