# Script to make environment maps for interactive environments from iGibson
# Use of this data is restricted by the following agreement: http://svl.stanford.edu/gibson2/assets/GDS_agreement.pdf
#
# The core of the code follows the steps in:
# https://github.com/StanfordVL/iGibson/blob/master/gibson2/examples/demo/trav_map_vis_example.py
# to create environment maps.

import sys
import os
import argparse
import numpy as np
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import json


def load_image(image_path, scale_ratio=None):
    """
    Load image
    :param image_path: path to the image
    :param scale_ratio: image scale ratio (for width dimension) -- set None for no resize
    :return image as numpy array
    """
    im = Image.open(image_path)

    if scale_ratio is not None:
        w, h = im.size
        out_width = int(w * scale_ratio)
        out_height = int(h * out_width / w)
        print("{} - out_width: {}, out_height: {}".format(image_path, out_width, out_height))
        im = np.array(im.resize((out_width, out_height), reducing_gap=3.0))
    else:
        im = np.array(im)

    return im


def make_env_array(scene_dir, scale_ratio=None):
    """
    Make environment numpy array for a given scene folder
    :param scene_dir: path to scene directory
    :param scale_ratio: image scale ratio (for width dimension) -- set None for no resize
    :return image, tuple with min/max row with non-occupied values, tuple with min/max col with non-occupied values
    """
    # load maps
    path_trav_map = os.path.join(scene_dir, "floor_trav_0.png")
    im_tm = load_image(path_trav_map, scale_ratio=scale_ratio)

    path_obs_map = os.path.join(scene_dir, "objects_trav_0.png")
    im_om = load_image(path_obs_map, scale_ratio=scale_ratio)

    assert im_tm.shape[0] == im_om.shape[0], \
        "Expected the traversability maps to have the same height but got {} vs {}".format(im_tm.shape[0],
                                                                                           im_om.shape[0])
    assert im_tm.shape[1] == im_om.shape[1], \
        "Expected the traversability maps to have the same width but got {} vs {}".format(im_tm.shape[1],
                                                                                          im_om.shape[1])

    # combine the two maps
    im = np.zeros((im_tm.shape[0], im_tm.shape[1]), dtype=np.uint8)
    im[im_om < 122] = 2  # occupied (short)
    im[im_tm < 122] = 1  # occupied (tall)

    print("original image size: {}".format(im.shape))

    # identify region with actual free space -- would make it easier to place groups later
    min_r = im.shape[0]
    max_r = 0
    min_c = im.shape[1]
    max_c = 0
    for r in range(im.shape[0]):
        for c in range(im.shape[1]):
            if im[r, c] == 0:  # free space
                if r < min_r:
                    min_r = r
                if r > max_r:
                    max_r = r
                if c < min_c:
                    min_c = c
                if c > max_c:
                    max_c = c

    print("region with occupied values: rows - {}, {}; cols - {}, {}".format(min_r, max_r, min_c, max_c))

    # add margin
    margin = 2  # pixels
    min_r = int(np.max([0, min_r - margin]))
    max_r = int(np.min([im.shape[0], max_r + margin]))
    min_c = int(np.max([0, min_c - margin]))
    max_c = int(np.min([im.shape[1], max_c + margin]))

    return im, (min_r, max_r), (min_c, max_c)


def main(args):
    """
    Main function
    """
    # get all scene folders in input dir
    scenes = [f.path for f in os.scandir(args.ig_dataset) if f.is_dir()]
    if args.verbose:
        print("Found %d scenes:" % len(scenes))

    # create output folder if it doesn't exist already
    if not os.path.exists(args.output_dir):
        print("The output folder {} does not exist. It will be created automatically for you now.".
              format(args.output_dir))
        os.makedirs(args.output_dir)

    # process each scene
    for s in scenes:
        if args.verbose:
            print(" Processing " + s)

        full_im, (min_r, max_r), (min_c, max_c) = \
            make_env_array(s, scale_ratio=0.01/args.resolution)  # by default, the iGibson maps are in 0.01 m

        # crop image
        im = full_im[min_r:max_r, min_c:max_c]

        if args.plot:
            print("cropping to lt: ({}, {}), br: ({}, {})".format(min_c, min_r, max_c, max_r))

            # convert to 3 channel image
            im2 = np.zeros((im.shape[0], im.shape[1], 3), dtype=np.uint8)
            im2[:, :, 0] = (im == 0) * 255  # red - empty
            im2[:, :, 1] = (im == 1) * 255  # green - tall obstacles
            im2[:, :, 2] = (im == 2) * 255  # blue - short obstacles
            plt.imshow(im2)
            plt.show()

        # save data
        scene_name = os.path.basename(s)
        output_file = os.path.join(args.output_dir, "env_%s.txt" % scene_name)
        labels = {0: 'empty', 1: 'occupied (tall)', 2: 'occupied (short)'}

        with open(output_file, 'w') as fid:
            d = {'grid': im.tolist(),
                 'left': 0.0,
                 'top': 0.0,
                 'resolution': args.resolution,
                 'labels': labels}
            json.dump(d, fid, separators=(',', ':'), indent=2, sort_keys=False)


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """

    curr_dir = os.path.abspath(os.path.dirname(__file__))
    dflt_ig_dataset = os.path.join(curr_dir, "../data/igibson_maps")
    dflt_output_fldr = os.path.join(curr_dir, "../data/igibson_json")

    parser = argparse.ArgumentParser(description='Generate env maps for iGibson trajectory maps.')
    parser.add_argument('--ig-dataset', type=str, required=True,
                        help='path to igibson dataset folder (dflt: data/igibson_maps in this repository)',
                        default=dflt_ig_dataset)
    parser.add_argument('--output-dir', type=str, required=False,
                        help='output-directory (dflt: data/igibson_json in this repository)',
                        default=dflt_output_fldr)
    parser.add_argument('-v', '--verbose', action="store_true",
                        help='print info?')
    parser.add_argument('-p', '--plot', action="store_true",
                        help='plot maps as they are being generated?')
    parser.add_argument('--resolution', type=float, required=False,
                        help='resolution for output map (in meters)',
                        default=0.25)

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)
