# Script to make simulated data in the same format as for the Cocktail Party dataset
import numpy as np
import os
import cv2
import sys
import math
import json
from typing import Tuple
import argparse
import deepdish as dd  # for saving Python dictionaries into h5 files
import time
from matplotlib import pyplot as plt

group_distribution = [0]

def centered_group(num_people_list: list, r_range: Tuple[int, int], personal_space: float,
                   x_index: int = 0, y_index: int = 1) -> np.ndarray:
    """
    Helper function to make centered group features.
    The data is generated first defining a circle around (0,0) and then placing people around it.
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    :return: Px3 numpy array where P in [people_lo, people_hi] and the columns are x, y, theta
    :note Example value for personal space: https://en.wikipedia.org/wiki/Proxemics#/media/File:Personal_Space.svg
    """
    # step 1. sample radius for all examples
    r = np.random.uniform(low=r_range[0], high=r_range[1])

    # step 2. sample number of people per group (we add 1 to high limit because randint does not include it)
    num_people = np.random.choice(num_people_list)

    # step 3. generate angles for the people in the group
    min_ang = np.arcsin(personal_space*0.5 / r) * 2.0
    offset = np.random.uniform(low=0.0, high=2*np.pi/num_people)
    max_ang = (2*np.pi-offset) / num_people
    angles = np.random.uniform(low=min_ang, high=max_ang, size=(num_people, 1))
    angles[0, 0] += offset  # add offset to the first angle to reduce bias towards zero
    angles = np.cumsum(angles, axis=0)
    np.random.shuffle(angles)

    # step 4. create position (x,y) values and angle towards the center (0,0)
    x = np.cos(angles) * r
    y = np.sin(angles) * r
    theta = np.arctan2(y, x) + np.pi

    data = np.concatenate((x, y, theta), axis=1)
    center = np.mean(data[1:], axis=0).reshape(1,3) # get center of context
    data[:, [x_index, y_index]] = data[:, [x_index, y_index]] - center[:, [x_index, y_index]] # center to context

    assert data.shape[0] == num_people
    assert data.shape[1] == 3

    return data


def load_environment(environment_file: str) -> dict:
    """
    Function that loads the environment annotations from disk
    :param environment_file: path to environment file
    """
    with open(environment_file, 'r') as fid:
        env_data = fid.read()
        environment = json.loads(env_data)
        environment['grid'] = np.asarray(environment['grid'])
        return environment


def env_centered_group(people_lo: int, people_hi: int, num_people_list: list, r_range: Tuple[int, int],
                       personal_space: float, env: dict, crop_radius: float, group_limit: int, x_index: int = 0,
                       y_index: int = 1) -> Tuple[np.ndarray, np.ndarray]:

    """
    Helper function to make centered group features given environment constraints
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    :param env: dict containing env data
    :param crop_radius: float radius for cropping env around group
    :param x_index: index containing x data in dataset
    :param y_index: index containing y data in dataset
    """
    res = env["resolution"]
    left = env["left"]
    top = env["top"]
    grid = np.fliplr(env["grid"])  # flip to make it easier to reason about x
    global group_distribution
    global empty_cells_ngroups
    

    # range based on grid limits
    Xrange = [left, left + grid.shape[1]*res]
    Yrange = [top, top + grid.shape[0]*res]

    # for debugging where points are being placed
    def plot_point_on_map(env_map, point):
        # convert to 3 channel image
        im2 = np.zeros((env_map.shape[0], env_map.shape[1], 3), dtype=np.uint8)
        im2[:, :, 0] = (env_map == 0) * 255  # red - empty
        im2[:, :, 1] = (env_map == 1) * 255  # green - tall obstacles
        im2[:, :, 2] = (env_map == 2) * 255  # blue - short obstacles
        # print(point)
        all_points = []
        for pt in point:
            # print(pt)
            all_points.extend(world_to_grid(np.expand_dims(pt,0)))

        all_points = np.squeeze(np.array(all_points))

        plt.scatter(all_points[:,0], all_points[:,1], color='red', s=1)
        plt.imshow(env_map)
        plt.show()

    def world_to_grid(pt: np.ndarray, x_index: int = 0, y_index: int = 1):
        x1 = (np.floor((pt[:, x_index] - left)/res))
        y1 = (np.floor((pt[:, y_index] - top)/res))
        x2 = (np.ceil((pt[:, x_index] - left)/res))
        y2 = (np.ceil((pt[:, y_index] - top)/res))

        # all 4 pixels around pt
        xx = np.concatenate((x1, x1, x2, x2), axis=0)
        yy = np.concatenate((y1, y2, y1, y2), axis=0)

        return np.column_stack((xx, yy)).astype(int)

    def get_grid_value(pt_world: np.ndarray, grid_data: np.ndarray):
        pt = world_to_grid(pt_world)
        values = []
        for i in range(pt.shape[0]):
            c = pt[i, 0]
            r = pt[i, 1]

            if c < 0 or c >= grid_data.shape[1]:  # x-coordinate
                values.append(-1)  # outside grid
            elif r < 0 or r >= grid_data.shape[0]:  # y-coordinate
                values.append(-1)  # outside grid
            else:
                values.append(grid_data[r, c])
        return values

    def compute_midpoints(xy: np.ndarray, center: np.ndarray, res: int):
        mps = []
        distance = np.sqrt((center[0][0] - xy[0][0])**2 + (center[0][1] - xy[0][1])**2)
        num_midpoints = (int)(np.ceil((distance/res))) # need to ciel in case dist/res is <1 so it rounds to 1
        for p in xy:
            for frac in range(1, num_midpoints):
                mid = ((frac*p[0] + (num_midpoints-frac)*center[0][0])/num_midpoints,
                        (frac*p[1] + (num_midpoints-frac)*center[0][1])/num_midpoints)
                mps.append(mid)
        return np.array(mps)

    def compute_people_midpoints(xy: np.ndarray, res: int):
        people_midpoints = []
        for i, p1 in enumerate(xy):
            for p2 in xy[i+1:]:
                distance = np.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

                # sample across the line according to resolution
                num_midpoints = (int)(np.ceil((distance/res))) # need to ciel in case dist/res is <1 so it rounds to 1
                for frac in range(1, num_midpoints):
                    mid = ((frac*p1[0] + (num_midpoints-frac)*p2[0])/num_midpoints,
                            (frac*p1[1] + (num_midpoints-frac)*p2[1])/num_midpoints)
                    people_midpoints.append(mid)

        return np.array(people_midpoints)

    def compute_people_bubbles(xy: np.ndarray, radius: int):
        n = xy.shape[0]
        # angles list for person bubbles
        angles_list = [0, .25*np.pi, .5*np.pi, .75*np.pi, np.pi, 1.25*np.pi, 1.5*np.pi, 1.75*np.pi]
        x_constant = radius * np.cos(angles_list)
        y_constant = radius * np.sin(angles_list)

        all_bubble_points = []
        for i in xy:
            x = (x_constant+i[0]).reshape((8,1))
            y = (y_constant+i[1]).reshape((8,1))
            this_bubble = np.hstack((x,y))
            all_bubble_points.extend(this_bubble)
        return np.vstack(all_bubble_points)
            
    start = time.time()
    timeout = 600
    # generate groups until one of them works out in the environment
    while True: # sum > 0
        now = time.time()
        
        if(now - start) > timeout:
            print("Group limit exceeded timeout of", timeout, "seconds")
            return ([-1],[-1]), ([-1],[-1])

        res = env["resolution"]
        left = env["left"]
        top = env["top"]
        grid = np.fliplr(env["grid"])  # flip to make it easier to reason about x

        group = centered_group(num_people_list, r_range, personal_space)
        npeople = group.shape[0]
        npeople_idx = npeople-people_lo

        # if there are already enough samples for this group size, try again with a different size choice
        if group_distribution[npeople_idx] > group_limit:
            num_people_list.remove(npeople)
            continue

        # if there are no empty spaces left for this group size, and we still want more samples for it, remake that part of the list
        if (len(empty_cells_ngroups[npeople_idx]) == 0):
            # print("Tried all spaces for group size", npeople)
            empty_cells_ngroups[npeople_idx] = np.argwhere(np.fliplr(env["grid"]) == 0).tolist()
            continue

        # try to find empty whole in map for the group
        k = np.random.randint(0, high=len(empty_cells_ngroups[npeople_idx]))
        indices = empty_cells_ngroups[npeople_idx][k]
        empty_cells_ngroups[npeople_idx].remove(indices)
        x = left + indices[1] * res
        y = top + indices[0] * res
        # print("Trying x,y={},{}".format(x,y))
            

        adjusted_center = np.repeat(np.array([[x,y,0]]), group.shape[0], axis=0)
        adjusted_group = group + adjusted_center
        center = np.array([[x,y]])
        assert center.shape[0] == 1, \
            "Expected the center to have 1 row (got {})".format(center.shape[0])
        assert center.shape[1] == 2, \
            "Expected the center to have 2 columns (got {})".format(center.shape[1])

        # check center and midpoints from center to people
        xy = adjusted_group[:, [x_index, y_index]]
        midpoints = compute_midpoints(xy, center, res)
        people_midpoints = compute_people_midpoints(xy, res)
        midpoints = np.vstack((people_midpoints, midpoints))
        
        values = get_grid_value(np.vstack((center, midpoints)), grid)  # check tall obstacle
        all_good = True
        for val in values:
            if val < 0 or val > 0:  # non-free cell
                all_good = False
                break
        if not all_good:
            continue

        # check people
        people_values = get_grid_value(adjusted_group[:, [x_index, y_index]], grid)
        for val in people_values:
            if val < 0 or val > 0:  # occupied or invalid cell
                all_good = False
                break

        # check bubbles
        bubbles = compute_people_bubbles(xy, 1)
        bubble_values = get_grid_value(bubbles, grid)
        for val in bubble_values:
            if val < 0 or val > 0:  # occupied or invalid cell
                all_good = False
                break

        # Double check the cropping didn't mess anything up
        env_data = crop_env(adjusted_group[1:], env, crop_radius) # Crop with respect to context only
        left = -env_data.shape[0] / 2.0 * res
        top = -env_data.shape[0] / 2.0 * res
        cropped_values = get_grid_value(group[:, [x_index, y_index]], env_data[:,:,1])
        for val in cropped_values:
            if val < 1 or val > 1:  # occupied or invalid cell
                all_good = False
                break

        # if we got this far, we are done
        if all_good:
            group_distribution[npeople_idx] += 1
            break

    return group, env_data

def crop_env(group: np.ndarray, env: dict, crop_radius: float = 3.0, x_index: int = 0, y_index: int = 1) -> np.ndarray:
    """
    Helper function to crop the environment map relative to a group's context
    :param group: group tensor
    :param env: dict containing environment data
    :param crop_radius: float radius for cropping env around group
    :param x_index: index containing x data in dataset
    :param y_index: index containing y data in dataset
    :return: cropped map
    """
    resolution = env["resolution"]
    left = env["left"]
    top = env["top"]
    grid = np.fliplr(env["grid"])

    group_center = np.mean(group[:, [x_index, y_index]], axis=0, keepdims=True)
    assert group_center.shape[0] == 1, \
        "Expected group center to have 1 row. Got shape: {}".format(group_center.shape)
    assert group_center.shape[1] == 2, \
        "Expected group center to have 2 cols. Got shape: {}".format(group_center.shape)

    r = int(round(crop_radius / resolution))  # cropped map radius in pixels

    cx = (group_center[0, 0] - left) / resolution
    cy = (group_center[0, 1] - top) / resolution

    x1 = ((group_center[0, 0] - crop_radius) - left) / resolution
    x2 = ((group_center[0, 0] + crop_radius) - left) / resolution
    y1 = ((group_center[0, 1] - crop_radius) - top) / resolution
    y2 = ((group_center[0, 1] + crop_radius) - top) / resolution

    x1i = int(math.floor(x1))
    x2i = int(math.ceil(x2))
    y1i = int(math.floor(y1))
    y2i = int(math.ceil(y2))

    border_top = max(y1i, 0) - y1i
    border_bottom = y2i - min(y2i, grid.shape[0])
    border_left = max(x1i, 0) - x1i
    border_right = x2i - min(x2i, grid.shape[1])

    # separate grid into multiple channels
    grid = np.int8(grid)
    multichannel_grid = np.zeros((grid.shape[0], grid.shape[1], 3), dtype=grid.dtype)
    multichannel_grid[:, :, 0] = np.where(grid == 0, np.ones(grid.shape), np.zeros(grid.shape))  # free space
    multichannel_grid[:, :, 1] = np.where(grid == 1, np.ones(grid.shape), np.zeros(grid.shape))  # tall obstacles
    multichannel_grid[:, :, 2] = np.where(grid == 2, np.ones(grid.shape), np.zeros(grid.shape))  # short obstacles

    # add border to grid if necessary for cropping
    if border_top > 0 or border_bottom > 0 or border_left > 0 or border_right > 0:
        expanded = cv2.copyMakeBorder(multichannel_grid, border_top, border_bottom, border_left, border_right,
                                      cv2.BORDER_CONSTANT, 0)
    else:
        expanded = multichannel_grid

    # crop
    expanded = expanded.astype('float32')
    cropped = cv2.getRectSubPix(expanded, (2*r, 2*r), (cx + border_left, cy + border_top))

    # separate each layer into 1/0 channels
    # we would create 4 channels: unknown, free, occupied rigid, occupied movable
    im = np.zeros((2*r, 2*r, 4), dtype=np.float32)  # height (y), width (x), channels

    # fill in unknown
    im[:, :, 0] = 1
    im[border_top:2*r-border_bottom, border_left:2*r-border_right, 0] = 0

    # fill in other channels (free, tall obs, short obs)
    im[:, :, 1:] = cropped

    return im


# The function below is not currently being used. It's only here for historic purposes...
def make_data(n: int, output_file: str, people_lo: int = 2, people_hi: int = 7,
              r_range: Tuple[int, int] = (0.8, 1.5), personal_space: float = 1.2) -> None:
    """
    Make simulated data with 2D position only. Outputs "features.txt" and "groups.txt" files to the given folder
    :param n: number of samples
    :param groups_file: string containing relative path of saved groups data file
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    """
    groups = []
    for i in range(n):
        group_data = centered_group(people_lo, people_hi, r_range, personal_space)
        num_people = group_data.shape[0]
        group = [i, num_people] + group_data.flatten()
        groups.append(group)

    with open(output_file, "w+") as f:
        for group in groups:
            f.write("%s\n" % group)

def make_data_with_env(n: int, output_file: str, env_file: str, crop_radius: float,
                       people_lo: int = 2, people_hi: int = 7, r_range: Tuple[int, int] = (0.8, 1.5),
                       personal_space: float = 0.7) -> None:
    """
    Make simulated data with 2D position only. Outputs group data and env data to output_file, the saved h5 file.
    :param n: number of samples
    :param output_file: string containing relative path of saved h5 file
    :param env_file: string containing relative path of environment file
    :param crop_radius: float for how wide env images should be cropped
    :param people_lo: minimum number of people per group
    :param people_hi: maximum number of people per group
    :param r_range: radius range for group circles
    :param personal_space: personal space radius in meters
    """
    env = load_environment(env_file)
    groups = []
    env_images = []
    global group_distribution 
    people_range = (people_hi - people_lo) + 1
    group_distribution = [0] * people_range

    # get a list of potential group sizes
    global num_people_list
    num_people_list = list(range(people_lo, people_hi + 1))
    
    # get empty cells in grid
    global empty_cells_ngroups
    empty_cells = np.argwhere(np.fliplr(env["grid"]) == 0)
    empty_cells_ngroups = np.repeat(empty_cells[np.newaxis, :, :], people_range, axis=0).tolist()

    # even amt of groups cannot be generated
    if n < people_range:
        print("Number of samples is less than available group sizes")
        exit(0)

    # limit to amount generated per group size
    group_limit = int(n/(people_range))

    for i in range(n):
        print("\rMaking sample %06d/%06d" % (i, n), end="")
        group_data, env_data = env_centered_group(people_lo, people_hi, num_people_list, r_range, personal_space, env, crop_radius, group_limit)
        if group_data == env_data:
            print("\nTime limit exceeded")
            break
        env_images.append(env_data)
        num_people = group_data.shape[0]
        group_data = list(group_data.flatten())
        group = [i, num_people] + group_data
        groups.append(group)
        # print(type(groups[0]))
    print("\nGroup distribution was {}".format(group_distribution))


    print("\nDone. Writing groups to disk ({}).".format(output_file))
    numpy_images = np.array(env_images)

    store_dict = {"groups": groups,
                "cmaps": numpy_images,
                "cwidth": numpy_images.shape[1],
                "env": env}

    dd.io.save(output_file, store_dict, compression=('blosc', 4))



def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Generate simulated data. The data will get printed to stdout.')

    parser.add_argument('-n', '--num-examples', type=int, default=3000,
                        help="number of examples (dflt: 1000)")
    parser.add_argument('--people_lo', type=int, default=2,
                        help="Min group size (dflt: 2)")
    parser.add_argument('--people_hi', type=int, default=6,
                        help="Max group size (dflt: 6)")
    parser.add_argument('--seed', type=int,
                        help='Seed for random data generation',
                        default=-1)
    parser.add_argument('--output-file', type=str, required=True,
                        help='output h5 file path (dflt: "data/simulated/output_data.h5")',
                        default="data/simulated/output_data.h5")
    parser.add_argument('--env-file', type=str, required=True,
                        help='environment map file path (dflt: "")',
                        default="")
    parser.add_argument("--personal-space", type=float, default=1.0,
                        help="min personal space in meters when generating groups (dflt: 1.0)")
    parser.add_argument("--crop-radius", type=float, default=3.0,
                        help="radius for cropping environment images (dflt: 3.0)")
    parser.add_argument('--r-range', nargs="+", type=int, default=[0.8, 1.5],
                        help='Radius range in meters for generating groups (default: [0.8, 1.5])')

    args = parser.parse_args()
    return args

def convert_to_absolute(path):
    cwd = os.getcwd()
    absolute = os.path.join(cwd, path)
    return absolute

def main(args):

    if args.seed >= 0:
        np.random.seed(args.seed)

    print("Making dataset with env images...")
    make_data_with_env(args.num_examples, args.output_file, args.env_file, args.crop_radius,
                       args.people_lo, args.people_hi, args.r_range, args.personal_space)

if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)




