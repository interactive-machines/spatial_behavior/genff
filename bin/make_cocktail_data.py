#!/usr/bin/env python3
# Script to make cocktail party datasets in the same format as for the Cocktail Party dataset
import numpy as np
import os
import re
import cv2
import sys
import math
import json
from typing import Tuple, List
import argparse
from collections import OrderedDict
import deepdish as dd  # for saving Python dictionaries into h5 files


def create_data_points(features: OrderedDict, groups: OrderedDict, use_body_orientation: bool) -> Tuple[list, dict]:
    """
    Extract data from Cocktail dataset dictionaries
    :param features: OrderedDict of features from features.txt file
    :param groups: OrderedDict of groups from groups.txt file
    """
    data_points = {}
    combined = [time[1] for time in features.items() if time[0] in groups.keys()]
    dataset = zip(groups.keys(), groups.values(), combined)
    for row in dataset:
        return_groups = []
        for group in row[1]:
            group_list = []
            for person in group:
                if use_body_orientation:
                    group_list.append(row[2][person][0:2] + row[2][person][3:])  # swap head for body
                else:
                    group_list.append(row[2][person][0:3])

            return_groups.append(group_list)
        data_points[row[0]] = return_groups
    stamps = [(float(x), x) for x in data_points.keys()]
    stamps.sort(key=lambda x: x[0])
    return stamps, data_points


def load_features(features_file: str) -> Tuple[int, OrderedDict]:
    """
    Method that loads the features from disk
    :param features_file: path to features text file
    """
    # clean up the store in case we had loaded features before
    person_ids = []
    return_features = OrderedDict()

    # parse file
    features_per_frame = np.genfromtxt(features_file, dtype='str')

    features_stamps = list(features_per_frame[:, 0])
    num_features = None
    pid_set = set()

    def check_num_features(num_features, f: List[float]):
        """Helper function to update the number of features estimated so far and check for consistency"""
        if num_features is None:
            num_features = len(f)
        elif num_features != len(f):
            raise RuntimeError("Inconsistent number of features (got {} but expected {})".
                               format(len(f), num_features))
        return num_features

    for i, stamp in enumerate(features_stamps):
        # get features data for the given stamp
        people = features_per_frame[i][1:]
        features = {}

        # parse people list
        pid = None
        f: List[float] = []
        for p in range(len(people)):
            if people[p][0:2] == "ID":
                if pid is not None:
                    features[pid] = f
                    num_features = check_num_features(num_features, f)
                pid = people[p]
                f = []
            else:
                f.append(float(people[p]))

        if pid is not None:
            features[pid] = f
            num_features = check_num_features(num_features, f)

        # save people's identifiers
        for identifier in features.keys():
            if identifier not in pid_set:
                pid_set.add(identifier)

        # store features for this frame
        return_features[stamp] = features

    # convert person ids from set to list
    person_ids = list(pid_set)
    person_ids.sort()
    return num_features, return_features


def load_groups(groups_file: str) -> OrderedDict:
    """
    Method that loads the groups from disk
    :param groups_file: path to groups text file
    """
    # clean up the store in case we had loaded features before
    return_groups = OrderedDict()

    # parse file
    groups_per_frame = np.genfromtxt(groups_file, dtype='str', delimiter=',')

    # convert group data to dict
    stamps = []
    for groups in groups_per_frame:
        groups_arr = re.split(" < | > < ", groups)
        s = groups_arr[0]

        stamps.append(s)
        return_groups[s] = []

        last_index = -1
        for group in groups_arr[1:]:
            last_index += 1
            return_groups[s].append(re.split(" ", group))

        # remove last > character
        if len(groups_arr[1:]) == 0:
            continue

        return_groups[s][last_index] = return_groups[s][last_index][:-1]

    return return_groups


def generate_labeled_data(stamps: list, data_points: dict) -> list:
    """
    Extract positive examples from Cocktail dataset
    :param stamps: time stamp list
    :param data_points: dict of datapoints from cocktail party dataset
    """
    return_groups = []

    for stamp_flt, stamp in stamps:
        groups = data_points[stamp]

        # If only one group in time frame, can only generate positive examples
        if len(groups) == 1 and len(groups[0]) > 1:
            for individual in range(len(groups[0])):
                return_group = np.vstack([groups[0][individual]] + groups[0][:individual] + \
                                          groups[0][individual+1:])
                return_groups.append((stamp_flt, return_group.tolist()))

        else:
            for (i, group) in enumerate(groups):
                # Generate positive example
                if len(group) > 1:
                    for individual in range(len(group)):
                        return_group = np.vstack([group[individual]] + group[:individual] + \
                                                  group[individual+1:])
                        return_groups.append((stamp_flt, return_group.tolist()))

    return return_groups


def load_environment(environment_file: str) -> dict:
    """
    Function that loads the environment annotations from disk
    :param environment_file: path to environment file
    """
    with open(environment_file, 'r') as fid:
        env_data = fid.read()
        environment = json.loads(env_data)
        environment['grid'] = np.asarray(environment['grid'])
        return environment


def crop_env(group: np.ndarray, env: dict, crop_radius: float = 3.0, x_index: int = 0, y_index: int = 1) -> np.ndarray:
    """
    Helper function to crop the environment map relative to a group's context
    :param group: group tensor
    :param env: dict containing environment data
    :param crop_radius: float radius for cropping env around group
    :param x_index: index containing x data in dataset
    :param y_index: index containing y data in dataset
    :return: cropped map
    """
    resolution = env["resolution"]
    left = env["left"]
    top = env["top"]
    grid = np.fliplr(env["grid"])

    group_center = np.mean(group[:, [x_index, y_index]], axis=0, keepdims=True)
    assert group_center.shape[0] == 1, \
        "Expected group center to have 1 row. Got shape: {}".format(group_center.shape)
    assert group_center.shape[1] == 2, \
        "Expected group center to have 2 cols. Got shape: {}".format(group_center.shape)

    r = int(round(crop_radius / resolution))  # cropped map radius in pixels

    cx = (group_center[0, 0] - left) / resolution
    cy = (group_center[0, 1] - top) / resolution

    x1 = ((group_center[0, 0] - crop_radius) - left) / resolution
    x2 = ((group_center[0, 0] + crop_radius) - left) / resolution
    y1 = ((group_center[0, 1] - crop_radius) - top) / resolution
    y2 = ((group_center[0, 1] + crop_radius) - top) / resolution

    x1i = int(math.floor(x1))
    x2i = int(math.ceil(x2))
    y1i = int(math.floor(y1))
    y2i = int(math.ceil(y2))

    border_top = max(y1i, 0) - y1i
    border_bottom = y2i - min(y2i, grid.shape[0])
    border_left = max(x1i, 0) - x1i
    border_right = x2i - min(x2i, grid.shape[1])

    # separate grid into multiple channels
    grid = np.int8(grid)
    multichannel_grid = np.zeros((grid.shape[0], grid.shape[1], 3), dtype=grid.dtype)
    multichannel_grid[:, :, 0] = np.where(grid == 0, np.ones(grid.shape), np.zeros(grid.shape))  # free space
    multichannel_grid[:, :, 1] = np.where(grid == 1, np.ones(grid.shape), np.zeros(grid.shape))  # tall obstacles
    multichannel_grid[:, :, 2] = np.where(grid == 2, np.ones(grid.shape), np.zeros(grid.shape))  # short obstacles

    # add border to grid if necessary for cropping
    if border_top > 0 or border_bottom > 0 or border_left > 0 or border_right > 0:
        expanded = cv2.copyMakeBorder(multichannel_grid, border_top, border_bottom, border_left, border_right,
                                      cv2.BORDER_CONSTANT, 0)
    else:
        expanded = multichannel_grid

    # crop
    expanded = expanded.astype('float32')
    cropped = cv2.getRectSubPix(expanded, (2*r, 2*r), (cx + border_left, cy + border_top))

    # separate each layer into 1/0 channels
    # we would create 4 channels: unknown, free, occupied rigid, occupied movable
    im = np.zeros((2*r, 2*r, 4), dtype=np.float32)  # height (y), width (x), channels

    # fill in unknown
    im[:, :, 0] = 1
    im[border_top:2*r-border_bottom, border_left:2*r-border_right, 0] = 0

    # fill in other channels (free, tall obs, short obs)
    im[:, :, 1:] = cropped

    return im


def center_group(group_data: np.ndarray, x_index: int = 0, y_index: int = 1) -> list:
    """
    Center group of cocktail data
    :param group_data: nx3 feature data
    """
    center = np.mean(group_data[1:, [x_index, y_index]], axis=0, keepdims=True)
    n = group_data.shape[0]
    center_repeated = np.repeat(center, n, axis=0)
    group_data[:, [x_index, y_index]] = group_data[:, [x_index, y_index]] - center_repeated[:, [x_index, y_index]]
    return group_data.flatten(order='C').tolist(), center


def make_data_with_env(out_file: str, groups_file: str = "data/cocktail_party/groups_train.txt",
                       features_file: str = "data/cocktail_party/features.txt",
                       env_file: str = "data/cocktail_party/env_cocktail_party.txt",
                       crop_radius: float = 3.0, use_body_orientation: bool = True) -> None:
    """
    Make cocktail data
    :param out_file: string containing relative path of saved h5 file
    :param groups_file: string containing relative path of saved groups data file
    :param features_file: string containing relative path of saved groups data file
    :param env_file: string containing relative path of environment file
    :param crop_radius: float for how wide env images should be cropped
    :param use_body_orientation: use the orientation of the body of people as the angle in the dataset?
    """
    num_features, features = load_features(features_file)
    groups = load_groups(groups_file)
    env = load_environment(env_file)
    stamps, data_points = create_data_points(features, groups, use_body_orientation)
    groups_data = generate_labeled_data(stamps, data_points)
    env_images = []
    final_groups = []
    group_centers = []

    for group in groups_data:
        stamp, group_data = group
        num_people = len(group_data)
        np_data, center = center_group(np.array(group_data))
        group_centers.append(center.tolist())
        group = [stamp, num_people] + np_data
        final_groups.append(group)
        env_image = crop_env(np.array(group_data[1:]), env, crop_radius=crop_radius)  # Crop w.r.t. context only
        env_images.append(env_image)

    numpy_images = np.array(env_images)
    store_dict = {"groups": final_groups,
                  "centers": group_centers,
                  "cmaps": numpy_images,
                  "cwidth": numpy_images.shape[1],
                  "env": env}

    dd.io.save(out_file, store_dict)


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Generate simulated data. The data will get printed to stdout.')
    parser.add_argument('--seed', type=int,
                        help='Seed for random data generation',
                        default=-1)
    parser.add_argument('--output-file', type=str,
                        help='output h5 file (dflt: "data/cocktail_party/output_data.h5")',
                        default="data/cocktail_party/output_data.h5")
    parser.add_argument('--groups-file', type=str, required=True,
                        help='input groups data txt file path (dflt: "data/cocktail_party/groups_train.txt")',
                        default="data/cocktail_party/groups_train.txt")
    parser.add_argument('--features-file', type=str, required=True,
                        help='features data txt file path (dflt: "data/cocktail_party/features.txt")',
                        default="data/cocktail_party/features.txt")
    parser.add_argument('--env-file', type=str, required=True,
                        help='environment map file path (dflt: "data/cocktail_party/env_cocktail_party.txt")',
                        default="data/cocktail_party/env_cocktail_party.txt")
    parser.add_argument("--crop-radius", type=float, default=3.0,
                        help="radius for cropping environment images (dflt: 3.0)")

    args = parser.parse_args()
    return args


def convert_to_absolute(path):
    cwd = os.getcwd()
    absolute = os.path.join(cwd, path)
    return absolute


def main(args):

    if args.seed >= 0:
        np.random.seed(args.seed)

    print("Making dataset with env images...")
    make_data_with_env(args.output_file, args.groups_file, args.features_file,
                       args.env_file, args.crop_radius)


if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)
