#!/usr/bin/env python
"""Split group file for Cocktail Party, such that we can use part of the data for training and part for evaluation.
By default, this script splits the data into two sets: training and testing. Testing is 20%, distributed evenly
between the beginning, middle and end of the Cocktail Party sequence.
"""

import argparse
import sys
import os
import math

def split_data(groups_file, output_dir, verbose, percent_test=0.2):
    """
    Split the group data into 2 files: groups_train.txt, and groups_test.txt
    :param groups_file: path to input groups file
    :param output_dir: output directory
    :param verbose: print data?
    :param segment_length: length of the segments at the beginning, middle and end (in seconds)
    """
    # pass 1. load up the stamps
    stamps = []
    with open(groups_file, 'r') as fid:
        for line in fid:
            line = line.rstrip().split()
            if len(line) == 0:
                continue
            stamps.append(float(line[0]))

    start_time = stamps[0]
    end_time = stamps[-1]
    mid_time = stamps[int(len(stamps)*0.5)]

    segment_length = ((end_time - start_time) * percent_test)/3

    segment_1 = (start_time, start_time + segment_length)
    segment_2 = (mid_time - segment_length * 0.5, mid_time + segment_length * 0.5)
    segment_3 = (end_time - segment_length, end_time)

    if verbose:

        print("Got {} lines with stamps. Start time = {}, Mid time = {}, End time = {}".
              format(len(stamps), start_time, mid_time, end_time))
        test_time = segment_3[1] - segment_3[0] + segment_2[1] - segment_2[0] + segment_1[1] - segment_1[0]
        train_time = segment_2[0] - segment_1[1] + segment_3[0] - segment_2[1]
        total_time = end_time - start_time
        assert math.fabs(test_time + train_time - total_time) < 1e-10, "Something is wrong with the split"
        print("Segment length = {:.2f} seconds.".format(segment_length))
        print("Segment 1: {} - {} (length: {})".format(segment_1[0], segment_1[1], segment_1[1] - segment_1[0]))
        print("Segment 2: {} - {} (length: {})".format(segment_2[0], segment_2[1], segment_2[1] - segment_2[0]))
        print("Segment 3: {} - {} (length: {})".format(segment_3[0], segment_3[1], segment_3[1] - segment_3[0]))
        print("Test time = {} ({:.2f}), Train time = {} ({:.2f})".
              format(test_time, test_time/total_time, train_time, train_time/total_time))

    # pass 2. split the data into train and test file
    name, _ = os.path.splitext(os.path.basename(groups_file))
    train_fid = open(os.path.join(output_dir, name + "_train.txt"), 'w')
    test_fid = open(os.path.join(output_dir, name + "_test.txt"), 'w')
    num_train = 0
    num_test = 0

    with open(groups_file, 'r') as fid:
        for line in fid:
            toks = line.rstrip().split()
            if len(toks) == 0:
                continue
            s = float(toks[0])
            if s < segment_1[1] or segment_2[0] <= s < segment_2[1] or s >= segment_3[0]:
                test_fid.write(line)
                num_test += 1
            else:
                train_fid.write(line)
                num_train += 1

    train_fid.close()
    test_fid.close()

    if verbose:
        print("Done. Wrote {} ({}%) test frames, and {} ({}%) train frames.".
              format(num_test, num_test / len(stamps), num_train, num_train / len(stamps)))


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Download data')

    parser.add_argument('-g', '--groups-file', type=str, required=True,
                        help="path to groups file")
    parser.add_argument('-o', '--output-dir', type=str, default="",
                        help="output directory (default: same as groups file)")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='Verbose output? (default: False)')

    args = parser.parse_args()
    return args


def main(args):

    groups_file = args.groups_file
    groups_dir = os.path.dirname(groups_file)
    if len(args.output_dir) == 0:
        output_dir = groups_dir
    else:
        output_dir = args.output_dir

    if args.verbose:
        print("Loading", groups_file)
        print("Output directory is", output_dir)

    split_data(groups_file, output_dir, args.verbose)

if __name__ == '__main__':
    args = get_params()
    main(args)
    sys.exit(0)





