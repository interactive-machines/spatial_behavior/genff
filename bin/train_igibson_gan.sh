# Train on iGibson
IGIBSON_H5="data/igibson_sim_data/env_Beechwood_0_int.h5 \
data/igibson_sim_data/env_Beechwood_1_int.h5 \
data/igibson_sim_data/env_Benevolence_1_int.h5 \
data/igibson_sim_data/env_Ihlen_0_int.h5 \
data/igibson_sim_data/env_Ihlen_1_int.h5 \
data/igibson_sim_data/env_Merom_1_int.h5 \
data/igibson_sim_data/env_Pomaria_1_int.h5 \
data/igibson_sim_data/env_Pomaria_2_int.h5 \
data/igibson_sim_data/env_Rs_int.h5 \
data/igibson_sim_data/env_Wainscott_0_int.h5 \
data/igibson_sim_data/env_Wainscott_1_int.h5 \
data/igibson_sim_data/env_Merom_0_int.h5 \
data/igibson_sim_data/env_Benevolence_2_int.h5 \
data/igibson_sim_data/env_Benevolence_0_int.h5 \
data/igibson_sim_data/env_Pomaria_0_int.h5"

CUDA_VISIBLE_DEVICES=0 \
python -m genff.gan_method.train_gan -i ${IGIBSON_H5} --gpenalty 10.0 --epochs 600 --weight-samples --ploss 0 \
  --plot-frequency 20 --angular-noise

# Train on iGibson + Warped
WARPED_H5="data/igibson_sim_data/env_Beechwood_0_int_warped.h5 \
data/igibson_sim_data/env_Beechwood_1_int_warped.h5 \
data/igibson_sim_data/env_Benevolence_1_int_warped.h5 \
data/igibson_sim_data/env_Ihlen_0_int_warped.h5 \
data/igibson_sim_data/env_Ihlen_1_int_warped.h5 \
data/igibson_sim_data/env_Merom_1_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_1_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_2_int_warped.h5 \
data/igibson_sim_data/env_Rs_int_warped.h5 \
data/igibson_sim_data/env_Wainscott_0_int_warped.h5 \
data/igibson_sim_data/env_Wainscott_1_int_warped.h5 \
data/igibson_sim_data/env_Merom_0_int_warped.h5 \
data/igibson_sim_data/env_Benevolence_2_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_0_int_warped.h5"

CUDA_VISIBLE_DEVICES=1 \
python -m genff.gan_method.train_gan -i ${IGIBSON_H5} ${WARPED_H5} --gpenalty 10.0 --epochs 600 --weight-samples --ploss 0 \
  --plot-frequency 20 --angular-noise