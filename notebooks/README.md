# Description of Jupyter Notebooks

- **check_context_in_occupied_space.ipynb**: Notebook to count the number of samples in the Cocktail Party dataset, simulated data on iGibson environments and augmented simulated data on iGibson environments that are located in non-free space.

- **create_warped_images.ipynb**: Notebook to draw figures for the augmentation of simulated data on iGibson environments. Images are saved to ```notebooks```.

- **evaluation.ipynb**: Notebook to compute quantitative metrics for each of the pose generation methods (Yang baseline, geometric and data-driven). For each method, a CSV containing the results is saved to ```notebooks/eval_logs```

- **GAN_iterative_group.ipynb**: Notebook that implements iterative group generation. Either the geometric or the data-driven pose generator can be invoked iteratively to construct an entire conversational group. Figures that illustrate the iterative group generation are saved to ```notebooks```.

- **igibson_dataset_statistics.ipynb**: Notebook to explore the group sizes and group distributions of the simulated data on iGibson environments.

- **mean_shift_clustering.ipynb**: Notebook to demonstrate the selection of a final pose as the mode of a distribution of poses.

- **metrics.ipynb**: Notebook to demonstrate the quantitative metrics used to evaluate the quality of a generated pose with respect to an existing social context and environment map.

- **plot_losses.ipynb**: Notebook to draw figures for generated poses in environments and for components of the loss function used by the model-based pose generator. Images are saved to ```notebooks```.

- **prolific_exp_data.ipynb**: Notebook to create CSV files of conversational interactants to be rendered by Unity. CSV files are saved to ```notebooks/csv_files```.

- **rule_based_eval_train.ipynb**: Notebook to search for good parameters for the weights of the losses used by the model-based pose generator. Results computed during training are saved to ```notebooks/rule_based_eval```.

- **rule_based_generative_model.ipynb**: Notebook to explore the implementation of the model-based pose generator.

- **visualize_groupdataset.ipynb**: Notebook to display images of social contexts from the Cocktail Party dataset (?)

- **warp_dataset_samples.ipynb**: Notebook to demonstrate the data augmentation techniques performed on simulated data on iGibson environments.

- **weighted_sampler_cocktail_party.ipynb**: Notebook to illustrate the distribution of group sizes before and after sample weighting.

- **yang_circle_method.ipynb**: Notebook to implement the pose generation method proposed by Yang et al., which we employ as a baseline method.
```
@INPROCEEDINGS{8206105,  
  author={S. {Yang} and E. {Gamborino} and C. {Yang} and L. {Fu}},  
  booktitle={2017 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},   
  title={A study on the social acceptance of a robot in a multi-human interaction using an F-formation based motion model},   
  year={2017},  
  volume={},  
  number={},  
  pages={2766-2771},  
  doi={10.1109/IROS.2017.8206105}}```
