#!/usr/bin/env python
# Script to download data for the project
import os
import sys
import argparse
import gdown
from collections import OrderedDict

data_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)))

def download_igibson_data():
    print("Downloading iGibson dataset from Google Drive...")
    igibson_dict = OrderedDict()

    # original dataset
    igibson_dict['env_Beechwood_0_int.h5'] = 'https://drive.google.com/uc?id=1-DLn1q_WeKFtX3zhbTgJ_9awc35PRanf'
    igibson_dict['env_Benevolence_2_int.h5'] = 'https://drive.google.com/uc?id=1BP1GM8IKTHvyAOte8ZAxfM474tw0KbnD'
    igibson_dict['env_Beechwood_1_int.h5'] = 'https://drive.google.com/uc?id=1HhWI25w1PA_V83rGG6GT0d_cOMaElHlE'
    igibson_dict['env_Benevolence_1_int.h5'] = 'https://drive.google.com/uc?id=1Jij3aL8TqK1fncuqEyA1L4gOASU-M68x'
    igibson_dict['env_Pomaria_1_int.h5'] = 'https://drive.google.com/uc?id=1MtiAUuxaJZ3WhXmYdBZXrV-rsg6tP444'
    igibson_dict['env_Merom_1_int.h5'] = 'https://drive.google.com/uc?id=1Pn2xkpkOTk_Err1C3tyyDG0zjS0xNch-'
    igibson_dict['env_Ihlen_1_int.h5'] = 'https://drive.google.com/uc?id=1RL3M9OHLvj577N8_bYM8ZzrOyrXFQmOZ'
    igibson_dict['env_Merom_0_int.h5'] = 'https://drive.google.com/uc?id=1gJMWfh8A0eJNoWc-wIk5DuMe_qT5ccRB'
    igibson_dict['env_Benevolence_0_int.h5'] = 'https://drive.google.com/uc?id=1i-M3pcKWVqVc3gLA8TzaRbnnXqXVdPnt'
    igibson_dict['env_Pomaria_0_int.h5'] = 'https://drive.google.com/uc?id=1se7vdSPAwUZ7DgiLTyxdfPc97cFsB_Zu'
    igibson_dict['env_Ihlen_0_int.h5'] = 'https://drive.google.com/uc?id=1z1wjca_pLbnwNnuVJHHPEt6H8iSfDUJK'
    igibson_dict['env_Wainscott_0_int.h5'] = 'https://drive.google.com/uc?id=10u-FCErNos3MWFgzVCJs0xKwYaHZuHRX'
    igibson_dict['env_Rs_0_int.h5'] = 'https://drive.google.com/uc?id=1hXEtbempu9GoN1Mprq9xcEpwiR6Bs2G3'
    igibson_dict['env_Pomaria_2_int.h5'] = 'https://drive.google.com/uc?id=1m-6BpPlJtP-EF1RdY9sgZeyTtUkS7DCd'
    igibson_dict['env_Wainscott_1_int.h5'] = 'https://drive.google.com/uc?id=1oeKTZhyKk-1odDTZ9Gcm_IQPLbCikhTe'

    # dataset with stretching and rotation
    igibson_dict['env_Beechwood_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1H_HoxnJxil4iTuQFfJdsNRq5Uc31qgHq'
    igibson_dict['env_Beechwood_1_int_warped.h5'] = 'https://drive.google.com/uc?id=15G3BRw3F8W_o_VHwV68rjMUmm6GEzGnw'
    igibson_dict['env_Benevolence_1_int_warped.h5'] = 'https://drive.google.com/uc?id=15Wr5DjHvu2nJzQ4pIdRh7nabY06NLua3'
    igibson_dict['env_Benevolence_2_int_warped.h5'] = 'https://drive.google.com/uc?id=1TsE9uYc-Nd21LfSaPA-S_43HdQ9_IaQD'
    igibson_dict['env_Ihlen_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1U8qo-pld4k5dnXVs33Af0CCfkBHOwdQH'
    igibson_dict['env_Ihlen_1_int_warped.h5'] = 'https://drive.google.com/uc?id=1gThrGHB15njCgKH3CZoUCbT1_pC2cK4O'
    igibson_dict['env_Merom_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1gThrGHB15njCgKH3CZoUCbT1_pC2cK4O'
    igibson_dict['env_Merom_1_int_warped.h5'] = 'https://drive.google.com/uc?id=1gThrGHB15njCgKH3CZoUCbT1_pC2cK4O'
    igibson_dict['env_Pomaria_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1gThrGHB15njCgKH3CZoUCbT1_pC2cK4O'
    igibson_dict['env_Pomaria_1_int_warped.h5'] = 'https://drive.google.com/uc?id=1b6LDFJXb6vqxyXMrnqqVzSXfLZL14qrm'
    igibson_dict['env_Pomaria_2_int_warped.h5'] = 'https://drive.google.com/uc?id=1ax4rFyHDjKdg_oK2eu83K0dP_5UNi-TC'
    igibson_dict['env_Rs_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1ax4rFyHDjKdg_oK2eu83K0dP_5UNi-TC'
    igibson_dict['env_Wainscott_0_int_warped.h5'] = 'https://drive.google.com/uc?id=1r2mXtMd6BrOXVQALjKTDwvCmnegDQnHl'
    igibson_dict['env_Wainscott_1_int_warped.h5'] = 'https://drive.google.com/uc?id=17neyKm5GtNPVT7hykVUnvBmdD7vWbcKT'

    output_folder = os.path.join(data_folder, 'igibson_sim_data')
    if not os.path.exists(output_folder):
        print("\tCreated the folder {}".format(output_folder))
        os.makedirs(output_folder)
    else:
        print("\tSaving files in the folder {}".format(output_folder))

    # download files
    for k in igibson_dict.keys():
        out_path = os.path.join(output_folder, k)
        gdown.download(igibson_dict[k], out_path, quiet=False)


def download_cp_data():
    print("Downloading Cocktail Party dataset from Google Drive...")

    output_folder = os.path.join(data_folder, 'cocktail_party')

    cp_dict = OrderedDict()
    cp_dict['cocktail_party_train.h5'] = 'https://drive.google.com/uc?id=1iNToKkTkM9YaSQyZzFdBP0wTTAZmmece'
    cp_dict['cocktail_party_test.h5'] = 'https://drive.google.com/uc?id=1JXBQNgD00enqaFoCblR6V_rexd431f1U'
    cp_dict['train_skip3.h5'] = 'https://drive.google.com/uc?id=13QDIYdeKH_OxouhOieJHN0OVwH0A0o_f'
    cp_dict['cocktail_party_train_centers.h5'] = 'https://drive.google.com/uc?id=1u_27aJ3m-q1gMGqI4siDM9xuEwwXiq03'
    cp_dict['cocktail_party_test_centers.h5'] = 'https://drive.google.com/uc?id=1hQ6uBVaEAcVbQqlWJP3hVXcRe1Fj6dbC'  # used for full group generation

    output_folder = os.path.join(data_folder, 'cocktail_party')
    print("\tSaving files in the folder {}".format(output_folder))

    # download files
    for k in cp_dict.keys():
        out_path = os.path.join(output_folder, k)
        gdown.download(cp_dict[k], out_path, quiet=False)

def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Generate simulated data. The data will get printed to stdout.')

    parser.add_argument('--all-data', action='store_true', default=False,
                        help='download all of the data?')
    parser.add_argument('--igibson-data', action='store_true', default=False,
                        help='download iGibson datasets (h5 files)?')
    parser.add_argument('--cp-data', action='store_true', default=False,
                        help='download Cocktail Party datasets (h5 files)?')
    args = parser.parse_args()
    return args


def main(args):
    """
    Main function
    """

    if args.all_data or args.igibson_data:
        download_igibson_data()

    if args.all_data or args.cp_data:
        download_cp_data()

    if not args.all_data and not args.igibson_data and not args.cp_data:
        print("No data is being downloaded. Check the input arguments (try --help).")

if __name__ == "__main__":
    args = get_params()
    main(args)
    sys.exit(0)
