## Data for the Pose Generation project

In this project, we use several different types of data, including:

- Layouts for iGibson environments (in png format), which can be downloaded from this 
  [GDrive folder](https://drive.google.com/drive/folders/13JmpWbr8JEcpZMtsI-AsAu4LWdqZlRsd?usp=sharing).
- Environment maps for iGibson environments (in json format), which can be downloaded from this 
  [GDrive folder](https://drive.google.com/drive/folders/1O4tVp5R-MxOGLeD7uWCVhm1do1s2MYAG?usp=sharing).
- Simulated group dataset (in h5 format), which was generated using iGibson environment maps.
- Real group datasets (in h5 format) from the [Cocktail Party Dataset](https://tev.fbk.eu/technologies/cocktailparty-dataset-multi-view-dataset-social-behavior-analysis)

For convenience, all the processed data can be downloaded by running the script `download_data.py`. For example:
```bash
$ python download_data.py --all-data
```

Below, we provide details on how each of these datasets were generated.

### Generating Simulated Data

We generated simulated group data in environments maps from the [iGibson simulator](http://svl.stanford.edu/igibson/),
which were created as described in the [igibson_maps](../igibson_maps) folder of this repository.
Once we had the maps,  we ran the `bin/make_sim_data.py` script on all JSON environments using a 
simple bash for loop:

```bash
cd .. # go to the top level of this repository
for i in data/igibson_maps/*.txt; do
  name=`basename $i .txt`;
  python3 bin/make_sim_data.py -n 2000 --seed 1 --env-file $i --output-file igibson_sim_data/env_${name}.h5
done
```

Once we had generated h5 files for the iGibson layouts, we augmented this data by stretching and rotating it. This
was done with the `/bin/warp_data.py` script as follows:
```bash
cd .. # go to the top level of this repository

for i in data/igibson_sim_data/env_*.h5; do 
  echo $i; ./bin/warp_data.py -i ${i}  --no-dyads; 
done
```

### Generating Cocktail Party Data

We first split the [Cocktail Party Dataset](https://tev.fbk.eu/technologies/cocktailparty-dataset-multi-view-dataset-social-behavior-analysis)
group annotations into a training and testing set, using the `/bin/split_cocktailparty_groups.py` script in this
repository. Then, we created dataset files for each set in h5 format using the `/bin/make_cocktail_data.py` script,
which took as input the corresponding groups (train or test), the features for people in the dataset (included within 
the `cocktail_party` folder with body orientation information), and the environment map for the Cocktail Party scenario
(included within the `cocktail_party` folder in json format). More specifically:

```bash
cd .. # go to the top level of this repository

# split cocktail party data (creates groups_train.txt file)
python bin/split_cocktailparty_groups.py -g data/cocktail_party/groups.txt

# create training dataset
python bin/make_cocktail_data.py \
  --features-file data/cocktail_party/features.txt \
  --groups-file data/cocktail_party/groups_train.txt \
  --env-file data/cocktail_party/env_cocktail_party.txt \
  --output-file data/cocktail_party/output_data.h5 \
  --use-body-orientation
```

### Inspecting a dataset h5 file

All dataset files are [HDF5 files](https://en.wikipedia.org/wiki/Hierarchical_Data_Format). They can be easily
inspected using the `ddls` tool from the [DeepDish library](https://deepdish.readthedocs.io/) (which is a 
dependency for this project). For example, running the command `ddls <h5 file>` should print a description of the
content of the file, as shown below:

```bash
ddls cocktail_party/cocktail_party_test_centers.h5 
/centers              pickled [object]
/cmaps                array (347, 24, 24, 4) [float32]
/cwidth               24 [int64]
/env                  dict
/env/grid             array (28, 24) [int64]
/env/labels           dict
/env/labels/0         'empty' (5) [unicode]
/env/labels/1         'occupied (tall)' (15) [unicode]
/env/labels/2         'occupied (short)' (16) [unicode]
/env/left             -0.69 [float64]
/env/pixels_per_cell  10 [int64]
/env/resolution       0.25 [float64]
/env/top              -0.65 [float64]
/groups               pickled [object]
```

Each of the fields shown above correspond to:

- **centers:** coordinates for the group's context center in the full map (only some datasets have this field, as it was used for the full group generation experiment only)
- **cmaps:** cropped maps for the examples in the dataset
- **cwidth:** cropped map width
- **env:** full environment map (/grid) with a certain number of labels (/labels) and origin coordinates (/left). 
- **groups:** poses for the examples in the dataset

