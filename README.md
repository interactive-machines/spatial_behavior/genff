# Pose Generation for Conversational Group Formations

This repository has the code for the paper entitled `Pose Generation for Social Robots in Conversational Group Formations`, by M. Vazquez, A. Lew, E. Gorevoy, and J. Connolly. The code was implemented in Ubuntu 18.04.

## Repository Organization

- **bin** (helper scripts)
- **data** (contains information about data used in our evaluation)
- **documentation**
- **genff** (main source code, written in Python)
- **igibson_maps** (code to generate iGibson layouts)
- **notebooks** (example usage for the genff code and helper scripts)
- **vonmiseskde** (external code for computing von mises kde)
- **human_evaluation** (code used to generate renderings for the human evaluation)
- requirements.txt (main Python requirements)

## Setup

### Installing Virtual Environment and PIP Dependencies
Simply run:

```bash
# Create virtual environment for this project
$ cd generating_fformations
$ python3 -m venv venv

# Source environment and install dependencies
$ source venv/bin/activate
(venv) $ pip install --upgrade pip  # upgrade pip
(venv) $ pip install -r requirements.txt
```

### Installing Submodules 
The code for the human evaluation is included in this repository as a submodule:

```bash
# Initialize submodules
(venv) $ git submodule update --init --recursive
```

You can then access the code within the `human_evaluation` folder. Note that this
code requires [ROS Melodic 18.04](http://wiki.ros.org/melodic) and [Unity v. 2019.4.0f1](https://unity3d.com/get-unity/download/archive?_ga=2.68276945.1711888716.1619797524-1476923395.1560095987).

## Testing models

First, download models files from this [GDrive wgan_models folder](https://drive.google.com/drive/folders/1_o0YMrVbw4vSUysHOHn1GE3LrsJbt3UH?usp=sharing). 
Place the folder at the root level of this repository. Then, checkout the notebook `notebooks/example_run_model.ipynb`.
Note that you can run [Jupyter](https://jupyter.org/) to test the code in the notebook by simply executing:
```bash
jupyter notebook
```

## Training WGAN models

First, download the training data using the instructions within the [data folder](data) of this repository. Then, 
you can train a model using the `python -m genff.gan_method.train_gan` command. For example, to train
a WGAN on all of the iGibson data, run can run:

```
# Train on iGibson
IGIBSON_H5="data/igibson_sim_data/env_Beechwood_0_int.h5 \
data/igibson_sim_data/env_Beechwood_1_int.h5 \
data/igibson_sim_data/env_Benevolence_1_int.h5 \
data/igibson_sim_data/env_Ihlen_0_int.h5 \
data/igibson_sim_data/env_Ihlen_1_int.h5 \
data/igibson_sim_data/env_Merom_1_int.h5 \
data/igibson_sim_data/env_Pomaria_1_int.h5 \
data/igibson_sim_data/env_Pomaria_2_int.h5 \
data/igibson_sim_data/env_Rs_int.h5 \
data/igibson_sim_data/env_Wainscott_0_int.h5 \
data/igibson_sim_data/env_Wainscott_1_int.h5 \
data/igibson_sim_data/env_Merom_0_int.h5 \
data/igibson_sim_data/env_Benevolence_2_int.h5 \
data/igibson_sim_data/env_Benevolence_0_int.h5 \
data/igibson_sim_data/env_Pomaria_0_int.h5"

WARPED_H5="data/igibson_sim_data/env_Beechwood_0_int_warped.h5 \
data/igibson_sim_data/env_Beechwood_1_int_warped.h5 \
data/igibson_sim_data/env_Benevolence_1_int_warped.h5 \
data/igibson_sim_data/env_Ihlen_0_int_warped.h5 \
data/igibson_sim_data/env_Ihlen_1_int_warped.h5 \
data/igibson_sim_data/env_Merom_1_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_1_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_2_int_warped.h5 \
data/igibson_sim_data/env_Rs_int_warped.h5 \
data/igibson_sim_data/env_Wainscott_0_int_warped.h5 \
data/igibson_sim_data/env_Wainscott_1_int_warped.h5 \
data/igibson_sim_data/env_Merom_0_int_warped.h5 \
data/igibson_sim_data/env_Benevolence_2_int_warped.h5 \
data/igibson_sim_data/env_Pomaria_0_int_warped.h5"

python -m genff.gan_method.train_gan -i ${IGIBSON_H5} ${WARPED_H5} \
    --gpenalty 10.0 --epochs 600 --weight-samples --ploss 0 --plot-frequency 20 --angular-noise
```

Training logs and model files will then be created inside of a `logs` folder in this repository. You can
inspect them using [TensorBoard](https://www.tensorflow.org/tensorboard).