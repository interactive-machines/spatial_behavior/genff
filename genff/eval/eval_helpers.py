"""
Helper functions to conduct evaluations
"""

from collections import OrderedDict
import numpy as np
import pandas as pd
import time
import os
import random
import torch
import matplotlib.pyplot as plt
from genff.data_handling.transform import RectangularAngle
from genff.opt_method.opt_generator import opt_generator_loss, torch_to_numpy
from genff.visualization.plotting import plot_sample
from genff.metrics import *
from genff.mode_estimation.mode_estimation import find_modes


def create_metrics_dict():
    """
    Helper function for creating a dictionary with relevant quantitative metrics
    """
    metrics = OrderedDict()
    metrics['CircFit'] = CircFitMetric()
    metrics['NotFree'] = NotFreeMetric()
    metrics['PerSpace'] = PersonalSpaceMetric()
    metrics['IntSpace'] = IntimateSpaceMetric()
    metrics['CenterDist'] = CenterDistMetric()
    metrics['OccludesOther'] = OccludesOtherMetric()
    metrics['IsOccluded'] = IsOccludedMetric()
    return metrics


# def center_sample(individual, context, mask, env, target):
#     """
#     Helper function to center a sample's data
#     :param individual: individual tensor
#     :param context: context tensor
#     :param mask: mask tensor
#     :param env: environment tensor
#     :param target: target tensor
#     :return centered individual, centered context, centered mask, env, mean of context positions
#     """
#     center_transform = TranslateToCentroid()
#     data, mean = center_transform.center_and_return_offset(((individual.unsqueeze(axis=0),
#                                                              context.unsqueeze(axis=0),
#                                                              mask.unsqueeze(axis=0),
#                                                              env.unsqueeze(axis=0)), target))
#     individual = data[0][0].squeeze(axis=0)
#     context = data[0][1].squeeze(axis=0)
#     mask = data[0][2].squeeze(axis=0)
#     return individual, context, mask, env, mean


def compute_results(gen_fun, dataset, method_name, metrics, solitary_sample=False, x0=None, draw_result=False, output_folder="eval_logs", metadata={},
                    verbose=True):
    """
    Helper function to compute results (one example at a time)
    :param gen_fun: function that computes generated individual
    :param dataset: dataset with samples
    :param method_name: name of the method being used to compute results
    :param metrics: metrics dictionary (values should be instances of genff.metrics.quant_metrics.BaseFFormationMetric)
    :param draw_result: True for saving plots of the results
    :param solitary_sample: True for evaluation over a single sample rather than distribution
    :param output_folder: output folder for saved plots
    :param metadata: dict (or OrderedDict) with additional values to store in df (has to have single value per key)
    :param verbose: print information as the data is being processed?
    :return: data frame with results per metric

    :note gen_fun should take as inputs ind, cont, mask, env, res. It should return individual as torch tensor.
    """
    n = len(dataset)  # number of examples

    results = OrderedDict()
    results['example_id'] = [x for x in range(n)]
    results['stamp'] = dataset.stamps
    # results['individual_id'] = [x[1] + 1 for x in dataset.labeled_data_stamps]
    results['exec_time'] = []
    results['method'] = [method_name for _ in range(n)]

    if bool(metadata):
        metadata_cols = list(metadata.keys())
        for k in metadata.keys():
            results[k] = [metadata[k] for _ in range(n)]
    else:
        metadata_cols = []

    if solitary_sample:
        N = x0.shape[0]
        indices = [i for i in range(N)]
        random.seed(0)

    for i in results['example_id']:

        if verbose:
            print("\rProcessing example {}/{}".format(i + 1, len(dataset)), end="... ")

        individual, context, mask, env = dataset[i]

        start_time = time.time()
        if solitary_sample:
            sample_space = np.reshape(x0[indices[random.randint(0,N-1)]], (1,2))
            gen_ind = gen_fun(individual, context, mask, env, dataset.resolution, x0=sample_space)
        else:
            gen_ind = gen_fun(individual, context, mask, env, dataset.resolution)
        end_time = time.time()
        results['exec_time'].append(end_time - start_time)

        if draw_result:
            method_folder = os.path.join(output_folder, method_name.lower().replace(" ", "_"))
            if not os.path.isdir(method_folder):
                os.mkdir(method_folder)

            plt.clf()
            plot_sample(gen_ind, context, mask,
                        env_cropped=env, env_res=dataset.resolution)
            plt.savefig(os.path.join(method_folder, "example_%03d" % i), bbox_inches='tight')

        for m in metrics.keys():

            if m not in results.keys():
                results[m] = []

            # compute metric
            value = metrics[m](gen_ind, context, mask, env, dataset.resolution)
            results[m].append(value)

    if draw_result:
        plt.close()

    if verbose:
        print("Done.")

    columns = ['method', 'example_id', 'stamp'] + list(metrics.keys()) + ['exec_time'] + metadata_cols
    df = pd.DataFrame(results, columns=columns)

    return df

def compute_generator_results(gen_fun, dataset, method_name, metrics, mode_samples=10, draw_result=False,
                              output_folder="eval_logs", metadata={}, verbose=True):
    """
    Helper function to compute results (one example at a time)
    :param gen_fun: function that computes generated individual
    :param dataset: dataset with samples
    :param method_name: name of the method being used to compute results
    :param metrics: metrics dictionary (values should be instances of genff.metrics.quant_metrics.BaseFFormationMetric)
    :param mode_samples: number of samples used to determine final output (by mean)
    :param draw_result: True for saving plots of the results
    :param output_folder: output folder for saved plots
    :param metadata: dict (or OrderedDict) with additional values to store in df (has to have single value per key)
    :param verbose: print information as the data is being processed?
    :return: data frame with results per metric

    :note gen_fun should take as inputs ind, cont, mask, env, res. It should return individual as torch tensor.
    """
    gen_fun.eval()
    n = len(dataset)  # number of examples
    rect_angle = RectangularAngle(angle_index=2)

    results = OrderedDict()
    results['example_id'] = [x for x in range(n)]
    results['stamp'] = dataset.stamps
    # results['individual_id'] = [x[1] + 1 for x in dataset.labeled_data_stamps]
    results['exec_time'] = []
    results['method'] = [method_name for _ in range(n)]

    if bool(metadata):
        metadata_cols = list(metadata.keys())
        for k in metadata.keys():
            results[k] = [metadata[k] for _ in range(n)]
    else:
        metadata_cols = []

    # center_transform = TranslateToCentroid()

    for i in results['example_id']:

        if verbose:
            print("\rProcessing example {}/{}".format(i + 1, len(dataset)), end="... ")

        individual, context, mask, env = dataset[i]
        rect_individual, rect_context, rect_mask, rect_env = rect_angle((individual.unsqueeze(0), context.unsqueeze(0), mask.unsqueeze(0), env.unsqueeze(0)))

        # center sample
        # individual, context, mask, env, _ = center_sample(individual, context, mask, env, target)
        # data = center_transform(((individual.unsqueeze(axis=0),
        #                           context.unsqueeze(axis=0),
        #                           mask.unsqueeze(axis=0),
        #                           env.unsqueeze(axis=0)), target))
        # individual = data[0][0].squeeze(axis=0)
        # context = data[0][1].squeeze(axis=0)
        # mask = data[0][2].squeeze(axis=0)

        start_time = time.time()
        dist_samples = []

        for sample in range(mode_samples):
            out = gen_fun(rect_context, rect_mask, rect_env)
            dist_samples.append(out)

        dist_tensor = torch.cat(dist_samples)
        if mode_samples > 1:
            dist_np = dist_tensor.detach().squeeze(1).numpy()
            dist_samples = np.zeros((mode_samples, 3))
            dist_samples[:, 0:2] = dist_np[:, 0:2]
            dist_samples[:, 2] = np.arctan2(dist_np[:, 3], dist_np[:, 2])

            modes = find_modes(dist_samples, bandwidth=0.8, threshold=0.3, kappa=1.0)
            mode = np.zeros((1,4))
            mode[:,[0, 1]] = modes[0,0:2]
            mode[:,2] = np.cos(modes[0,2])
            mode[:,3] = np.sin(modes[0,2])
            gen_ind = torch.from_numpy(mode).unsqueeze(0)
        else:
            gen_ind = dist_tensor[0].unsqueeze(0)

        end_time = time.time()
        results['exec_time'].append(end_time - start_time)

        if draw_result:
            method_folder = os.path.join(output_folder, method_name.lower().replace(" ", "_"))
            if not os.path.isdir(method_folder):
                os.mkdir(method_folder)

            plt.clf()
            plot_sample(gen_ind, context, mask,
                        env_cropped=env, env_res=dataset.resolution)
            plt.savefig(os.path.join(method_folder, "example_%03d" % i), bbox_inches='tight')

        for m in metrics.keys():

            if m not in results.keys():
                results[m] = []

            # compute metric
            value = metrics[m](gen_ind.squeeze(1), context, mask, env, dataset.resolution)
            results[m].append(value)

    if draw_result:
        plt.close()

    if verbose:
        print("Done.")

    columns = ['method', 'example_id', 'stamp'] + list(metrics.keys()) + ['exec_time'] + metadata_cols
    df = pd.DataFrame(results, columns=columns)

    return df
