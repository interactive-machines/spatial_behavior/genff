"""
Generator functions to be used with genff.eval.eval_helpers.compute_results()
"""
import torch
import numpy as np
from genff.opt_method.opt_generator import opt_generator
from genff.yang_method.yang_method import yang_generator
from genff.mode_estimation.mode_estimation import find_modes


def gen_gt(individual, context, mask, env, resolution):
    """
    Ground truth gen fun
    :param individual: individual tensor
    :param context: context tensor
    :param mask: mask tensor
    :param env: environment tensor
    :param resolution: environment resolution
    :return generated individual
    """
    return individual


def gen_opt(individual, context, mask, env, resolution, x0, wpsloss=1.0, wcloss=0.2, weloss=1.0):
    """
    Optimization method
    :param individual: individual tensor
    :param context: context tensor
    :param mask: mask tensor
    :param env: environment tensor
    :param resolution: environment resolution
    :param x0: initial values for the optimization
    :param wpsloss: weight for personal space loss
    :param wcloss: weight for circular loss
    :param weloss: weight for environment loss
    :return generated individual

    :note to be used with lamba to remove extra params. For example:
    (lambda i, c, m, e, r: gen_opt(i, c, m, e, r, <x0>, <wpsloss>, <wcloss>, <weloss>)
    """
    gen_ind, final_losses, _ = opt_generator(context, mask, emap=env, eres=resolution,
                                             x0=x0, x_index=0, y_index=1, angle_index=2, ospace_dist=0.72,
                                             weight_psloss=wpsloss, weight_closs=wcloss, weight_eloss=weloss,
                                             verbose=False)
    # pick min loss
    if x0.shape[0] > 1:
        modes = find_modes(gen_ind, bandwidth=0.8, threshold=0.3, kappa=1.0)
        return torch.from_numpy(modes[0]).unsqueeze(0)
    else:
        best_index = np.argmin(final_losses)
        return torch.from_numpy(gen_ind[best_index:best_index+1,:])


def gen_yang(individual, context, mask, env, resolution):
    pred_individual = yang_generator(context, mask)
    return torch.from_numpy(pred_individual)
