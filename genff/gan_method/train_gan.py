import sys
import os
import json
from datetime import datetime
import argparse
from argparse import Namespace
import torch
import time
import numpy as np
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from genff.gan_method.personal_space_loss import PersonalSpaceLoss
from genff.data_handling.dataset import GroupDataset
from genff.data_handling.utils import create_weighted_sampler
from genff.data_handling.transform import RectangularAngle, AddNoiseToContextAngles
from genff.gan_method.networks import GenNet, DiscNet
from genff.gan_method.tensorboard import log_losses, log_sample_images


def get_params():
    """
    Script parameters
    :return: arguments from argparse
    """
    parser = argparse.ArgumentParser(description='Generate simulated data. The data will get printed to stdout.')

    parser.add_argument('-i', '--input-file', nargs='+', required=True,
                        help="input h5 file(s) with training data")
    parser.add_argument('--model-path', type=str,
                        help='models logfile (Format: <datetime>_<iteration> (without .pt at the end)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('-b', '--batch-size', type=int, default=32,
                        help="batch size")
    parser.add_argument('--critic-iters', type=int, default=5,
                        help="number of iterations for the critic")
    parser.add_argument('-e', '--epochs', type=int, default=100,
                        help='Number of training epochs (default: 100)')
    parser.add_argument('--train-prop', type=float, default=0.9,
                        help='Percentage of dataset reserved for training (default: 0.9)')
    parser.add_argument('--lrd', type=float, default=0.00002,
                        help='Learning rate for the discriminator (default: 0.00002)')
    parser.add_argument('--lrg', type=float, default=0.00002,
                        help='Learning rate for the generator (default: 0.00002)')
    parser.add_argument('--decay-lr', type=float, default=0,
                        help='Decay for learning rates (default: 0 -- no decay)')
    parser.add_argument('--gpenalty', type=float, default=0.0,
                        help='Weighting factor for gradient penalty (default: 0.0 -- no penalty)')
    parser.add_argument('--plot-sigma', type=float, default=0.05,
                        help='Std dev for plotting people as gaussians (default: 0.05)')
    parser.add_argument('--combine-env-channels', action='store_true', default=False,
                        help='combine tall and short obstacles in the environment map')
    parser.add_argument('--ploss', type=float, default=0.1,
                        help='Multiplier for personal space loss (default: 0.1)')
    parser.add_argument('--num-images', type=int, default=12,
                        help='Number of generated images plotted in tensorboard per save (default: 16)')
    parser.add_argument('--plot-frequency', type=int, default=10,
                        help='Plot images every X epochs (default: 10 epochs)')
    parser.add_argument('--log-frequency', type=int, default=2,
                        help='Log scalar losses every X epochs (default: 2 epochs)')
    parser.add_argument('--checkpoint-frequency', type=int, default=25,
                        help='Saving frequency for checkpoints (default: 25 epochs)')
    parser.add_argument('--weight-samples', action='store_true', default=False,
                        help='weight samples based on group size')
    parser.add_argument('--angular-noise', action='store_true', default=False,
                        help='add angular noise to the context?')
    parser.add_argument('--angular-noise-sigma', type=float, default=0.349066,
                        help='sigma for context angular noise (default: 0.349066 (20 deg in radians))')

    args = parser.parse_args()
    return args


def load_args(path):
    # save params
    with open(path, 'r') as fid:
        return json.load(fid)


def save_args(path, args):
    # save params
    with open(path, 'w+') as fid:
        json.dump(args.__dict__, fid, indent=2)
    print("Network args saved in '{}'".format(path))


def create_networks_and_optimizers(args, device, env, grid_res, grid_w):
    """
    Helper function to create networks
    :param arg: argparse arguments
    :param device: training device
    :param env: environment dictionary for new model
    :param grid_res: environment resolution
    :param grid_w: grid width for new model
    :return: critic model, generator model, optimizer for the critic, and optmizer for the generator
    """
    if hasattr(args, "grid_res") and hasattr(args, "grid_w"):
        # Loaded model
        args.grid_w = int(args.grid_w)  # conversion necessary to work with old models..
        critic = DiscNet(args.grid_res, args.grid_w, args.grid_w, args.combine_env_channels, sigma=args.plot_sigma)
        generator = GenNet(args.grid_res, args.grid_w, args.grid_w, args.combine_env_channels, sigma=args.plot_sigma)
    else:
        # New model
        setattr(args, "grid_res", grid_res)
        setattr(args, "grid_w", int(grid_w))
        critic = DiscNet(grid_res, grid_w, grid_w, args.combine_env_channels, sigma=args.plot_sigma)
        generator = GenNet(grid_res, grid_w, grid_w, args.combine_env_channels, sigma=args.plot_sigma)

    print("-- Models --")
    print(critic)
    print(generator)

    # move networks to device
    critic.to(device)
    generator.to(device)

    opt_genenerator = torch.optim.Adam(generator.parameters(), lr=args.lrg, betas=(0.5, 0.9))
    opt_critic = torch.optim.Adam(critic.parameters(), lr=args.lrd, betas=(0.5, 0.9))

    return critic, generator, opt_critic, opt_genenerator


def save_networks(logs_folder, name, epoch, generator, critic, opt_gen, opt_cri):
    """
    Helper function to save the model state dict
    :param logs_folder: folder with logs
    :param name: log name (e.g., stamp)
    :param epoch: epoch
    :param generator: generator model
    :param critic: critic model
    :param opt_gen: optimizer for the generator
    :param opt_cri: optimizer for the critic
    """
    output_path = os.path.join(logs_folder, "{}_{}.pt".format(name, epoch))
    torch.save({
        'generator_state_dict': generator.state_dict(),
        'critic_state_dict': critic.state_dict(),
        'optimizerG_state_dict': opt_gen.state_dict(),
        'optimizerD_state_dict': opt_cri.state_dict(),
    }, output_path)
    print("Both generator and discriminator were saved to {}".format(output_path))


def calc_gradient_penalty(critic, real_individual, fake_individual, context, mask, env, device):
    """
    Calculate gradient penalty
    :param critic: DiscNet model
    :param real_individual: B x 1 x F real individual
    :param fake_individual: B x 1 x F fake individual
    :param context: B x P x F context
    :param mask: B x P mask for the context
    :param device: torch device
    :return: B x 1 sized pytorch tensor of gradient penalties

    References:
        - https://github.com/caogang/wgan-gp/issues/56
        - https://github.com/pytorch/pytorch/issues/2534
        - https://cvnote.ddlee.cn/2019/09/26/dcgan-wgan-training-loss-curve
    """
    alpha = torch.rand(real_individual.shape, device=device)

    interpolates = alpha * real_individual + ((1 - alpha) * fake_individual)
    interpolates = torch.autograd.Variable(interpolates, requires_grad=True)

    disc_interpolates = critic(interpolates, context, mask, env)

    gradients = torch.autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                                    grad_outputs=torch.ones(disc_interpolates.size(), device=device, requires_grad=False),
                                    create_graph=True, retain_graph=True, only_inputs=True)[0]
    gradients = gradients.view(gradients.shape[0], -1)

    # modification due to https://github.com/caogang/wgan-gp/issues/56 and https://github.com/pytorch/pytorch/issues/2534
    # gradients_flattened = gradients.view(gradients.shape[0], -1)
    # gradient_penalty = ((gradients_flattened.norm(2, dim=1) - 1) ** 2).mean() * self.gpenalty
    gradient_penalty = \
        torch.mean((1. - torch.sqrt(1e-8 + torch.sum(torch.square(gradients), dim=1))) ** 2)

    return gradient_penalty


def update_critic(generator, critic, data_fun, ploss_fun, device, opt_critic, critic_iters, gpenalty,
                  elosses_critic_loss, elosses_critic_penalty, elosses_critic_ploss, elosses_critic):
    """
    Helper function to update the critic
    :param generator: generator model
    :param critic: critic model
    :param data_fun: reference to get_data_and_log_losses()
    :param ploss_fun: function to calculate personal space loss
    :param device: training device
    :param opt_critic: optimizer for the critic
    :param gpenalty: weight for gradient penalty for the critic
    :param critic_iters: number of iterations for the critic
    :param elosses_critic_loss: list into which to store feature differences for the critic
    :param elosses_critic_penalty: list into which to store gradient penalties for the critic
    :param elosses_critic_ploss: list into which to store personal space losses for the critic
    :param elosses_critic: list into which to store the total critic loss (features diff + gpenalty * penalty)
    """

    generator.eval()

    for p in critic.parameters():
        p.requires_grad = True

    iters = 0  # critic iterations
    while iters < critic_iters:
        # clear gradients
        critic.zero_grad()

        # get data
        individual, context, mask, env = data_fun()

        # labels for training
        ones = torch.full((individual.shape[0], 1), 1, device=device, dtype=torch.float32)  # one
        mones = torch.full((individual.shape[0], 1), -1, device=device, dtype=torch.float32)  # minus one

        # forward pass real batch through D
        output_real = critic(individual, context, mask, env)
        output_real.backward(ones)

        # generate fake image batch with G and compute gradients for this batch
        with torch.no_grad():
            fake = generator(context, mask, env)
        output_fake = critic(fake, context, mask, env)
        output_fake.backward(mones)

        # save loss for logging
        critic_loss = output_real - output_fake
        elosses_critic_loss.append(critic_loss)

        # add personal space contribution to loss
        if ploss_fun.alpha > 0:
            p_space_loss = ploss_fun((fake, context, mask)).unsqueeze(1).to(device)
            p_space_loss.backward(ones)
            critic_loss += p_space_loss
            elosses_critic_ploss.append(p_space_loss)

        # add gradient penalty to loss
        if gpenalty > 0:
            penalty = gpenalty * \
                      calc_gradient_penalty(critic, individual, fake, context, mask, env, device)
            penalty.backward()
            critic_loss += penalty
            elosses_critic_penalty.append(penalty.unsqueeze(0))  # we need to unsqueeze here because p has zero shape

        elosses_critic.append(critic_loss)

        # update the critic's weights
        opt_critic.step()

        iters += 1


def update_generator(generator, critic, data_fun, device, opt_genenerator, elosses_generator):
    """
    Helper function to update the generator
    :param generator: generator model
    :param critic: critic model
    :param data_fun: reference to get_data_and_log_losses()
    :param device: training device
    :param opt_genenerator: optimizer for the critic
    :param elosses_generator: list into which to store the generator loss
    """

    for p in critic.parameters():
        p.requires_grad = False

    # clear gradients
    generator.train()
    generator.zero_grad()

    # get data
    individual, context, mask, env = data_fun()

    # labels for training
    ones = torch.full((context.shape[0], 1), 1, device=device, dtype=torch.float32)

    # generate fake image batch with G and compute gradients for this batch
    fake = generator(context, mask, env)
    output_fake = critic(fake, context, mask, env)
    output_fake.backward(ones)

    # update the generator's weights
    opt_genenerator.step()

    elosses_generator.append(-output_fake)


def main(args):

    # load data
    dataset = GroupDataset(args.input_file, rect_repr=False)
    resolution = dataset.resolution
    rect_angle = RectangularAngle(angle_index=2)
    angular_noise = None
    if args.angular_noise:
        angular_noise = AddNoiseToContextAngles(angle_index=2, sigma=args.angular_noise_sigma)

    # split data into train/val
    idx = torch.randperm(len(dataset))
    split_idx = int((args.train_prop)*len(dataset))
    train_idx = idx[:split_idx]
    test_idx = idx[split_idx:]
    train_dataset = torch.utils.data.Subset(dataset, train_idx)
    test_dataset = torch.utils.data.Subset(dataset, test_idx)
    train_dataset.env_dict = dataset.env_dict
    test_dataset.env_dict = dataset.env_dict

    if args.weight_samples:
        sampler = create_weighted_sampler(train_dataset)
        train_loader = torch.utils.data.DataLoader(train_dataset, num_workers=0, batch_size=args.batch_size,
                                                   shuffle=False, sampler=sampler)
    else:
        train_loader = torch.utils.data.DataLoader(train_dataset, num_workers=0, batch_size=args.batch_size,
                                                   shuffle=False)

    # setup training device
    use_cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    # Load model if desired
    if args.model_path:
        loaded_timestamp = "_".join(args.model_path.split("_")[0:2])
        print("Loading pre-trained model from {}...".format(loaded_timestamp))

        # load args for previous model
        loaded_args = load_args("logs/{}/args.txt".format(loaded_timestamp))

        loaded_args = Namespace(**loaded_args)

        assert int(dataset.grid_w) == int(loaded_args.grid_w), \
            "Invalid pre-trained model. The input model was trained on data with a grid width of {}, but the " \
            "current dataset has a grid width of {}".format(loaded_args.grid_w, dataset.grid_w)
        assert int(dataset.resolution) == int(loaded_args.grid_res), \
            "Invalid pre-trained model. The input model was trained on data with a grid resolution of {}, but the " \
            "current dataset has a grid resolution of {}".format(loaded_args.grid_res, dataset.resolution)
        assert str(args.combine_env_channels) == str(loaded_args.combine_env_channels), \
            "Invalid pre-trained model. The input model was trained with combine_env_channels set to {}, but the " \
            "argument set for combine_env_channels was {}".format(loaded_args.combine_env_channels, args.combine_env_channels)

        # define network
        critic, generator, opt_critic, opt_genenerator = create_networks_and_optimizers(loaded_args, device,
                                                                                        dataset.env_dict,
                                                                                        dataset.resolution,
                                                                                        dataset.grid_w)

        # update weights
        checkpoint_path = "logs/{}/{}.pt".format(loaded_timestamp, args.model_path)
        checkpoint = torch.load(checkpoint_path, map_location=device)
        generator.load_state_dict(checkpoint['generator_state_dict'])
        critic.load_state_dict(checkpoint['critic_state_dict'])
        opt_critic.load_state_dict(checkpoint['optimizerD_state_dict'])
        opt_genenerator.load_state_dict(checkpoint['optimizerG_state_dict'])
        print("Done loading generator and discriminator from {}".format(checkpoint_path))

    else:
        # define networks and optimizers
        critic, generator, opt_critic, opt_genenerator = create_networks_and_optimizers(args, device,
                                                                                        dataset.env_dict,
                                                                                        dataset.resolution,
                                                                                        dataset.grid_w)

    # define scheduler for decaying learning rate
    scheduler_critic = None
    scheduler_generator = None
    got_decay_lr = args.decay_lr > 0
    if got_decay_lr:
        scheduler_critic = \
            torch.optim.lr_scheduler.ExponentialLR(opt_critic, gamma=args.decay_lr, last_epoch=-1)
        scheduler_generator = \
            torch.optim.lr_scheduler.ExponentialLR(opt_genenerator, gamma=args.decay_lr, last_epoch=-1)

    # logging
    stamp = '{0:%Y%m%d_%H%M%S}'.format(datetime.now())
    logs_folder = 'logs/{}'.format(stamp)
    print("Tensorboard logs saved in '{}'".format(logs_folder))
    if not os.path.isdir(logs_folder):
        os.makedirs(logs_folder)
    save_args("logs/{}/args.txt".format(stamp), args)
    tbwriter = SummaryWriter(os.path.join(logs_folder))

    # local vars
    epoch = 0                       # current epoch
    data_iter = iter(train_loader)  # iterator for the data loader

    # epoch losses
    elosses_critic = []             # total loss for the critic
    elosses_critic_loss = []        # feature difference for the critic
    elosses_critic_penalty = []     # gradient penalty for the critic
    elosses_critic_ploss = []       # personal space loss for the critic
    elosses_generator = []          # total loss for the generator
    epoch_duration = []             # amount of time that it took to complete an epoch
    start_time = time.time()        # start time for training epoch

    personal_space_loss = PersonalSpaceLoss(alpha=args.ploss, personal_sigma=args.plot_sigma, device=device)

    def get_data_and_log_losses():
        """
        Helper function to get next batch and log losses if a new epoch starts
        """
        nonlocal generator, critic, resolution
        nonlocal epoch, data_iter, device, stamp, args, rect_angle, angular_noise, start_time
        nonlocal elosses_critic, elosses_critic_loss, elosses_critic_penalty, \
                 elosses_critic_ploss, elosses_generator, epoch_duration
        nonlocal got_decay_lr, scheduler_critic, scheduler_generator

        new_epoch = False
        try:
            # get batch
            batch = next(data_iter)
        except StopIteration:
            # try again getting the batch
            data_iter = iter(train_loader)
            batch = next(data_iter)
            new_epoch = True
            epoch += 1

            new_time = time.time()
            epoch_duration.append(new_time - start_time)
            start_time = new_time

        # convert batch to rect angle representation and unpack batch data
        if angular_noise is not None:
            batch = angular_noise(batch)
        batch = rect_angle(batch)
        individual, context, mask, env = batch
        individual = individual.to(device=device)
        context = context.to(device=device)
        mask = mask.to(device=device)
        env = env.to(device=device)

        # log losses for the last epoch and update losses lists
        if new_epoch and (epoch == 1 or (epoch-1) % args.log_frequency == 0):
            # write TensorBoard scalar logs
            closs = log_losses(elosses_critic, tbwriter, epoch, label="LossCritic")
            log_losses(elosses_critic_loss, tbwriter, epoch, label="CriticFeatureDifference")
            log_losses(elosses_critic_penalty, tbwriter, epoch, label="CriticGradientPenalty")
            log_losses(elosses_critic_ploss, tbwriter, epoch, label="CriticPersonalSpaceLoss")
            gloss = log_losses(elosses_generator, tbwriter, epoch, label="LossGenerator")

            # print info to terminal
            out_str = '[Epoch %05d/%05d]\tCriticLoss=%.3f\tGenLoss=%.3f\tEpochDuration=%.2f (+-%.2f) sec' % \
                      (epoch, args.epochs, closs, gloss, np.mean(epoch_duration), np.std(epoch_duration))
            if got_decay_lr:
                out_str += "\tCriticLR=%.6f\tGenLR=%.6f" % (scheduler_critic.get_last_lr()[0],
                                                            scheduler_generator.get_last_lr()[0])
            print(out_str)
            sys.stdout.flush()

            # clear loss buffers
            elosses_critic = []
            elosses_critic_loss = []
            elosses_critic_penalty = []
            elosses_critic_ploss = []
            elosses_generator = []
            epoch_duration = []

        # save images
        if new_epoch and (epoch == 1 or (epoch-1) % args.plot_frequency == 0):
            log_sample_images(generator, train_dataset, resolution, tbwriter, epoch, [x for x in range(args.num_images)],
                              device=device, num_samples=20, prefix="train")
            log_sample_images(generator, test_dataset, resolution, tbwriter, epoch, [x for x in range(args.num_images)],
                              device=device, num_samples=20, prefix="test")

        # save models (but skip the ones after the first epoch since weights are pretty much random still)
        if new_epoch and epoch > 1 and (epoch-1) % args.checkpoint_frequency == 0:
            save_networks(logs_folder, stamp, epoch, generator, critic, opt_genenerator, opt_critic)

        # update learning rates as we completed an epoch
        if new_epoch and got_decay_lr:
            scheduler_critic.step()
            scheduler_generator.step()

        return individual, context, mask, env

    # train loop
    print("Training...")
    while epoch < args.epochs:
        update_critic(generator, critic, get_data_and_log_losses, personal_space_loss, device, opt_critic,
                      args.critic_iters, args.gpenalty, elosses_critic_loss, elosses_critic_penalty,
                      elosses_critic_ploss, elosses_critic)

        update_generator(generator, critic, get_data_and_log_losses, device, opt_genenerator, elosses_generator)



    # save the state of the training loop one last time...
    save_networks(logs_folder, stamp, epoch, generator, critic, opt_genenerator, opt_critic)


if __name__ == '__main__':
    args = get_params()
    main(args)
    sys.exit(0)
