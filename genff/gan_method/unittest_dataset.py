import unittest
import os
import torch
import numpy as np

from genff.gan_method.dataset import GroupDataset


def sim_dataset_file():
    """
    Helper function to get the path to the simulated dataset file
    """
    folder = os.path.dirname(__file__)
    # output_file_path = os.path.join(folder, "..", "..", "data", "simulated", "cocktail_output_data.txt")
    # env_file_path = os.path.join(folder, "..", "..", "data", "simulated", "cocktail_env_images.npz")
    output_file_path = os.path.join(folder, "..", "..", "data", "simulated", "gen_group_output_data.txt")
    env_file_path = os.path.join(folder, "..", "..", "data", "simulated", "gen_group_env_images.npz")
    # output_file_path = os.path.join(folder, "..", "..", "data", "generated", "cocktail_group_output_data.txt")
    # env_file_path = os.path.join(folder, "..", "..", "data", "generated", "cocktail_group_senv_images.npz")

    return output_file_path, env_file_path


class TestSimulatedDataset(unittest.TestCase):
    """
    Tests for GroupDataset class using simulated data
    """

    def __init__(self, *args, **kwargs):
        """
        Constructor
        """
        super(TestSimulatedDataset, self).__init__(*args, **kwargs)
        self.input_file, self.env_file = sim_dataset_file()            # input file
        self.dataset = GroupDataset(self.input_file, self.env_file)    # dataset
        self.x_index = 0                                               # index for the horizontal coord
        self.y_index = 1                                               # index for the vertical coord

    def test_dataset_len(self):
        """
        Check that the length of the dataset matches the number of examples in the input file
        """
        num_examples = 0
        with open(self.input_file, 'r') as fid:
            for line in fid:
                line = line.rstrip()
                if len(line) == 0:
                    continue  # empty line
                num_examples += 1

        self.assertEqual(num_examples, len(self.dataset),
                         "Expected the dataset to have {} examples but got {}".format(num_examples, len(self.dataset)))

    def test_dataset_env_placement(self, x_index: int = 0, y_index: int = 1):
        env = self.dataset.env_dict
        res = env["resolution"]
        left = -self.dataset.grid_w / 2.0 * res
        top = -self.dataset.grid_w / 2.0 * res

        def world_to_grid(pt: np.ndarray, x_index: int = 0, y_index: int = 1):
            x1 = (np.floor((pt[:, x_index] - left)/res))
            y1 = (np.floor((pt[:, y_index] - top)/res))
            x2 = (np.ceil((pt[:, x_index] - left)/res))
            y2 = (np.ceil((pt[:, y_index] - top)/res))

            # all 4 pixels around pt
            xx = np.concatenate((x1, x1, x2, x2), axis=0)
            yy = np.concatenate((y1, y2, y1, y2), axis=0)

            return np.column_stack((xx, yy)).astype(int)

        def get_grid_value(pt_world: np.ndarray, grid: np.ndarray):
            pt = world_to_grid(pt_world)
            values = []
            for i in range(pt.shape[0]):
                c = pt[i, 0]
                r = pt[i, 1]
                if c < 0 or c >= grid.shape[1]:  # x-coordinate
                    values.append(-1)  # outside grid
                elif r < 0 or r >= grid.shape[0]:  # y-coordinate
                    values.append(-1)  # outside grid
                else:
                    values.append(grid[r, c])
            return values

        for i in range(len(self.dataset)):
            individual, context, mask, env = self.dataset[i]
            valid_context = context[mask.bool(), :]
            ind_values = get_grid_value(individual[:, [x_index, y_index]].numpy(), env.numpy()[:,:,1])
            context_values = get_grid_value(valid_context[:, [x_index, y_index]].numpy(), env.numpy()[:,:,1])

            for j, val in enumerate(ind_values):
                self.assertTrue(val != -1,
                                "Individual for dataset index {} outside the grid".format(i))
                self.assertFalse(val < 1 or val > 1,
                                 "Individual for dataset index {} not in free space. IND {}".format(i, ind_values))

            for j, val in enumerate(context_values):
                self.assertTrue(val != -1,
                                "Context for dataset index {} outside the grid".format(i))
                self.assertFalse(val < 1 or val > 1,
                                 "Context for dataset index {} not in free space. IND {}".format(i, context_values))


    # def test_examples_in_circle(self):
    #     """
    #     Check that the positions of group members is exactly on a circle centered at (0,0)
    #     """

    #     for i in range(len(self.dataset)):
    #         individual, context, mask, env = self.dataset[i]
    #         valid_context = context[mask.bool(), :]

    #         # check radius
    #         r = torch.norm(individual[0, [self.x_index, self.y_index]], p=None, dim=None)
    #         for p in range(valid_context.shape[0]):
    #             n = torch.norm(valid_context[p, [self.x_index, self.y_index]], p=None, dim=None)
    #             self.assertAlmostEqual(n.item(), r.item(), places=1,
    #                                    msg="Inconsistent radius on example {}. Expected {} but got {}".
    #                                    format(i, r.item(), n.item()))

    def test_rect_repr(self):
        """
        Check that we can output data in rectangular angle representation
        """
        dataset2 = GroupDataset(self.input_file, self.env_file, rect_repr=True)
        for i in range(len(dataset2)):
            individual, context, mask, env = dataset2[i]
            self.assertEqual(individual.shape[-1], 4,
                             "Expected the individual to have 4 features but got {}".format(individual.shape[-1]))
            self.assertEqual(context.shape[-1], 4,
                             "Expected the context to have 4 features but got {}".format(context.shape[-1]))


if __name__ == '__main__':
    unittest.main()
