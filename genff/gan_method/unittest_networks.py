import unittest
import torch
from torch.utils.data import DataLoader

from genff.gan_method.dataset import GroupDataset
from genff.gan_method.networks import GenNet, DiscNet, PlotPositionsNet
from genff.gan_method.unittest_dataset import sim_dataset_file
from genff.gan_method.transform import RectangularAngle


class TestNetworks(unittest.TestCase):
    """
    Tests for the PlotPositionsNet class using simulated data
    """

    def __init__(self, *args, **kwargs):
        """
        Constructor
        """
        super(TestNetworks, self).__init__(*args, **kwargs)
        self.input_file = sim_dataset_file()            # input file
        self.dataset = GroupDataset(self.input_file)    # dataset
        self.transform = RectangularAngle(2)

    def test_PlotPositionsNet(self):
        """
        Test the PlotPositionsNet forward function
        """
        grid_h = 22
        grid_w = 24
        plotter = PlotPositionsNet(0.25, grid_w, grid_h, sigma=0.05, x_index=0, y_index=1)
        dataloader = DataLoader(self.dataset, num_workers=1, batch_size=32)

        def check_dims(tensor, reference):
            """
            Helper function to check the output dimensions of the PlotPositionsNet forward function
            """
            self.assertEqual(tensor.shape[0], reference.shape[0],
                             "Expected the output of PlotPositionsNet to have {} examples, but got {}".
                             format(reference.shape[0], tensor.shape[0]))
            self.assertEqual(tensor.shape[1], grid_h,
                             "Expected the output of PlotPositionsNet to have a height of {}, but got {}".
                             format(grid_h, tensor.shape[2]))
            self.assertEqual(tensor.shape[2], grid_w,
                             "Expected the output of PlotPositionsNet to have a width of {}, but got {}".
                             format(grid_w, tensor.shape[2]))

        # process all batches
        for step, batch in enumerate(dataloader):
            batch = self.transform(batch)
            batch_i, batch_c, batch_m = batch

            # individual
            out_i = plotter(batch_i)
            check_dims(out_i, batch_i)

            # context
            out_c = plotter(batch_c, batch_m)
            check_dims(out_c, batch_c)

    def test_DiscNet(self):
        """
        Test the DiscNet forward function
        """
        batch_size = 10
        grid_h = 24
        grid_w = 24
        critic = DiscNet(0.25, grid_w, grid_h, sigma=0.05)
        dataloader = DataLoader(self.dataset, num_workers=1, batch_size=batch_size)

        # process all batches
        for step, batch in enumerate(dataloader):
            batch = self.transform(batch)
            batch_i, batch_c, batch_m = batch

            out = critic(batch_i, batch_c, batch_m)
            self.assertEqual(out.shape[0], batch_size,
                             "Expected the output of DiscNet to have {} examples, but got {}".
                             format(batch_size, out.shape[0]))
            self.assertEqual(out.shape[1], 1,
                             "Expected the output of DiscNet to be a single value per example but got {}".
                             format(out.shape[1]))

    def test_GenNet(self):
        """
        Test the GenNet forward function
        """
        batch_size = 100
        grid_h = 24
        grid_w = 24
        generator = GenNet(0.25, grid_w, grid_h, sigma=0.05)
        dataloader = DataLoader(self.dataset, num_workers=1, batch_size=batch_size)

        def check_dim(tensor):
            """
            Helper function to check the output dims by the generator
            """
            self.assertEqual(tensor.shape[0], batch_size,
                             "Expected the output of DiscNet to have {} examples, but got {}".
                             format(batch_size, tensor.shape[0]))
            self.assertEqual(tensor.shape[1], 1,
                             "Expected the output of DiscNet to have dimensions B x 1 x F, but the second dim was {}".
                             format(tensor.shape[1]))
            self.assertEqual(tensor.shape[2], 4,
                             "Expected the output of DiscNet to be a 4D vector per example but got {}".
                             format(tensor.shape[1]))

        # process all batches
        for step, batch in enumerate(dataloader):
            batch = self.transform(batch)
            batch_i, batch_c, batch_m = batch

            # let's have the generator make the noise by itself...
            out1 = generator(batch_c, batch_m)
            check_dim(out1)

            # let's check what happens if we pass the noise ourselves...
            noise = torch.normal(0, 1, size=(batch_size, 1, grid_h, grid_h))
            out2 = generator(batch_c, batch_m, noise=noise)
            check_dim(out2)


if __name__ == '__main__':
    unittest.main()
