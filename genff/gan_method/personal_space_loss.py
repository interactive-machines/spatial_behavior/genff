"""
Data transforms
"""
import math
import numpy as np
import torch
import random

class PersonalSpaceLoss(object):
    """
    Calculate personal space loss of sample
    """

    def __init__(self, personal_space=0.5, alpha=0.01, x_index=0, y_index=1, angle_index=2, position_only=False, personal_sigma=0.21, dtype=torch.float32, device="cpu"):
        """
        Constructor
        :param personal_space: (float) minimum distance at which personal distance is valid
        :param alpha: (float) regularization parameter to scale loss
        :param x_index: (int) position of the x coordinate feature in the second dim of the input tensors
        :param y_index: (int) position of the y coordinate feature in the second dim of the input tensors
        :param angle_index: (int or list) position of the orientation feature in the second dim of the input tensors
        :param position_only: (bool) is data xy data only?
        :param personal_sigma: sigma for personal space gaussian bump (we recommend intimate_space/2.0 such that 95% of the dist corresponds to intimate space)
        :param dtype: (torch data type) data type of pytorch tensors
        """
        if isinstance(angle_index, list):
            angle_index = angle_index
        else:
            angle_index = [angle_index]

        for key, value in locals().items():
            if key != "self":
                self.__dict__.update({key: value})

    def _calculate_losses(self, generated, context, mask):
        """
        Helper function to calculate personal space loss
        :param generated: batch of generated individual positions, shape: B x 1 X F
        :param context: batch of context people, shape: B x C X F
        :param mask: batch of masks, shape: B x C
        :return: tensor of B X 1 losses
        """
        batch_losses = torch.zeros(context.shape[0])

        for i in range(context.shape[0]):

            # Get individual sample
            individual_tensor = generated[i]
            context_tensor = context[i]
            mask_tensor = mask[i]
            mask_indices = mask_tensor.nonzero().squeeze(1)
            masked_context = context_tensor.index_select(0, mask_indices)

            # Calculate distances to context
            squared_distances = torch.pow(masked_context[:, [self.x_index, self.y_index]] - \
                                individual_tensor[:, [self.x_index, self.y_index]].repeat(masked_context.shape[0],1), 2)
            distances = torch.sqrt(squared_distances.sum(dim=1))

            # Subtract from personal distance
            personal_dist = torch.FloatTensor([self.personal_space]).to(self.device)
            differences = personal_dist.expand_as(distances) - distances

            # Take max of differences and 0
            floor = torch.FloatTensor([0]).to(self.device)
            losses = torch.max(differences, floor.expand_as(differences))

            # Append to batch loss list
            batch_losses[i] = losses.sum()

        # Regularize by alpha
        return self.alpha * batch_losses

    def _calculate_losses_gaussian(self, generated, context, mask):
        """
        Helper function to calculate gaussian personal space loss
        :param generated: batch of generated individual positions, shape: B x 1 X F
        :param context: batch of context people, shape: B x C X F
        :param mask: batch of masks, shape: B x C
        :return: tensor of B X 1 losses
        """

        batch_losses = []

        for i in range(context.shape[0]):

            # Get individual sample
            individual_tensor = generated[i]
            context_tensor = context[i]
            mask_tensor = mask[i]
            mask_indices = torch.nonzero(mask_tensor, as_tuple=False)
            mask_indices = mask_indices.squeeze(1)
            masked_context = context_tensor.index_select(0, mask_indices)
            diff = individual_tensor.repeat(masked_context.shape[0],1)[:, [self.x_index, self.y_index]] - \
                   masked_context[:, [self.x_index, self.y_index]]
            variance = self.personal_sigma * self.personal_sigma
            det_Sigma = variance * variance  # sigma is diag => the det is the product of diag elements
            denom = math.sqrt(4 * np.pi * np.pi * det_Sigma)

            # (x-mu)^T Sigma^-1 (x-mu) can be simplified as below because Sigma is diag
            num = torch.exp(-0.5 * torch.sum(torch.pow(diff, 2), dim=1) / variance)
            pdf = num / denom

            # Append to batch loss list
            batch_losses.append(self.alpha * torch.sum(pdf))

        # Regularize by alpha
        return torch.tensor(batch_losses, requires_grad=True, device="cpu")

    def __call__(self, sample):
        """
        Get personal space loss of input
        :param sample: tuple of batch-wise individual, context, and mask tensors
        :return: tensor of losses
        """
        individual_input, context_input, mask_input = sample
        return self._calculate_losses_gaussian(individual_input, context_input, mask_input)

    def __repr__(self):
        return self.__class__.__name__ + '(x_index={}, y_index={}, angle_index={})'.\
            format(self.x_index, self.y_index, self.angle_index)

if __name__ == "__main__":
    ind = torch.tensor([[[0,0,-100,-100]], [[0.11,0.11,0.5,0.5]]], dtype=torch.float32)
    context = torch.tensor([[[0.1,0.1,3,4],[4,5,6,7],[7,8,9,10],[10,11,12,13]], [[0.1,0.1,3,4],[100,100,6,7],[7,8,9,10],[10,11,12,13]]], dtype=torch.float32)
    mask = torch.tensor([[1,1,1,0],[1,1,0,1]], dtype=torch.float32)
    p = PersonalSpaceLoss()
    print(p((ind,context,mask)))


