import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict


class SetNet(nn.Module):
    """Set transform for converting group context features in to a higher-dimensional representation"""

    def __init__(self, input_feat, mlp_dims=None, max_pool=True, add_batchnorm=True):
        """
        Constructor
        :param input_feat: number of features per person in the model
        :param mlp_dims: number of features in the individual mlp
        :param max_pool: if True, use max pooling to combine people's features; otherwise, use avg. pooling
        :param add_batchnorm: apply batchnorm? (default: True)
        """
        super(SetNet, self).__init__()

        self.input_feat = input_feat
        self.max_pool = max_pool

        # set default number of layers if none were specified
        if mlp_dims is None:
            self.mlp_dims = [32, 256, 1024]
        else:
            self.mlp_dims = mlp_dims

        self.mlp = self.create_shared_mlp(add_batchnorm)

    def create_shared_mlp(self, add_batchnorm):
        """
        Helper function to make shared mlp
        :param add_batchnorm: add batch norm?
        """
        input_feat = self.input_feat
        layers = []
        for i in range(len(self.mlp_dims)):
            layers.append(("Dense_{}".format(i), nn.Linear(input_feat, self.mlp_dims[i], bias=True)))
            input_feat = self.mlp_dims[i]
            layers.append(("RELU_{}".format(i), nn.ReLU()))
            if add_batchnorm:
                layers.append(("BN_{}".format(i), nn.BatchNorm1d(input_feat)))
        return nn.Sequential(OrderedDict(layers))

    def forward(self, tensor, mask=None):
        """
        Compute the output of the model
        :param tensor: BxPxF individual or context tensor
        :param mask: BxP mask for the tensor (in the case of a context tensor)
        :return: BxK tensor
        """
        batch_size = tensor.shape[0]
        num_people = tensor.shape[1]

        # make fake mask so that we can deal with individuals and context tensors the same way...
        if mask is None:
            mask = torch.ones((batch_size, num_people), device=tensor.device)

        # compute higher-dimensional features
        features = tensor
        for layer in self.mlp:
            if isinstance(layer, torch.nn.BatchNorm1d):
                # reshape the tensor to normalize the features based on the batch
                tmp = features.view(-1, features.shape[-1])
                tmp = layer(tmp)
                features = tmp.view(batch_size, num_people, features.shape[-1])
            else:
                features = layer(features)

        features = features.transpose(2, 1)

        # apply pooling for each element in the batch
        l = []
        for b in range(batch_size):
            m = torch.nonzero(mask[b, :], as_tuple=False).flatten()
            x = features[b:b+1, :, m]

            if self.max_pool:
                # note that we don't define the pooling operation in the init function of this
                # class because that would require to know the number of people before calling the forward function
                pooled_feat = F.max_pool1d(x,  # input
                                           (m.shape[0],),  # kernel size (number of people)
                                            1,  # stride
                                            0,  # padding
                                            1,  # dilation
                                            False,  # ceil mode
                                            False)  # return indices
            else:
                # use avg pooling
                pooled_feat = F.avg_pool1d(x,  # input
                                          (m.shape[0],),  # kernel size (number of people)
                                           1,  # stride
                                           0,  # padding
                                           False,  # ceil mode
                                           False)  # count include pad

            l.append(pooled_feat)

        # concat each element in the batch
        out = torch.cat(l, dim=0)
        # remove singleton dimension
        out = torch.squeeze(out, dim=2)

        return out


class PlotPositionsNet(nn.Module):
    """
    Deterministic neural network that converts 2D coordinates rel. to the group to a bump image
    """

    def __init__(self, grid_res, grid_w, grid_h, sigma=0.05, x_index=0, y_index=1):
        """
        Constructor
        :param grid_res: grid resolution (how many meters is each cell)
        :param grid_w: grid width
        :param grid_h: grid height
        :param sigma: sigma for the gaussian bumps
        :param x_index: index for the x coordinate in the input tensor
        :param y_index: index for the y coordinate in the input tensor
        """
        super(PlotPositionsNet, self).__init__()
        self.grid_res = grid_res
        self.grid_w = grid_w
        self.grid_h = grid_h
        self.sigma = sigma
        self.x_index = x_index
        self.y_index = y_index

        # generate indices for the grid
        xv, yv = torch.meshgrid([torch.arange(0, self.grid_h), torch.arange(0, self.grid_w)])

        # we assume the value for a cell is in the middle of a cell in the grid
        xv_position = xv * self.grid_res - (self.grid_h * self.grid_res / 2.0) + (self.grid_res / 2.0)
        yv_position = yv * self.grid_res - (self.grid_w * self.grid_res / 2.0) + (self.grid_res / 2.0)
        grid_positions = torch.stack([xv_position, yv_position], dim=2)  # grid_h x grid_w x 2

        self.register_buffer('grid_positions', grid_positions)

    def forward(self, tensor, mask=None):
        """
        Compute the output of the model
        :param tensor: BxPxF individual or context tensor
        :param mask: BxP mask for the tensor (in the case of a context tensor)
        """
        batch_size = tensor.shape[0]

        if mask is None:
            mask = torch.ones((batch_size, 1))  # fake mask to handle individual tensors in the same way as the context

        grid = torch.zeros((batch_size, self.grid_h, self.grid_w), dtype=tensor.dtype, device=tensor.device)
        for b in range(batch_size):
            batch_mask = torch.nonzero(mask[b], as_tuple=False)
            mu = tensor[b, batch_mask, [self.y_index, self.x_index]].unsqueeze(0).unsqueeze(0)  # 1 x 1 x P x F
            mu = mu.repeat(self.grid_h, self.grid_w, 1, 1).permute(2, 0, 1, 3)  # repeat mu for all pixels (P x grid_h x grid_w x F)

            # we now compute the unnormalized gaussian: exp(-0.5 * (x - mu)^T Sigma^-1 (x - mu))
            # Sigma is a diag matrix with sigma in the diagonal. This allows us to decompose the math
            # into the following steps:
            # 1. compute the difference x - mu
            difference = (self.grid_positions.unsqueeze(0).repeat(batch_mask.shape[0], 1, 1, 1) - mu)
            # 2. reshape the difference into a N x 2 matrix
            d = difference.view(batch_mask.shape[0], -1, 2)
            # 3. compute (x - mu)^T Sigma^-1 (x - mu) = (1/sigma) * sum(d^2)
            squared_d = torch.square(d)
            n = torch.sum(squared_d, 2)
            e = torch.exp(- (0.5 / self.sigma) * n)

            grid[b, :, :] = e.sum(dim=0).view(difference.shape[1], difference.shape[2])

        return grid


class DiscNet(nn.Module):
    """
    Critic for WGAN-GP
    """

    def __init__(self, grid_res, grid_w, grid_h, combine_env_channels=False, sigma=0.05):
        """
        Constructor
        :param grid_res: grid resolution (how many meters is each cell)
        :param grid_w: grid width
        :param grid_h: grid height
        :param combine_env_channels: combine environment channels into a single channel? (if false, 2 channels are used: one for unknown+tall, and another one for short obstacles)
        :param sigma: sigma for the gaussian bumps
        """
        super(DiscNet, self).__init__()
        self.x_index = 0
        self.y_index = 1
        self.c_index = 2
        self.s_index = 3
        self.combine_env_channels = combine_env_channels

        self.plotter = PlotPositionsNet(grid_res, grid_w, grid_h, sigma=sigma,
                                        x_index=self.x_index, y_index=self.y_index)

        if self.combine_env_channels:
            self.cnn = nn.Sequential(
                nn.Conv2d(3, 8, kernel_size=3, stride=1, padding=1),        # 8 x 24 x 24 (excluding batch size)
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 8 x 12 x 12
                nn.Conv2d(8, 32, kernel_size=3, stride=1, padding=1),       # 32 x 12 x 12
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 32 x 6 x 6
                nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),      # 64 x 6 x 6 = 2304
                nn.ReLU()
            )
        else:
            self.cnn = nn.Sequential(
                nn.Conv2d(4, 8, kernel_size=3, stride=1, padding=1),        # 8 x 24 x 24 (excluding batch size)
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 8 x 12 x 12
                nn.Conv2d(8, 32, kernel_size=3, stride=1, padding=1),       # 32 x 12 x 12
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 32 x 6 x 6
                nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),      # 64 x 6 x 6 = 2304
                nn.ReLU()
            )

        self.individual_mlp = nn.Sequential(
            nn.Linear(4, 32),
            nn.ReLU(),
            nn.Linear(32, 64),
            nn.ReLU()
        )
        self.context_mlp = SetNet(4, mlp_dims=[32, 64, 128], add_batchnorm=False)
        self.final_mlp = nn.Sequential(
            nn.Linear(6*6*64 + 64 + 128, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 1)
        )

    def forward(self, individual, context, mask, env):
        """
        Forward function for the model
        :param individual: Bx1xF individual tensor
        :param context: BxPxF context tensor
        :param mask: BxP context mask
        :param env: BxHxWx4 cropped env map
        :return: 1D feature vector
        """

        # convert individual and context to image format
        im_ind = self.plotter(individual).unsqueeze(1)  # B x 1 x H x W
        im_con = self.plotter(context, mask).unsqueeze(1)  # B x 1 x H x W

        if self.combine_env_channels:
            not_free = env[:, :, :, 0:1] + env[:, :, :, 2:3] + env[:, :, :, 3:4]  # unknown + tall obstacles + short obstacles
            im_env = not_free.permute(0, 3, 1, 2)  # B x 1 x H x W
        else:
            env_tall = env[:,:,:,0] + env[:,:,:,2] # tall obstacles + unknown
            not_free = torch.stack((env[:,:,:,3], env_tall), 3)
            im_env = not_free.permute(0,3,1,2) # B x 2 x H x W

        # create input image
        im = torch.cat((im_ind, im_con, im_env), 1)  # B x 4 x H x W

        # pass through CNN
        cnn_feat = self.cnn(im)
        cnn_flattened = cnn_feat.view(cnn_feat.size(0), -1)

        # individual transform
        ind_feat = self.individual_mlp(individual)
        ind_flattened = ind_feat.view(ind_feat.size(0), -1)

        # context transform
        con_feat = self.context_mlp(context, mask)

        # join features
        joint_feat = torch.cat((cnn_flattened, ind_flattened, con_feat), dim=1)

        # pass through MLP
        out = self.final_mlp(joint_feat)

        return out


class GenNet(nn.Module):
    """
    Generator for WGAN-GP. Outputs individual features based on random noise and social group context
    """

    def __init__(self, grid_res, grid_w, grid_h, combine_env_channels=False, sigma=0.05):
        """
        Constructor
        :param grid_res: grid resolution (how many meters is each cell)
        :param grid_w: grid width
        :param grid_h: grid height
        :param combine_env_channels: combine environment channels into a single channel? (if false, 2 channels are used: one for unknown+tall, and another one for short obstacles)
        :param sigma: sigma for the gaussian bumps
        """
        super(GenNet, self).__init__()
        self.grid_w = grid_w
        self.grid_h = grid_h
        self.x_index = 0
        self.y_index = 1
        self.c_index = 2
        self.s_index = 3
        self.combine_env_channels = combine_env_channels

        self.plotter = PlotPositionsNet(grid_res, grid_w, grid_h, sigma=sigma,
                                        x_index=self.x_index, y_index=self.y_index)

        if self.combine_env_channels:
            self.cnn = nn.Sequential(
                nn.Conv2d(3, 8, kernel_size=3, stride=1, padding=1),        # 8 x 24 x 24 (excluding batch size)
                nn.BatchNorm2d(8),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 8 x 12 x 12
                nn.Conv2d(8, 32, kernel_size=3, stride=1, padding=1),       # 32 x 12 x 12
                nn.BatchNorm2d(32),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 32 x 6 x 6
                nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),      # 64 x 6 x 6 = 2304
                nn.BatchNorm2d(64),
                nn.ReLU()
            )
        else:
            self.cnn = nn.Sequential(
                nn.Conv2d(4, 8, kernel_size=3, stride=1, padding=1),        # 8 x 24 x 24 (excluding batch size)
                nn.BatchNorm2d(8),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 8 x 12 x 12
                nn.Conv2d(8, 32, kernel_size=3, stride=1, padding=1),       # 32 x 12 x 12
                nn.BatchNorm2d(32),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),           # 32 x 6 x 6
                nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),      # 64 x 6 x 6 = 2304
                nn.BatchNorm2d(64),
                nn.ReLU()
            )

        self.context_mlp = SetNet(4, mlp_dims=[32, 64, 128], add_batchnorm=True)
        self.final_mlp = nn.Sequential(
            nn.Linear(6*6*64 + 128, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 4)
        )

    def forward(self, context, mask, env, noise=None):  # todo complete!
        """
        Forward function for the model
        :param context: BxPxF context tensor
        :param mask: BxP context mask
        :param noise: Bx1xHxW
        :param env: BxHxWx4 cropped env map
        :return: Bx3 features for a new individual
        """
        # convert individual and context to image format
        im_con = self.plotter(context, mask).unsqueeze(1)  # B x 1 x H x W

        if noise is None:
            b = im_con.shape[0]  # batch size
            noise = torch.normal(0, 1, size=(b, 1, self.grid_h, self.grid_h), device=context.device)  # noise

        # Get right dimensions for env image
        if self.combine_env_channels:
            not_free = env[:, :, :, 0:1] + env[:, :, :, 2:3] + env[:, :, :, 3:4] # unknown + tall obstacles + short obstacles
            im_env = not_free.permute(0, 3, 1, 2)  # B x 1 x H x W
        else:
            env_tall = env[:,:,:,0] + env[:,:,:,2] # tall obstacles + unknown
            not_free = torch.stack((env[:,:,:,3], env_tall), 3)
            im_env = not_free.permute(0,3,1,2) # B x 2 x H x W

        # create input image
        im = torch.cat((noise, im_con, im_env), 1)  # B x (4 or 3) x H x W

        # pass through CNN
        cnn_feat = self.cnn(im)
        cnn_flattened = cnn_feat.view(cnn_feat.size(0), -1)

        # get features for the group considering orientations
        group_feat = self.context_mlp(context, mask)

        # concat
        joint_feat = torch.cat((cnn_flattened, group_feat), dim=1)

        # pass through MLP
        out = self.final_mlp(joint_feat)

        # constraint the last two features to be in (-1,1) assuming that they represent cos(theta), sin(theta)
        out[:, [self.c_index, self.s_index]] = torch.tanh(out[:, [self.c_index, self.s_index]])
        return out.unsqueeze(1)
