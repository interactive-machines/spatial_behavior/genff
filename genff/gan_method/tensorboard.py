from torch.utils.tensorboard import SummaryWriter
from typing import List
import torch
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
from genff.visualization.plotting import plot_sample_no_map_rect, plot_sample_on_full_map_rect, plot_sample_rect
from genff.data_handling.transform import RectangularAngle


def log_losses(losses_list: List[torch.Tensor], writer: SummaryWriter, epoch: int, label: str = "Loss") -> float:
    """
    Herlper function to log the losses to tensorboard
    :param losses_list: list with losses from training loop
    :param writer: Tensorboard SummaryWriter
    :param label: label for the losses
    :param epoch: training epoch
    :return: scalar loss
    """
    if len(losses_list) == 0:
        # handles empty list (e.g., when no penalty is applied)
        avg_loss = 0.0
    else:
        # special case for gradient penalty, which is a standard list
        avg_loss = torch.mean(torch.cat(losses_list)).item()
    writer.add_scalar(label, avg_loss, epoch)
    writer.flush()
    return avg_loss


def log_sample_images(generator: torch.nn.Module, dataset: Dataset, resolution, writer: SummaryWriter,
                      epoch: int, images_indices: List[int], device: torch.device,
                      num_samples: int = 20, prefix: str = "") -> None:
    """
    Helper function to log generated samples to tensorboard
    :param generator: generator model
    :param dataset: dataset with images
    :param writer: Tensorboard SummaryWriter
    :param epoch: current epoch for logging purposes
    :param images_indices: indices of the images to be plotted
    :param device: processing device
    :param num_samples: number of samples to generate per batch example
    :param prefix: string indicating desired prefix when images are displayed in TB
    """
    previously_training = generator.training
    if previously_training:
        generator.eval()

    transform = RectangularAngle(2)

    with torch.no_grad():

        # make batch of images
        batch_size = len(images_indices)
        ind_list = []
        con_list = []
        mas_list = []
        env_list = []
        for i, ind in enumerate(images_indices):
            sample = dataset[ind]
            ind_list.append(sample[0])
            con_list.append(sample[1])
            mas_list.append(sample[2])
            env_list.append(sample[3])

        individual = torch.stack(ind_list).to(device=device)
        context = torch.stack(con_list).to(device=device)
        mask = torch.stack(mas_list).to(device=device)
        env = torch.stack(env_list).to(device=device)

        # transform batch to rect repr
        individual, context, mask, env = transform((individual, context, mask, env))

        # make storage for the fake samples
        num_features = individual.shape[-1]
        samples = torch.zeros((batch_size, num_samples, num_features), device="cpu")

        # generate samples
        for s in range(num_samples):
            new_ind = generator(context, mask, env)
            samples[:, s, :] = new_ind[:, 0, :].cpu()

        individual = individual.cpu()
        context = context.cpu()
        mask = mask.cpu()

        # plot data
        for b in range(batch_size):
            fig = plt.figure()
            plot_sample_rect(samples[b, :, :], context[b, :, :], mask[b, :], env_cropped=env[b,:,:,:],
                             env_res=resolution, ind_color="r", mode="free")
            plot_sample_rect(individual[b, :, :], context[b, :, :], mask[b, :], env_cropped=env[b,:,:,:],
                             env_res=resolution, ind_color="g", mode="free")
            plt.gca().set_aspect('equal')
            writer.add_figure(tag="Generated/{}{}".format(prefix,b), figure=fig, global_step=epoch, close=True)

    if previously_training:
        generator.train()
