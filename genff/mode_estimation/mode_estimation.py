"""
Methods to determine the mode of a distribution of samples
"""

from typing import Tuple

import numpy as np
from sklearn.cluster import MeanShift

from vonmiseskde.vonmiseskde import VonMisesKDE


def find_nearby_poses(poses: np.ndarray, point: np.ndarray,
                      threshold: float, fallback: int=3) -> Tuple[np.ndarray, np.ndarray]:
    """
    :param poses: Nx3 array containing the poses of the generated distribution samples
    :param p: 1x2 array containing the point to be compared against
    :param threshold: floating point value representing distance
    :param fallback: number of closest poses to return if no poses are within threshold from p
    :return: Kx3 array with the K poses closer than threshold to p,
             Kx1 array with corresponding distances to p
    """
    distances = np.linalg.norm(poses[:,0:2] - point, axis=1)
    nearby_distances = distances[distances < threshold]
    nearby_poses = poses[distances < threshold, :]

    if nearby_poses.shape[0] == 0:
        nearby_poses = poses[np.argsort(distances)[:fallback], :]
        nearby_distances = distances[np.argsort(distances)[:fallback]]

    return nearby_poses, nearby_distances


def find_modes(poses: np.ndarray, bandwidth: float=0.8, threshold: float=0.25,
               kappa: float=1.0, use_kde: bool=False) -> np.ndarray:
    """
    :param poses: Nx3 array containing the poses of the generated distribution samples
    :param bandwidth: bandwidth value to pass to MeanShift
    :param threshold: threshold for finding nearby poses to a cluster center
    :param kappa: kappa value to pass to Von Mises KDE
    :param use_kde: if true, outputs mode from kde (including vm kde); if false, outputs the closest pose to the mode
    :return: Kx3 array where K is the number of modes found
    """
    mean_shift = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    mean_shift.fit(poses[:,0:2]) # only use mean shift to fit xy positions

    # sort the clusters by number of labels, descending
    counts = np.bincount(mean_shift.labels_)
    cluster_order = np.argsort(counts * -1)
    positions = mean_shift.cluster_centers_[cluster_order, :]

    modes = []
    for point in positions:
        nearby_poses, distance_weights = find_nearby_poses(poses, point, threshold)
        if use_kde:
            if np.array_equal(distance_weights, np.zeros(1,)):
                distance_weights = np.ones((1,))
            angles = nearby_poses[:,2]
            kde = VonMisesKDE(angles, weights=distance_weights, kappa=kappa)
            angle_dist = kde.vonMisesPDF(angles)
            theta = angles[np.argmax(angle_dist)]
            mode = np.array([point[0], point[1], theta])
        else:
            mode = nearby_poses[0]
        modes.append(mode)

    return np.asarray(modes)
