import warnings
import torch
import math
import numpy as np
import scipy.optimize as optimize

from genff.opt_method.fit_circle import fit_circle_individual, fit_circle_dyad, fit_circle_geom
from genff.opt_method.fit_ellipse import fit_ellipse, ellipse_center, ellipse_ax_length, ellipse_angle_of_rotation
from genff.opt_method.opt_loss import CircleLoss, EllipseLoss


def torch_to_numpy(tensor):
    """
    Helper function to transform a torch tensor to numpy
    :param tensor: tensor (torch or numpy)
    :return: numpy tensor
    """
    if tensor is not None and torch.is_tensor(tensor):
        array = tensor.detach().numpy()
    else:
        array = tensor
    return array


def fit_circle_to_context(context, x_index=0, y_index=1, angle_index=2, ospace_dist=0.72):
    """
    Helper function to fit circle based on the number of people in the context
    :param context: NxD valid context
    :param x_index: : index of the x coordinates
    :param y_index: index of the y coordinates
    :param angle_index: index of the person's orientation
    :param ospace_dist: default distance for o-space in case the number of people is 1
    return circle center, radius
    """
    num_people = context.shape[0]

    if num_people == 1:
        # There's a single individual in the group. We fit a circle
        center, radius = fit_circle_individual(context[0, x_index],
                                               context[0, y_index],
                                               context[0, angle_index],
                                               ospace_dist=ospace_dist)
    elif num_people == 2:
        # Fit circle to dyad
        center, radius = fit_circle_dyad(context[:, [x_index, y_index]].T)
    else:
        # Still not enough people to fit ellipse. We stick with a circle
        (center, radius), _ = fit_circle_geom(context[:, [x_index, y_index]].T,
                                              verbose=False,
                                              compute_residuals=True)
    return center, radius


def opt_generator_loss(context, env_map, env_res,
                       x_index=0, y_index=1, angle_index=2, ospace_dist=0.72,
                       weight_psloss=1.0, weight_closs=0.2, weight_eloss=1.0, verbose=False):
    """
    Create opt-based generator loss
    :param context: NxD valid context matrix as numpy array
    :param env_map: environment map as numpy array or None if no env map
    :param env_res: environment map resolution or None
    :param x_index: index of the x coordinates
    :param y_index: index of the y coordinates
    :param angle_index: index of the person's orientation
    :param ospace_dist: default distance for o-space in case the number of people is 1
    :param weight_psloss: weight for personal space loss
    :param weight_closs: weight for circular loss
    :param weight_eloss: weight for environment loss
    :return: loss, params for fitted circular shape (keys: type, center, ...)
    """
    num_people = context.shape[0]

    def create_circle_loss(center, radius):
        return CircleLoss(center, radius, context[:, [x_index, y_index]],
                          env_map=env_map, env_res=env_res,
                          weight_psloss=weight_psloss, weight_closs=weight_closs, weight_eloss=weight_eloss)

    params = {}

    if num_people < 5:
        # Not enough people to fit ellipse. We fit a circle instead
        center, radius = fit_circle_to_context(context, x_index=x_index, y_index=y_index,
                                               angle_index=angle_index, ospace_dist=ospace_dist)
        loss = create_circle_loss(center, radius)
        params = {'type': 'circle',
                  'center': center,
                  'radius': radius}
        if verbose:
            print("Fit circle for group (N={})".format(num_people))

    else:
        # Fit ellipse
        try:
            params = fit_ellipse(context[:, [x_index, y_index]].T)
            center = ellipse_center(params)
            ax_length = ellipse_ax_length(params)
            radians = ellipse_angle_of_rotation(params)
            loss = EllipseLoss(center, ax_length, radians, context[:, [x_index, y_index]],
                               env_map=env_map, env_res=env_res,
                               weight_psloss=weight_psloss, weight_closs=weight_closs,
                               weight_eloss=weight_eloss)
            params = {'type': 'ellipse',
                      'center': center,         # ellipse center
                      'ax_length': ax_length,   # axes lengths
                      'radians': radians}       # angle of rotation
            if verbose:
                print("Fit ellipse for group (N={})".format(num_people))
        except RuntimeError as e:
            # Failed to fit ellipse. Try fitting a circle instead
            warnings.warn('Failed to fit ellipse. Fitting circle instead. Error: {}'.format(e))
            center, radius = fit_circle_to_context(context, x_index=x_index, y_index=y_index,
                                                   angle_index=angle_index, ospace_dist=ospace_dist)
            loss = create_circle_loss(center, radius)
            params = {'type': 'circle',
                      'center': center,
                      'radius': radius}
            if verbose:
                print("Fit circle for group; backup option (N={})".format(num_people))

    return loss, params


def opt_generator(c, m, emap=None, eres=None,
                  x0=None, x_index=0, y_index=1, angle_index=2, ospace_dist=0.72,
                  weight_psloss=1.0, weight_closs=0.2, weight_eloss=1.0,
                  verbose=False):
    """
    (Rule-based or) Optimization-based individual generator
    :param c: NxD matrix (each row corresponds to a person's features)
    :param m: Nx1 mask for the context
    :param emap: environment map (optional)
    :param eres: environment map resolution
    :param x0: Kx2 array with initial value for the solver (None means start from [0,0] -- the ospace center)
    :param x_index: index of the x coordinates
    :param y_index: index of the y coordinates
    :param angle_index: index of the person's orientation
    :param ospace_dist: default distance for o-space in case the number of people is 1
    :param weight_psloss: weight for personal space loss
    :param weight_closs: weight for circular loss
    :param weight_eloss: weight for environment loss
    :return: Kx3 with (x, y, angle for the individual) per row, list with final losses, loss function
    """
    # transform torch data to numpy
    context = torch_to_numpy(c)
    mask = torch_to_numpy(m)
    env_map = torch_to_numpy(emap)

    valid_context = context[mask > 0, :]  # keep only valid context rows
    loss, params = opt_generator_loss(valid_context, env_map, eres, x_index, y_index, angle_index,
                                      ospace_dist, weight_psloss, weight_closs, weight_eloss, verbose)
    center = params['center']

    if x0 is None:
        x0 = np.zeros((1, 2))

    assert x0.shape[1] == 2, "Expected the initial positions to be 2D (x0.shape={})".format(x0.shape)

    K = x0.shape[0]
    out = np.zeros((K, 3))

    final_losses = []

    for i in range(K):

        # optimize for a suitable position for the person
        bnds = ((center[0] - 5.0, center[0] + 5.0), (center[1] - 5.0, center[1] + 5.0))
        res = optimize.minimize(loss, [x0[i, 0], x0[i, 1]], method='Powell', bounds=bnds)

        # compute angle
        dx = center[0] - res.x[0]
        dy = center[1] - res.x[1]
        angle = math.atan2(dy, dx)

        # save result
        out[i, 0] = res.x[0]
        out[i, 1] = res.x[1]
        out[i, 2] = angle

        final_losses.append(loss(res.x))

    return out, final_losses, loss
