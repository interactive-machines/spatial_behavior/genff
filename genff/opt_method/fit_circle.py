import numpy as np
from scipy import odr


def fit_circle_individual(x, y, theta, ospace_dist=0.72):
    """
    Fit circle when there's a single individual. The circle is placed forward from the individual.
    :param x: x coordinate for the individual
    :param y: y coordinate for the individual
    :param theta: angle for the individual (in radians)
    :param ospace_dist: distance from the individual to the o-space center (default value from M. Vazquez's PhD thesis (appendix B))
    :return center (x,y), radius
    """
    a = x + np.cos(theta)*ospace_dist
    b = y + np.sin(theta)*ospace_dist
    radius = ospace_dist
    return (a, b), radius


def fit_circle_dyad(points):
    """
    Fit circle for a dyad. The circle is centered among them.
    :param points: 2xN matrix with N 2D points (and N >= 3)
    :return center (x,y), radius
    """
    center = np.mean(points, axis=1)
    diff = points[:, 0] - center
    radius = np.sqrt(np.dot(diff.T,diff))
    return (center[0], center[1]), radius


def fit_circle_geom(points, verbose=False, compute_residuals=True):
    """
    Fit circle as in Method 3b from https://scipy-cookbook.readthedocs.io/items/Least_Squares_Circle.html
    :param points: 2xN matrix with N 2D points (and N >= 3)
    :param verbose: print info?
    :param compute_residuals: compute residuals?
    :return: tuple with ((a,b) center and radius) and residuals (None if compute_residuals is False)
    """

    assert points.shape[0] == 2, "Expected the input data to have 2 rows (got {})".format(points.shape[0])
    assert points.shape[1] >= 3, "Expected the input data to have at least 3 columns (got {})".format(points.shape[1])

    def calc_R(x, y, xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((x - xc) ** 2 + (y - yc) ** 2)

    def f_3b(beta, x):
        """ implicit definition of the circle """
        return (x[0] - beta[0]) ** 2 + (x[1] - beta[1]) ** 2 - beta[2] ** 2

    def jacb(beta, x):
        """ Jacobian function with respect to the parameters beta.
        return df_3b/dbeta
        """
        xc, yc, r = beta
        xi, yi = x

        df_db = np.empty((beta.size, x.shape[1]))
        df_db[0] = 2 * (xc - xi)  # d_f/dxc
        df_db[1] = 2 * (yc - yi)  # d_f/dyc
        df_db[2] = -2 * r  # d_f/dr

        return df_db

    def jacd(beta, x):
        """ Jacobian function with respect to the input x.
        return df_3b/dx
        """
        xc, yc, r = beta
        xi, yi = x

        df_dx = np.empty_like(x)
        df_dx[0] = 2 * (xi - xc)  # d_f/dxi
        df_dx[1] = 2 * (yi - yc)  # d_f/dyi

        return df_dx

    def calc_estimate(data):
        """ Return a first estimation on the parameter from the data  """
        xc0, yc0 = data.x.mean(axis=1)
        r0 = np.sqrt((data.x[0] - xc0) ** 2 + (data.x[1] - yc0) ** 2).mean()
        return xc0, yc0, r0

    # for implicit function :
    #       data.x contains both coordinates of the points
    #       data.y is the dimensionality of the response
    lsc_data = odr.Data(points, y=1)
    lsc_model = odr.Model(f_3b, implicit=True, estimate=calc_estimate, fjacd=jacd, fjacb=jacb)
    lsc_odr = odr.ODR(lsc_data, lsc_model)  # beta0 has been replaced by an estimate function
    lsc_odr.set_job(deriv=3)  # use user derivatives function without checking
    if verbose:
        lsc_odr.set_iprint(iter=1, iter_step=1)  # print details for each iteration
    lsc_out = lsc_odr.run()

    xc_3b, yc_3b, R_3b = lsc_out.beta

    if compute_residuals:
        Ri_3b = calc_R(points[0, :], points[1, :], xc_3b, yc_3b)
        residu_3b = sum((Ri_3b - R_3b) ** 2)
    else:
        residu_3b = None

    return ((xc_3b, yc_3b), R_3b), residu_3b


