import numpy as np
from scipy.linalg import eig
from scipy import optimize
import mpmath
import warnings

def ellipse_center(params):
    """
    Compute ellipse center from conic params (follows https://mathworld.wolfram.com/Ellipse.html)
    :param params: conic params output by fit_ellipse()
    :return: center as a tuple
    """
    b, c, d, f, g, a = params[1] / 2, params[2], params[3] / 2, params[4] / 2, params[5], params[0]
    denom = b * b - a * c
    x0 = (c * d - b * f) / denom
    y0 = (a * f - b * d) / denom
    return x0.item(), y0.item()


def ellipse_ax_length(params):
    """
    Compute the length of the axes of the ellipse (follows https://mathworld.wolfram.com/Ellipse.html)
    :param params: conic params output by fit_ellipse()
    :return: length1 (major), length2 (minor)
    """
    a, b, c, d, f, g = params[0], params[1] / 2, params[2], params[3] / 2, params[4] / 2, params[5]
    numerator = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g)
    tmp1 = (b * b - a * c)
    tmp2 = np.sqrt(((a - c) * (a - c)) + 4 * b * b)
    tmp3 = (a + c)
    denom1 = tmp1 * (tmp2 - tmp3)
    denom2 = tmp1 * (-tmp2 - tmp3)
    length1 = np.sqrt(numerator / denom1).item()
    length2 = np.sqrt(numerator / denom2).item()
    if length1 > length2:
        return length1, length2
    else:
        print("Flipped a and b")
        return length2, length1


def ellipse_angle_of_rotation(params, eps=1e-15):
    """
    Compute counterclcockwise angle of rotation from the x-axis to the major exis of the ellipse
    The implementation follows https://mathworld.wolfram.com/Ellipse.html
    :param params: conic params output by fit_ellipse()
    :return: angle of rotation (in radians)
    """
    b, c, d, f, g, a = params[1] / 2, params[2], params[3] / 2, params[4] / 2, params[5], params[0]
    abs_b = np.abs(b).item()
    if abs_b < eps:
        # axis aligned ellipse (B = 0 approx.)
        if a < c:
            return 0.0
        else:
            return 0.5 * np.pi
    else:
        # ellipse is not aligned with x or y (B != 0)
        if a < c:
            return 0.5 * float(mpmath.acot((a - c)/(2 * b)))
        else:
            return 0.5 * (np.pi + float(mpmath.acot((a - c)/(2 * b))))


def fit_ellipse(data, method='stable', verify_solution=True):
    """
    Fit ellipse to a set of 2D points
    :param data: 2xN matrix with x y per column (should have at least 5 points)
    :param method: one of 'fitz', 'stable', 'ls'
    :param verify_solution: check that the ellipse is valid?
    :return: list with conic params for ellipse
    The 'fitz' method corresponds to A. Fitzgibbon et al., "Direct Least Square Fitting of Ellipses".
    The 'stable' method corresponds to Halir and and Flusser, "Numerically stable direct least squares fitting of ellipses"
    The 'ls' method corresponds to least squares per https://stackoverflow.com/questions/47873759/how-to-fit-a-2d-ellipse-to-given-points
    """
    assert data.shape[0] == 2, "Expected the data matrix to have 2 rows (got {} rows)".format(data.shape[0])
    assert data.shape[1] > 4, "Expected the data matrix to have more than 4 columns (got {} cols)".format(data.shape[1])

    x = data[0:1, :].T.astype(np.double)
    y = data[1:2, :].T.astype(np.double)

    if method == 'stable':

        # follows https://www.mathworks.com/matlabcentral/fileexchange/22684-ellipse-fit-direct-method
        # which is based on http://autotrace.sourceforge.net/WSCG98.pdf
        centroid_x = np.mean(x)
        centroid_y = np.mean(y)

        centered_x = x - centroid_x
        centered_y = y - centroid_y

        D1 = np.hstack((centered_x*centered_x, centered_x*centered_y, centered_y*centered_y))
        D2 = np.hstack((centered_x, centered_y, np.ones_like(x)))
        S1 = np.dot(D1.T, D1)
        S2 = np.dot(D1.T, D2)
        S3 = np.dot(D2.T, D2)

        # check if all points lie on a line
        if np.fabs(np.linalg.det(S3)) < 1e-10:
            raise RuntimeError("The points are very close to being collinear. Failed to fit an ellipse!")

        T = np.dot(-np.linalg.inv(S3), S2.T)
        M = S1 + np.dot(S2, T)
        M = np.vstack((M[2, :] * 0.5, -M[1, :], M[0, :] * 0.5))
        eigval, eigvec = eig(M)
        cond = np.multiply(4 * eigvec[0, :], eigvec[2, :]) - \
            np.multiply(eigvec[1, :], eigvec[1, :])
        a1 = eigvec[:, np.where(cond > 0)[0]]
        a = np.vstack((a1, np.dot(T, a1))).squeeze()

        # remove centering
        a3 = a[3] - 2*a[0]*centroid_x - a[1]*centroid_y
        a4 = a[4] - 2*a[2]*centroid_y - a[1]*centroid_x
        a5 = a[5] + a[0]*centroid_x*centroid_x + a[2]*centroid_y*centroid_y + \
            a[1]*centroid_x*centroid_y - a[3]*centroid_x - a[4]*centroid_y

        a[3] = a3
        a[4] = a4
        a[5] = a5

        params = a / np.linalg.norm(a)

    elif method == 'fitz':

        # try direct method by Fitzgibbon
        D = np.hstack((x * x, x * y, y * y, x, y, np.ones_like(x)))
        S = np.dot(D.T, D)
        C = np.zeros([6, 6], dtype=np.double)
        C[0, 2], C[2, 0], C[1, 1] = -2, -2, 1  # we flip the sign here following the pseudocode in the paper

        eigval, eigvec = eig(S, b=C)
        # print("eigenvalues", eigval)
        # print("eigenvec\n", eigvec)

        index, = np.where((eigval < 0) & (~ np.isinf(eigval)))  # only one eigval should satisfy the conditions

        if index.size == 0 or index.size > 1:
            # failed to find a solution. It could be that some points are collinear!
            raise RuntimeError('No unique (negative) singular value was found! Eigenvalues: {}'.format(eigval))

        # the direct method worked!
        params = np.squeeze(eigvec[:, index])

    elif method == 'ls':

        # fallback to iterative method...
        # formulate and solve the least squares problem ||Ax - b ||^2
        A = np.hstack((x * x, x * y, y * y, x, y))
        b = np.ones_like(x)
        x = np.linalg.lstsq(A, b, rcond=None)
        params = np.array(x[0].squeeze().tolist() + [-1.0])

    else:
        raise ValueError("Unrecognized fitting method for ellipse.")

    # print("LS params: {}".format(params))

    # flip sign of a if negative in the general conic form
    if params[0] < 0:
        for i, x in enumerate(params):
            params[i] = -params[i]  # multiply all by -1

    # last verification step!
    if verify_solution:

        # we know that for ellipses, b^2 - 4ac < 0
        # (see for example https://math.libretexts.org/Courses/Prince_George%27s_Community_College/MAT_1350%3A_Precalculus_Part_I/12%3A_Analytic_Geometry/12.04%3A_Rotation_of_Axes )
        constraint = params[1]*params[1] - 4*params[0]*params[2]
        if constraint > -1e-6:
            raise RuntimeError("The fitted ellipse should satisfy b^2 - 4ac < 0, but b^2 - 4ac is close to zero ({})".
                               format(constraint))

        # we also check that the ratio of the axes is not too far from each other
        length1, length2 = ellipse_ax_length(params)
        if not np.isfinite(length1) or not np.isfinite(length2):
            raise RuntimeError("The fitted ellipse had nonfinite axis length (ax1_length={}, ax2_length={}).".
                               format(length1, length2))

        if length1 > 5:
            raise RuntimeError("The fitted ellipse has a major semi-axis of {} units (>5), but this is not likely for group "
                               "conversations. The fitting must have given a bad result!".format(length1))

    return params

