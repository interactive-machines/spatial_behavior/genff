import numpy as np
import math
import cv2

class IndividualLoss(object):
    """
    Base class for optimization losses. The main assumption is that the final
    loss is composed of three components:

    1. Personal space loss - penalty for violating personal space
    2. Circular loss - penalty from deviating from the fitted circle/ellipse
    3. Environment loss - penalty for positioning the individual in a non-free environment cell

    Subclasses must implement the circular loss function.
    """

    def __init__(self, context, env_map=None, env_res=None,
                 personal_sigma=0.21, nonfree_penalty=100.0, weight_psloss=0.2, weight_closs=0.2,
                 weight_eloss=0.5, blur_map=True):
        """
        Loss for computing the position of an individual
        :param context: context with valid people only
        :param env_map: environment map (centered as the context)
        :param env_res: environment resolution
        :param personal_sigma: sigma for personal space gaussian bump (we recommend intimate_space/2.0 such that 95% of the dist corresponds to intimate space)
        :param nonfree_penalty: penalty for nonfree space
        :param weight_psloss: weight for personal space loss
        :param weight_closs: weight for circular loss
        :param weight_eloss: weight for environment loss
        :param blur_map: blur the map?
        """
        self.context = context
        self.env_map = env_map  # this is the crooped map (4 channels), already centered as the context
        self.env_res = env_res
        self.blur_map = blur_map

        # blur env map
        self.blurred_free_space = None
        if self.blur_map and self.env_map is not None:
            # the first channel is free space
            self.blurred_free_space = cv2.GaussianBlur(self.env_map[:, :, 0] +
                                                       self.env_map[:, :, 2] +
                                                       self.env_map[:, :, 3], (5, 5), 0)  # unknown + tall + short obs

        self.personal_sigma = personal_sigma
        self.nonfree_penalty = nonfree_penalty

        self.weight_psloss = weight_psloss
        self.weight_closs = weight_closs
        self.weight_eloss = weight_eloss

    def personal_space_loss(self, x, y):
        """
        Penalize violations to personal space
        :param x: individual's x coordinate
        :param y: individual's y coordinate
        """
        diff = np.repeat([[x, y]], self.context.shape[0], axis=0) - self.context

        # we now compute gaussian pdf assuming diag Sigma with the same variance for x,y
        variance = self.personal_sigma * self.personal_sigma
        det_Sigma = variance * variance  # sigma is diag => the det is the product of diag elements
        denom = math.sqrt(4 * np.pi * np.pi * det_Sigma)
        # (x-mu)^T Sigma^-1 (x-mu) can be simplified as below because Sigma is diag
        num = np.exp(-0.5 * np.sum(np.power(diff, 2), axis=1) / variance)
        pdf = num / denom  # gaussian pdf
        return np.sum(pdf)

    def circular_loss(self, x, y):
        """
        Penalize distance from P-space (along the circle/ellipse)
        :param x: individual's x coordinate
        :param y: individual's y coordinate
        """
        return 0.0  # to be implemented by subclasses

    def env_loss(self, x, y):
        """
        Penalize not being in free space
        :param x: individual's x coordinate
        :param y: individual's y coordinate
        """
        if self.env_map is None:  # no penalty if there's no environment
            return 0.0

        offset_x = (self.env_map.shape[0] * 0.5) * self.env_res
        offset_y = (self.env_map.shape[1] * 0.5) * self.env_res

        x = offset_x + x
        y = offset_y + y

        #c = min(max(int((x / self.env_res)), 0), self.env_map.shape[1] - 1)
        #r = min(max(int((y / self.env_res)), 0), self.env_map.shape[0] - 1)

        c = int((x / self.env_res))
        r = int((y / self.env_res))

        # if the position is outside of the map, penalize as if it was not free
        if c < 0 or c >= self.env_map.shape[1] or r < 0 or r >= self.env_map.shape[0]:
            return self.nonfree_penalty

        if self.blur_map:
            environment = self.blurred_free_space
        else:
            environment = self.env_map[:, :, 0] + \
                          self.env_map[:, :, 2] + \
                          self.env_map[:, :, 3]  # unknown + tall + short obstacles

        env_value = environment[r, c]
        return env_value*self.nonfree_penalty

    def __call__(self, input_value):
        """
        Compute loss for a given input
        :param input_value: input value (expected to have two elements: x,y coordinates)
        """
        x = input_value[0]
        y = input_value[1]

        l1 = 0.0
        if self.weight_psloss > 0.0:
            l1 = self.weight_psloss * self.personal_space_loss(x, y)

        l2 = 0.0
        if self.weight_closs > 0.0:
            l2 = self.weight_closs * self.circular_loss(x, y)

        l3 = 0.0
        if self.weight_eloss > 0.0:
            l3 = self.weight_eloss * self.env_loss(x, y)

        return l1 + l2 + l3


class CircleLoss(IndividualLoss):
    """
    Loss for when a circle is fitted to the context
    """

    def __init__(self, center, radius, context, env_map=None, env_res=None,
                 personal_sigma=0.21, nonfree_penalty=100.0, weight_psloss=0.2, weight_closs=0.2,
                 weight_eloss=0.5):
        """
        Constructor
        :param center: tuple with circle center (x,y)
        :param radius: circle radius
        :param context: context with valid people only
        :param env_map: environment map (centered as the context)
        :param env_res: environment resolution
        :param personal_sigma: sigma for personal space gaussian bump (we recommend intimate_space/2.0 such that 95% of the dist corresponds to intimate space)
        :param nonfree_penalty: penalty for nonfree space
        :param weight_psloss: weight for personal space loss
        :param weight_closs: weight for circular loss
        :param weight_eloss: weight for environment loss
        """
        super(CircleLoss, self).__init__(context=context, env_map=env_map, env_res=env_res,
                                         personal_sigma=personal_sigma, nonfree_penalty=nonfree_penalty,
                                         weight_psloss=weight_psloss, weight_closs=weight_closs,
                                         weight_eloss=weight_eloss)
        self.center = center
        self.radius = radius

    def circular_loss(self, x, y):
        """
        Penalize distance from P-space (along the circle/ellipse)
        :param x: individual's x coordinate
        :param y: individual's y coordinate
        :note Implements the (non-squared) geometric distance as in https://www.emis.de/journals/BBMS/Bulletin/sup962/gander.pdf (sec. 3)
        """
        dx = self.center[0] - x
        dy = self.center[1] - y
        distance_to_center = math.fabs(np.sqrt((dx * dx) + (dy * dy)) - self.radius)
        return distance_to_center


class EllipseLoss(IndividualLoss):

    def __init__(self, center, ax_length, radians, context, env_map=None, env_res=None,
                 personal_sigma=0.21, nonfree_penalty=100.0, weight_psloss=0.2, weight_closs=0.2, weight_eloss=0.5):
        """
        Constructor
        :param center: tuple with the ellipse center (x,y)
        :param ax_length: length of the ellipse's semi-major and semi-minor axes (a,b)
        :param radians: ellipse orientation
        :param context: context with valid people only
        :param env_map: environment map (centered as the context)
        :param env_res: environment resolution
        :param personal_sigma: sigma for personal space gaussian bump (we recommend intimate_space/2.0 such that 95% of the dist corresponds to intimate space)
        :param nonfree_penalty: penalty for nonfree space
        :param weight_psloss: weight for personal space loss
        :param weight_closs: weight for circular loss
        :param weight_eloss: weight for environment loss
        """
        super(EllipseLoss, self).__init__(context=context, env_map=env_map, env_res=env_res,
                                          personal_sigma=personal_sigma, nonfree_penalty=nonfree_penalty,
                                          weight_psloss=weight_psloss, weight_closs=weight_closs,
                                          weight_eloss=weight_eloss)
        self.center = center
        self.ax_length = ax_length
        self.radians = radians

    def distance_to_ellipse(self, x1, y1):
        """
        Helper function to compute the distance to the (unrotated) ellipse
        :param x1: x coordinate in the ellipse's frame
        :param y1: y coordinate in the ellipse's frame
        :note this implementation is based on https://wet-robots.ghost.io/simple-method-for-distance-to-ellipse/
        See https://stackoverflow.com/a/46007540 for a discussion on computing the distance
        """

        def solve(semi_major, semi_minor, p):
            """
            Solve for closest point on ellipse
            """
            px = abs(p[0])
            py = abs(p[1])

            tx = 0.707
            ty = 0.707

            a = semi_major
            b = semi_minor

            for x in range(0, 3):
                x = a * tx
                y = b * ty

                ex = (a * a - b * b) * tx ** 3 / a
                ey = (b * b - a * a) * ty ** 3 / b

                rx = x - ex
                ry = y - ey

                qx = px - ex
                qy = py - ey

                r = math.hypot(ry, rx)
                q = math.hypot(qy, qx)

                tx = min(1, max(0, (qx * r / q + ex) / a))
                ty = min(1, max(0, (qy * r / q + ey) / b))
                t = math.hypot(ty, tx)
                tx /= t
                ty /= t

            return (math.copysign(a * tx, p[0]), math.copysign(b * ty, p[1]))

        a, b = self.ax_length
        x2, y2 = solve(a, b, (x1, y1))
        dx = x1 - x2
        dy = y1 - y2
        return math.sqrt(dx * dx + dy * dy)

    def circular_loss(self, x, y):
        """
        Penalize distance from P-space (along the circle/ellipse)
        :param x: individual's x coordinate
        :param y: individual's y coordinate
        :note Implements the geometric distance as in https://www.emis.de/journals/BBMS/Bulletin/sup962/gander.pdf (sec. 3)
        but applied to ellipse
        """
        # compute x,y relative to the ellipse's center
        x = self.center[0] - x
        y = self.center[1] - y
        # rotate x,y so that it's in the ellipse's coord frame
        x1 = x * math.cos(-self.radians) - y * math.sin(-self.radians)
        y1 = x * math.sin(-self.radians) + y * math.cos(-self.radians)
        return self.distance_to_ellipse(x1, y1)
