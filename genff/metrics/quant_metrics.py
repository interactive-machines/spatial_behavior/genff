"""
Metrics for evaluating how well groups conform to F-formations
"""
from abc import ABC, abstractmethod
import numpy as np
import math
from genff.opt_method.opt_generator import torch_to_numpy, opt_generator_loss
from genff.opt_method.opt_loss import IndividualLoss
from genff.data_handling.utils import moveAngleToPIRange, minAngleDiff

class BaseFFormationMetric(ABC):
    """Base metric"""

    def __init__(self, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        self.x_index = x_index
        self.y_index = y_index
        self.angle_index = angle_index

    @abstractmethod
    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: metric value and extra information as a tuple
        """
        pass  # to be implemented by subclasses

    def __call__(self, individual, context, mask=None, env=None, eres=None):
        """
        Compute metric for a given group
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: metric value
        """
        d, *extra_outputs = self.compute_metric(individual, context, mask=mask, env=env, eres=eres)
        return d


class CircFitMetric(BaseFFormationMetric):
    """
    Perpendicular distance from a generated interactant to a circleor ellipse that has been fitted to the other members
    of the group. Range is [inf, 0]. The lower the better.
    """

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: distance to circular shape, params for circular shape as dict
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())
        np_env = torch_to_numpy(env.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        # create loss for fitting circle/ellipse
        loss, params = opt_generator_loss(np_context, env_map=np_env, env_res=eres,
                                          x_index=self.x_index, y_index=self.y_index, angle_index=self.angle_index,
                                          weight_psloss=0.0, weight_closs=1.0, weight_eloss=0.0,
                                          verbose=False)

        d = loss([np_individual[0, self.x_index], np_individual[0, self.y_index]])

        return d, params


class NotFreeMetric(BaseFFormationMetric):
    """
    Binary metric: 1 if the individual is in a non-free cell of the map; 0 otherwise
    """

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return 1 if the individual is not in free space, or 0 otherwise
        """
        if env is None or eres is None:
            raise RuntimeError("Cannot compute NotFreeMetric without environment map (check env, eres)")

        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())
        np_env = torch_to_numpy(env.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        # create loss for fitting circle/ellipse
        loss = IndividualLoss(np_context, env_map=np_env, env_res=eres,
                              nonfree_penalty=1.0, weight_psloss=0.0, weight_closs=0.0, weight_eloss=1.0,
                              blur_map=False)

        d = loss([np_individual[0, self.x_index], np_individual[0, self.y_index]])
        epsilon = 0.005
        if (1 - d) < (1 + epsilon) and (1 - d) > (1 - epsilon):
            d = np.float64(0.0)
        return d,


class PersonalSpaceMetric(BaseFFormationMetric):
    """
    Number of times the individual is closer to a member of the group than a given threshold (corresponding to personal space)
    """

    def __init__(self, threshold=0.68, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param threshold: distance threshold
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        super(PersonalSpaceMetric, self).__init__(x_index=x_index, y_index=y_index, angle_index=angle_index)
        self.threshold = threshold

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: number of violations to distance threshold in the group
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        ind_pos = np_individual[:, [self.x_index, self.y_index]]
        con_pos = np_context[:, [self.x_index, self.y_index]]
        diff = np.repeat(ind_pos, con_pos.shape[0], axis=0) - con_pos
        dist = np.linalg.norm(diff, axis=1)
        instances = np.maximum(0, np.full_like(dist, self.threshold) - dist)
        out = np.count_nonzero(instances)
        return out,


class IntimateSpaceMetric(PersonalSpaceMetric):
    """
    Number of times the individual is closer to a member of the group than a given threshold (corresponding to intimate space)
    """

    def __init__(self, threshold=0.42, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param threshold: distance threshold
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        super(IntimateSpaceMetric, self).__init__(threshold=threshold,
                                                  x_index=x_index,
                                                  y_index=y_index,
                                                  angle_index=angle_index)


def ospace_proposal(tensor, x_index, y_index, angle_index, d=0.72):
    """
    Helper function to compute o-space proposals based on M Vazquez's thesis (Appendix B)
    :param tensor: NxF tensor with people's features
    :param x_index: index for the x coordinate in the tensor
    :param y_index: index for the y coordinate in the tensor
    :param angle_index: index for the angle in the tensor
    :param d: stride parameter for the o-space proposal
    """
    op = tensor[:, [x_index, y_index]] + d * np.stack((np.cos(tensor[:, angle_index]),
                                                       np.sin(tensor[:, angle_index])), axis=1)
    assert op.shape[0] == tensor.shape[0], \
        "Expected the computed proposal tensor to have {} rows (got {})".format(tensor.shape[0], op.shape[0])
    assert op.shape[1] == 2, \
        "Expected the computed proposal tensor to have 2 columns (got {})".format(op.shape[1])
    return op


def average_ospace_proposal(individual, context, x_index, y_index, angle_index, d=0.72):
    """
    Helper function to compute o-space proposals and their average location
    :param individual: individual tensor as a numpy array (or None for no individual)
    :param context: context tensor as a numpy array
    :param x_index: index for the x coordinate in the tensor
    :param y_index: index for the y coordinate in the tensor
    :param angle_index: index for the angle in the tensor
    :param d: stride parameter for the o-space proposal
    :return avg_proposal, all_proposals (first row is individual, the rest is context)
    """
    op_context = ospace_proposal(context, x_index, y_index, angle_index, d=d)

    if individual is not None:
        op_individual = ospace_proposal(individual, x_index, y_index, angle_index, d=d)
        all_proposals = np.vstack((op_individual, op_context))
    else:
        all_proposals = op_context

    # compute average proposal
    avg_proposal = np.mean(all_proposals, axis=0, keepdims=True)
    assert avg_proposal.shape[0] == 1, \
        "Expected the avg proposal to have 1 row (got {})".format(avg_proposal.shape[0])
    assert avg_proposal.shape[1] == 2, \
        "Expected the avg proposal to have 2 cols (got {})".format(avg_proposal.shape[1])

    return avg_proposal, all_proposals


class CenterDistMetric(BaseFFormationMetric):
    """
    Average distance from o-space proposal to average o-space proposal for the whole group (the lower, the better)
    """

    def __init__(self, d=0.72, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param d: stride parameter for the o-space proposal
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        super(CenterDistMetric, self).__init__(x_index=x_index,
                                               y_index=y_index,
                                               angle_index=angle_index)
        self.d = d

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: avg distance to group's avg o-space proposal, (avg o-space proposal, all_proposals)
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        avg_proposal, all_proposals = average_ospace_proposal(np_individual, np_context,
                                                              self.x_index, self.y_index, self.angle_index,
                                                              d=self.d)

        # compute distance to average proposal
        diff = all_proposals - np.repeat(avg_proposal, all_proposals.shape[0], axis=0)
        dist = np.linalg.norm(diff, axis=1)
        assert dist.shape[0] == all_proposals.shape[0], \
            "Expected the distance vector to have {} rows (got {})".format(all_proposals.shape[0], dist.shape[0])

        return np.mean(dist, dtype=np.float64), (avg_proposal, all_proposals)


class ChangeCenterDistMetric(CenterDistMetric):
    """
    Change in distance from o-space proposal to average o-space proposal for the group (context) after adding individual
    :note: This metric did not seem to be useful in our early exploration. It's left here for historic reasons.
    """

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: diff CenterDist, avg_proposal without ind, avg_proposal with ind, all proposals
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        avg_proposal1, context_proposals = average_ospace_proposal(None, np_context,
                                                   self.x_index, self.y_index, self.angle_index,
                                                   d=self.d)
        avg_proposal2, all_proposals = average_ospace_proposal(np_individual, np_context,
                                                               self.x_index, self.y_index, self.angle_index,
                                                               d=self.d)



        # compute distance to average proposal
        diff1 = context_proposals - np.repeat(avg_proposal1, context_proposals.shape[0], axis=0)
        dist1 = np.linalg.norm(diff1, axis=1)

        diff2 = all_proposals - np.repeat(avg_proposal1, all_proposals.shape[0], axis=0)
        dist2 = np.linalg.norm(diff2, axis=1)

        # d1, extra1 = super(ChangeCenterDistMetric, self).__call__(None, context, mask=mask, env=env, eres=eres)
        # d2, extra2 = super(ChangeCenterDistMetric, self).__call__(individual, context, mask=mask, env=env, eres=eres)

        return np.mean(dist1) - np.mean(dist2), avg_proposal1, avg_proposal2, all_proposals


class OccludesOtherMetric(BaseFFormationMetric):
    """
    Number of times the individual occludes another member of the group relative to the o-space average
    """

    def __init__(self, r=0.2, d=0.42, min_dist=0.1, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param r: min perpendicular distance to group member (suggested value: half shoulder width for a person)
        :param d: stride parameter for the o-space proposal
        :param min_distance: min distance to say the agent is on top of o-space (in which case it occludes it!)
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        super(OccludesOtherMetric, self).__init__(x_index=x_index,
                                                  y_index=y_index,
                                                  angle_index=angle_index)
        self.r = r
        self.d = d
        self.min_dist = min_dist

    def _occlusion(self, v1, v2):
        """
        Helper function to detect if person 1 occludes person 2
        :param v1: position for person 1 relative to the o-space center
        :param v2: position for person 2 relative to the o-space center
        :return 1 if occluded; 0 otherwise
        """
        assert len(v1.shape) == 1, "The input v1 vector must be 1D (shape = {})".format(v1.shape)
        assert len(v2.shape) == 1, "The input v2 vector must be 1D (shape = {})".format(v2.shape)

        n1 = np.linalg.norm(v1)
        n2 = np.linalg.norm(v2)
        # print("n1: {}, n2: {}".format(n1, n2))

        if n1 < self.min_dist:
            return 1.0
        elif n1 < n2:
            tau = moveAngleToPIRange(math.atan2(self.r, n2))
            prod = np.dot(v1, v2) / (n1 * n2)
            try:
                p = max(min(prod.item(), 1.0), -1.0)
                angle = math.acos(p)
                angle = moveAngleToPIRange(angle)
                # print("angle: {}, tau: {}".format(angle, tau))
            except ValueError as e:
                print("Got value error for prod: {}, p: {}".format(prod.item(), p))
                print(e)
                raise e

            if math.fabs(angle) < tau:
                return 1.0

        return 0.0

    def _compute_v_vectors(self, individual, context, mask):
        """
        Helper function to compute vectors p - ospace
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        return v_individual, v_context (as a matrix)
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        # we exclude the individual from the avg o-space proposal
        avg_proposal, _ = average_ospace_proposal(None, np_context,
                                                  self.x_index, self.y_index, self.angle_index, d=self.d)

        v_ind = np_individual[:, [self.x_index, self.y_index]] - avg_proposal
        v_con = np_context[:, [self.x_index, self.y_index]] - np.repeat(avg_proposal, np_context.shape[0], axis=0)

        return v_ind, v_con, avg_proposal

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return number of occlusions, (average proposal, v individual, v context)
        """
        v_ind, v_con, avg_proposal = self._compute_v_vectors(individual, context, mask)

        instances = 0
        for j in range(v_con.shape[0]):  # for each context member
            instances += self._occlusion(v_ind[0,:], v_con[j, :])

        return instances, (avg_proposal, v_ind, v_con)


class IsOccludedMetric(OccludesOtherMetric):
    """
    Number of times the individual is occluded by another member of the group relative to the o-space average
    """

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return number of occlusions, (average proposal, v individual, v context)
        """
        v_ind, v_con, avg_proposal = self._compute_v_vectors(individual, context, mask)

        instances = 0
        for j in range(v_con.shape[0]):  # for each context member
            instances += self._occlusion(v_con[j, :], v_ind[0, :])  # we swap the order w.r.t. OccludesOther

        return instances, (avg_proposal, v_ind, v_con)


class OrientMetric(BaseFFormationMetric):
    """
    Absolute angular deviation (in degrees) from the new individual orientation to the o-space center of the context
    """

    def __init__(self, use_individual=False, fit_circ=False, d=0.72, x_index=0, y_index=1, angle_index=2):
        """
        Constructor
        :param use_individual: use the individual position when computing the o-space?
        :param fit_circ: if True, the center is computed by fitting circular shape; otherwise, we use o-space proposals
        :param d: stride parameter for the o-space proposal
        :param x_index: index for the x-coordinate in the features
        :param y_index: index for the y-coordinate in the features
        :param angle_index: index for the body orientation in the features
        """
        super(OrientMetric, self).__init__(x_index=x_index,
                                           y_index=y_index,
                                           angle_index=angle_index)
        self.d = d
        self.use_individual = use_individual
        self.fit_circ = fit_circ

    def compute_metric(self, individual, context, mask=None, env=None, eres=None):
        """
        Internal method to compute metric for a given group and output any other useful information for debugging
        :param individual: 1xF individual tensor
        :param context: NxF context tensor
        :param mask: 1xN mask for the context (or None for no mask)
        :param env: HxWx4 environment tensor (or None for no environment map)
        :param eres: environment resolution
        :return: diff CenterDist, ideal angle, group center (either via fit circ or o-space proposals)
        """
        # transform torch data to numpy
        np_individual = torch_to_numpy(individual.cpu())
        np_context = torch_to_numpy(context.cpu())
        np_mask = torch_to_numpy(mask.cpu())
        np_env = torch_to_numpy(env.cpu())

        if np_mask is not None:
            # keep only valid context
            np_context = np_context[np_mask > 0, :]

        if self.fit_circ:
            # fit circle/ellipse
            if self.use_individual:
                data = np.vstack((np_individual, np_context))
            else:
                data = np_context
            _, params = opt_generator_loss(data, env_map=np_env, env_res=eres,
                                           x_index=self.x_index, y_index=self.y_index, angle_index=self.angle_index,
                                           weight_psloss=0.0, weight_closs=1.0, weight_eloss=0.0,
                                           verbose=False)
            center = np.array([[params['center'][0], params['center'][1]]])
        else:
            # compute o-space center
            ind = None
            if self.use_individual:
                ind = np_individual
            center, _ = average_ospace_proposal(ind, np_context,
                                                self.x_index, self.y_index, self.angle_index, d=self.d)

        # vector from individual to o-space
        v = center - np_individual[:, [self.x_index, self.y_index]]
        ideal_angle = np.arctan2(v[0, 1], v[0, 0])

        angle_diff = minAngleDiff(np_individual[0, self.angle_index], ideal_angle)
        return np.fabs(angle_diff * 180.0 / np.pi), ideal_angle, center
