import matplotlib.pyplot as plt
import torch
import numpy as np
from genff.data_handling.transform import RectangularAngle


def convert_to_rect_representation(individual, context, mask, env=None, angle_index=2):
    """
    Helper function to convert a sample from 3 features to 4 features (rectangular representation)
    :param individual: NxF individual tensor
    :param context: NxF context tensor
    :param mask: Nx1 mask vector
    :param env: NxKxKx4 environment
    :param angle_index: angle index
    :return new individual and new context in rectangular representation
    """
    transform = RectangularAngle(angle_index=angle_index)

    # make fake batch
    ind = torch.unsqueeze(individual, 0)
    con = torch.unsqueeze(context, 0)
    m = torch.unsqueeze(mask, 0)
    if env is not None:
        e = torch.unsqueeze(env, 0)
    else:
        e = env

    # convert from 3 features to 4 features (angular representation)
    new_ind, new_con, *other = transform((ind, con, m, e))

    return new_ind[0, :, :], new_con[0, :, :]


def plot_sample_on_full_map_rect(individual, context, mask, env_map, env_res,
                                 env_left, env_top, ax=None, x_index=0, y_index=1, cos_index=2, sin_index=3):
    """
    Plot sample inputs
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param env_map: environment map (WxHx4)
    :param env_res: environment resolution
    :param env_left: x coordinate for the environment top-left corner
    :param env_top: y coordinate for the environment top-left corner
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param cos_index: index for the cosine of the angle in the individual and context tensors
    :param sin_index: index for the sine of the angle in the individual and context tensors
    """

    if ax is None:
        f = plt
    else:
        f = ax

    shape = env_map.shape
    y1 = env_top
    y2 = env_top + env_res*shape[0]
    x1 = env_left
    x2 = env_left + env_res*shape[1]

    # env_map for CP is 28x24
    extent = [x1 - env_res*0.5, x2 - env_res*0.5, y1 - env_res*0.5, y2 - env_res*0.5]  # left right bottom top - https://matplotlib.org/3.1.1/tutorials/intermediate/imshow_extent.html

    if torch.is_tensor(env_map):
        grid = torch.flip(env_map, [1])  # flip to make the grid coord frame be aligned with world
    else:
        grid = np.fliplr(env_map)

    f.imshow(grid, extent=extent, origin='lower', interpolation="bilinear")

    f.plot(context[mask.bool(), x_index], context[mask.bool(), y_index], 'oc')

    f.quiver(context[mask.bool(), x_index],
             context[mask.bool(), y_index],
             context[mask.bool(), cos_index],
             context[mask.bool(), sin_index],
             color="c")
    f.quiver(individual[0, x_index],
             individual[0, y_index],
             individual[0, cos_index],
             individual[0, sin_index],
             color="w")


def plot_sample_on_full_map(individual, context, mask, env_map, env_res,
                            env_left, env_top, ax=None, x_index=0, y_index=1, angle_index=2):
    """
    Plot sample inputs
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param env_map: environment map (WxHx4)
    :param env_res: environment resolution
    :param env_left: x coordinate for the environment top-left corner
    :param env_top: y coordinate for the environment top-left corner
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param angle_index: index for the angle in the individual and context tensors
    :return:
    """
    new_ind, new_con = convert_to_rect_representation(individual, context, mask, env_map, angle_index)
    plot_sample_on_full_map_rect(new_ind, new_con, mask, env_map, env_res, env_left, env_top, ax,
                                 x_index if x_index < angle_index else x_index + 1,
                                 y_index if y_index < angle_index else y_index + 1,
                                 angle_index, angle_index+1)


def plot_sample_rect(individual, context, mask, env_cropped=None, env_res=None, ax=None,
                     x_index=0, y_index=1, cos_index=2, sin_index=3, mode='all', plot_center=False,
                     ind_color='b', con_color='c'):
    """
    Plot sample inputs (assumes rectangular angle representation)
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param env_cropped: cropped environment (WxHx4)
    :param env_res: environment resolution
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param cos_index: index for the cosine of the angle in the individual and context tensors
    :param sin_index: index for the cosine of the angle in the individual and context tensors
    :param mode: plotting mode: 'all' for channels 1-3, 'free' for channel 1 only, 'unknown' for channel 0 only
    :param plot_center: if True, a diamond is plotted in the center of the image -- should correspond to (0,0)
    :param ind_color: color for the individual in the plot
    :param con_color: color for the context in the plot
    """

    if ax is None:
        f = plt
    else:
        f = ax

    if mode == 'all':
        f.imshow(env_cropped[:, :, 1:].cpu(), origin='lower', vmin=0.0, vmax=1.0)  # no need to flip here, because this map was flipped when cropped
    elif mode == 'free':
        f.imshow(env_cropped[:, :, 1].cpu(), origin='lower', vmin=0.0, vmax=1.0)
    elif mode == 'unknown':
        f.imshow(env_cropped[:, :, 0].cpu(), origin='lower', vmin=0.0, vmax=1.0)
    else:
        raise ValueError("Invalid mode {} (expected one of: all, free, unknown)".format(mode))

    offset_x = (env_cropped.shape[1] * 0.5) * env_res
    offset_y = (env_cropped.shape[0] * 0.5) * env_res
    if individual is not None:
        f.quiver((offset_x + individual[:, x_index].cpu())/env_res,
                 (offset_y + individual[:, y_index].cpu())/env_res,
                 individual[:, cos_index].cpu(),
                 individual[:, sin_index].cpu(),
                 color=ind_color)

    f.quiver((offset_x + context[mask.bool(), x_index].cpu()) / env_res,
             (offset_y + context[mask.bool(), y_index].cpu()) / env_res,
             context[mask.cpu().bool(), cos_index].cpu(),
             context[mask.cpu().bool(), sin_index].cpu(),
             color=con_color)



    if plot_center:
        f.plot(offset_x / env_res, offset_y / env_res, 'dw')


def plot_sample(individual, context, mask, env_cropped=None, env_res=None, ax=None,
                x_index=0, y_index=1, angle_index=2, mode='all', plot_center=False,
                ind_color='b', con_color='c'):
    """
    Plot sample inputs
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param env_cropped: cropped environment (WxHx4)
    :param env_res: environment resolution
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param angle_index: index for the angle in the individual and context tensors
    :param mode: plotting mode: 'all' for channels 1-3, 'free' for channel 1 only, 'unknown' for channel 0 only
    :param plot_center: if True, a diamond is plotted in the center of the image -- should correspond to (0,0)
    :param ind_color: color for the individual in the plot
    :param con_color: color for the context in the plot
    """
    new_ind, new_con = convert_to_rect_representation(individual, context, mask, env_cropped, angle_index)
    plot_sample_rect(new_ind, new_con, mask, env_cropped, env_res, ax,
                     x_index if x_index < angle_index else x_index + 1,
                     y_index if y_index < angle_index else y_index + 1,
                     angle_index, angle_index+1, mode=mode, plot_center=plot_center,
                     ind_color=ind_color, con_color=con_color)


def plot_sample_no_map(individual, context, mask, ax=None, x_index=0, y_index=1,
                       angle_index=2, ind_color='w', con_color='c'):
    """
    Plot sample without a map
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param angle_index: index for the angle in the individual and context tensors
    :param ind_color: color for the individual
    :param con_color: color for the context
    """
    new_ind, new_con = convert_to_rect_representation(individual, context, mask, None, angle_index)

    if ax is None:
        f = plt
    else:
        f = ax

    f.quiver(new_con[mask.bool(), x_index],
             new_con[mask.bool(), y_index],
             new_con[mask.bool(), angle_index],
             new_con[mask.bool(), angle_index+1],
             color=con_color)

    f.quiver(new_ind[0, x_index],
             new_ind[0, y_index],
             new_ind[0, angle_index],
             new_ind[0, angle_index+1],
             color=ind_color)


def plot_sample_no_map_rect(individual, context, mask, ax=None, x_index=0, y_index=1,
                           c_index=2, s_index=3, ind_color='b', con_color='c'):
    """
    Plot sample (in rect angle representation) without a map
    :param individual: individual tensor (1xF) - after applying centering transform
    :param context: context tensor (NxF) - after applying centering transform
    :param mask: mask for the context tensor (Nx1)
    :param ax: matplotlib axes onto which do the plotting
    :param x_index: index for the x coordinate in the individual and context tensors
    :param y_index: index for the y coordinate in the individual and context tensors
    :param c_index: index for the cos(angle) in the individual and context tensors
    :param s_index: index for the sin(angle) in the individual and context tensors
    :param ind_color: color for the individual
    :param con_color: color for the context
    """

    if ax is None:
        f = plt
    else:
        f = ax

    if context is not None:
        f.quiver(context[mask.bool(), x_index],
                 context[mask.bool(), y_index],
                 context[mask.bool(), c_index],
                 context[mask.bool(), s_index],
                 color=con_color)

    if individual is not None:
        f.quiver(individual[0, x_index],
                 individual[0, y_index],
                 individual[0, c_index],
                 individual[0, s_index],
                 color=ind_color)
