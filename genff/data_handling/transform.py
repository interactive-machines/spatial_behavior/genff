import torch
import numpy as np

class RectangularAngle(object):
    """
    Convert the angle (theta) feature in an input tensor to its rectangular (2D) representation: cos(theta), sin(theta)
    """

    def __init__(self, angle_index=2):
        """
        Constructor
        :param angle_index: (int or list) position of the orientation feature in the second dim of the input tensors
        """
        if not isinstance(angle_index, list):
            self.angle_index = [angle_index]
        else:
            self.angle_index = angle_index

    def __call__(self, sample):
        """Transform a given sample
        :param sample: Dataset batch
        :return: transformed sample
        """
        individual_input, context_input, mask_input, env_input = sample

        if len(individual_input.shape) != 3:
            raise RuntimeError("Expected the individual_input in the sample to have 3 dimensions, but got {} (shape={}). Tensor:\n{}".
                               format(len(individual_input.shape), individual_input.shape, individual_input))
        if len(context_input.shape) != 3:
            raise RuntimeError("Expected the context_input in the sample to have 3 dimensions, but got {} (shape={}). Tensor:\n{}".
                               format(len(context_input.shape), context_input.shape, context_input))
        if individual_input.shape[-1] != context_input.shape[-1]:
            raise RuntimeError("Expected the individual and context tensors to have the same number of features "
                               "(got {} and {})".format(individual_input.shape[-1], context_input.shape[-1]))

        # compute new number of features, which dimensions we are keeping, and allocate output tensors
        final_dims = individual_input.shape[-1] + len(self.angle_index)

        keep_dims = list(set([x for x in range(individual_input.shape[-1])]) - set(self.angle_index))

        new_individual = \
            torch.zeros((individual_input.shape[0], individual_input.shape[1], final_dims),
                        dtype=individual_input.dtype, device=individual_input.device)
        new_context = \
            torch.zeros((context_input.shape[0], context_input.shape[1], final_dims),
                        dtype=context_input.dtype, device=context_input.device)

        # copy same values
        for i, index in enumerate(keep_dims):
            new_individual[:, :, i] = individual_input[:, :, index]
            new_context[:, :, i] = context_input[:, :, index]

        # transform angles
        k = len(keep_dims)
        for i, index in enumerate(self.angle_index):
            new_individual[:, :, k + i*2] = torch.cos(individual_input[:, :, index])
            new_individual[:, :, k + i*2 + 1] = torch.sin(individual_input[:, :, index])
            new_context[:, :, k + i*2] = torch.cos(context_input[:, :, index])
            new_context[:, :, k + i*2 + 1] = torch.sin(context_input[:, :, index])

        return new_individual, new_context, mask_input, env_input

    def __repr__(self):
        return self.__class__.__name__ + '(angle_index={})'.\
            format(self.angle_index)


class AddNoiseToContextAngles(object):
    """
    Add a bit of noise to the angles (theta) feature in the context tensor of a sample.
    @note Assumes that the input tensor is not in the rectangular representation!
    """

    def __init__(self, angle_index=2, sigma=0.349066):
        """
        Constructor
        :param angle_index: (int or list) position of the orientation feature in the second dim of the input tensors
        :param sigma: (float) std for sampling angular deviations (dflt: 20 degrees in radians)
        """
        self.angle_index = angle_index
        self.sigma = sigma

    def __call__(self, sample):
        """Transform a given sample
        :param sample: Dataset batch
        :return: transformed sample
        """
        individual_input, context_input, mask_input, env_input = sample

        deviations = torch.normal(0.0, self.sigma, size=(context_input.shape[0], context_input.shape[1], 1))
        context = torch.clone(context_input)
        context[:, :, self.angle_index:self.angle_index+1] = \
            context_input[:, :, self.angle_index:self.angle_index+1] + deviations

        return individual_input, context, mask_input, env_input

    def __repr__(self):
        return self.__class__.__name__ + '(angle_index={}, sigma={} ({} degrees))'.\
            format(self.angle_index, self.sigma, self.sigma * 180.0 / np.pi)
