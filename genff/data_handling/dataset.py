import numpy as np
import torch
import json
import os
import deepdish as dd
from typing import Union, List
from torch.utils.data import Dataset
from genff.data_handling.transform import RectangularAngle


class GroupDataset(Dataset):
    """
    Simulated dataset with position only. The data is centered by default.

    Each example corresponds to a 3-element tuple with:
    - individual: 1 x 3 matrix
    - context: P x 3 matrix
    - mask: P vector
    where the individual are the features for 1 person in the group, and context are the features for all other members.
    Because we might have different number of members per group, the tuple also includes a mask vector with 1's for
    the valid people in the context.
    """

    def __init__(self, input_file: Union[str, List[str]], rect_repr: bool = False, include_centers: bool = False) -> None:
        """
        Constructor
        :param input_file: path to data or list of paths (when loading multiple datasets simultaneously)
        :param rect_repr: output examples in rectangular angle representation?
        """
        super(GroupDataset, self).__init__()

        self.num_features = 3   # we assume that we have this many features: x, y, theta (although output is 4D)
        self.rect_repr = rect_repr
        self.transform = RectangularAngle(2)
        self.individual = None  # individual per example
        self.context = None     # context per example
        self.center = None      # center of context (in global coordinates) per example
        self.mask = None        # mask per example
        self.stamps = []        # stamps for the examples
        self.num_examples = 0   # number of examples

        self.env = None         # cropped maps
        self.env_key = []       # keys for full maps per example
        self.env_dict = {}      # full maps
        self.grid_w = None      # width for cropped maps (depends on the resolution)
        self.resolution = None  # resolution for the full maps

        self.include_centers = include_centers

        if isinstance(input_file, str):
            input_file = [input_file]
        self.load_data(input_file)

    def load_data(self, input_file: List[str]) -> None:
        """
        Helper function to load the data from disk
        :param input_file: list of files to load data from (they are expected to be h5 files generated with deepdish)
        """
        # Load environment images

        env = []       # cropped maps
        self.env_key = []   # key for full map in env_dict
        self.env_dict = {}  # full environment maps
        self.grid_w = None  # grid width -- should be the same for all datasets that are loaded here!
        self.resolution = None  # full map resolution
        num_people = 0      # maximum number of people in all of the datasets
        self.num_examples = 0

        for fp in input_file:
            print("Loading "+fp)
            b = os.path.basename(fp)  # key for the full environment

            # load cropped map width
            cwidth = dd.io.load(fp, "/cwidth")
            if self.grid_w is None:
                self.grid_w = cwidth
            else:
                # check that all of the datasets have consistent dimensions for the cmaps
                assert cwidth == self.grid_w, "Expected the cropped maps in {} to have width={}".format(fp, self.grid_w)

            # load cropped maps
            cmaps = dd.io.load(fp, "/cmaps")
            env.append(cmaps)

            # load full map
            self.env_key.extend([b for _ in range(len(cmaps))])
            self.env_dict[b] = dd.io.load(fp, "/env")

            res = self.env_dict[b]['resolution']
            if self.resolution is None:
                self.resolution = res
            else:
                # check that the resolution is consistent across maps
                assert res == self.resolution, \
                    "Expected the map resolution in {} to be {} (but got {})".format(fp, self.resolution, res)

            # load num people
            groups = dd.io.load(fp, "/groups")
            self.num_examples += len(groups)
            max_people = np.max([x[1] for x in groups])
            if max_people > num_people:
                num_people = max_people

        self.env = torch.tensor(np.vstack(env))

        # sanity check
        assert num_people is not None, \
            "Failed to load data from file ({}). Could not get max. number of people.".format(input_file)

        # initialize vars
        self.individual = torch.zeros((self.num_examples, 1, self.num_features))
        self.context = torch.zeros((self.num_examples, num_people-1, self.num_features))
        self.center = torch.zeros((self.num_examples, 1, 2))
        self.mask = torch.zeros((self.num_examples, num_people-1))

        # do a second pass to load up the data
        counter = 0
        for fp in input_file:
            groups = dd.io.load(fp, "/groups")
            if "centers" in dd.io.load(fp):
                centers = dd.io.load(fp, "/centers")
            else:
                centers = np.zeros((len(groups), 1, 2)).tolist()

            for i in range(len(groups)):
                n = groups[i][1]
                features = groups[i][2:]
                matrix = np.reshape(np.array(features), (n, self.num_features), order='C')

                self.stamps.append(groups[i][0])
                self.individual[counter, 0:1, :] = torch.tensor(matrix[0:1, :])
                self.context[counter, :n - 1, :] = torch.tensor(matrix[1:, :])
                self.mask[counter, :n - 1] = torch.full((n - 1,), 1.0)
                self.center[counter, :, :] = torch.tensor(centers[i])
                counter += 1

        # one more sanity check
        assert counter == self.num_examples, \
            "Expected to load {} examples but got {} from {}.".format(self.num_examples, counter, input_file)
        assert self.env.shape[0] == self.num_examples, \
            "Expected there to be as many cropped maps as group examples, but got {} vs {}".format(self.env.shape[0],
                                                                                                   self.num_examples)

    def __len__(self):
        """
        Return the size of the dataset
        :return: number of examples
        """
        return self.num_examples

    def __getitem__(self, idx):
        """
        Get ith example from the dataset
        :param idx: example index
        :return: tuple with individual tensor, context tensor, and mask tensor
        """
        ind = self.individual[idx, :, :]
        con = self.context[idx, :, :]
        mas = self.mask[idx, :]
        env = self.env[idx, :, :, :]

        if self.include_centers:
            cent = self.center[idx, :, :].squeeze(0)

        if self.rect_repr:
            ind, con, mas, env = self.transform((ind.unsqueeze(0), con.unsqueeze(0),
                                                 mas.unsqueeze(0), env.unsqueeze(0)))

        if self.include_centers:
            return ind, con, mas, env, cent
        else:
            return ind, con, mas, env
