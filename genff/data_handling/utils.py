"""Utility methods/functions for data handling"""
import numpy as np
import math
import cv2
import torch
from torch.utils.data import Dataset
from torch.utils.data.sampler import WeightedRandomSampler
import typing
from typing import List, Tuple


def moveAngleToPIRange(a):
    """
    Helper function to move an angle to the [-pi, pi) range
    :param a: input angle (in radians)
    :return: transformed angle
    """
    result = a
    if a >= 2 * np.pi:
        result = a - np.floor(a / (2 * np.pi)) * 2 * np.pi
    elif a <= -2 * np.pi:
        result = a + np.floor(-a / (2 * np.pi)) * 2 * np.pi

    if result <= -np.pi:
        result = 2 * np.pi + result
    elif result > np.pi:
        result = -2 * np.pi + result

    return result


def minAngleDiff(a1, a2):
    """
    Helper function to compute the min. angle difference: a1 - a2
    :param a1: input angle (in radians)
    :param a2: input angle (in radians)
    :return: difference
    """
    a1 = moveAngleToPIRange(a1)
    a2 = moveAngleToPIRange(a2)
    d = a1 - a2
    if d > np.pi:
        d = d - 2*np.pi
    elif d < -np.pi:
        d = d + 2*np.pi
    return d


def identify_group_sizes(dataset: Dataset) -> List[int]:
    """
    Helper function to identify the group sizes in a dataset
    :param dataset: torch dataset
    :return List with unique group sizes
    """
    group_sizes = []
    for sample in dataset:
        ind, con, mas, env = sample
        group_sizes.append(mas.sum().item())  # size is given by the number of people in the context

    group_sizes = sorted([int(x) for x in list(set(group_sizes))])  # unique group sizes
    return group_sizes


def compute_group_size_count(dataset: Dataset, group_sizes: List[int]) -> Tuple[List[float], List[int]]:
    """
    Helper function to compute the prob of a group size (and the number of samples per group size)
    :param dataset: torch dataset
    :param group_sizes: list with the group sizes (as output by identify_group_sizes())
    :return tuple with list of prob and counts per group size
    """
    num = [0 for _ in range(len(group_sizes))]
    for sample in dataset:
        ind, con, mas, env = sample
        s = int(mas.sum().item())  # size is given by the number of people in the context
        c = group_sizes.index(s)   # should never through ValueError exception
        num[c] += 1                # increase count for given class
    prob = [x / len(dataset) for x in num]  # probability of class 1 = 0.7, of 2 = 0.3 etc
    return prob, num


def create_weighted_sampler(dataset: Dataset) -> WeightedRandomSampler:
    """
    Create weighted sampler in case we wanted to make the distribution of samples per group size more equal
    :param dataset: torch dataset
    :return WeightedRandomSampler with the right weights based on the data loaded into this dataset
    """
    # step 1. Identify group sizes in the dataset
    group_sizes = identify_group_sizes(dataset)

    # step 2. compute class prob -- each class corresponds to a group size
    prob, _ = compute_group_size_count(dataset, group_sizes)

    # step 3. compute sample weight
    reciprocal_weights = torch.zeros(len(dataset), dtype=torch.float)
    for index in range(len(dataset)):
        ind, con, mas, env = dataset[index]
        s = int(mas.sum().item())  # size is given by the number of people in the context
        c = group_sizes.index(s)   # should never through ValueError exception
        reciprocal_weights[index] = prob[c]

    weights = 1.0 / reciprocal_weights
    sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(dataset))
    return sampler