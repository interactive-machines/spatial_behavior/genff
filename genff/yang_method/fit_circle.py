"""
Functions to fit a circlular F-formation to an individual or group of interactants.
Implements method proposed by Yang et al. in a 2017 paper:
    @INPROCEEDINGS{8206105,
        author={S. {Yang} and E. {Gamborino} and C. {Yang} and L. {Fu}},
        booktitle={2017 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
        title={A study on the social acceptance of a robot in a multi-human interaction
               using an F-formation based motion model},
        year={2017},
        volume={},
        number={},
        pages={2766-2771},
        doi={10.1109/IROS.2017.8206105}}
"""

from typing import Tuple
import numpy as np
from scipy import special


def fit_circle_individual(x: float, y: float, theta: float,
                          ospace_dist: float=0.72) -> Tuple[Tuple[float, float], float]:
    """
    Fit circle when there's a single individual. The circle is placed forward from the individual.
    :param x: x coordinate for the individual
    :param y: y coordinate for the individual
    :param theta: angle for the individual (in radians)
    :param ospace_dist: distance from the individual to the o-space center
    :return: center (x,y), inner_radius, outer_radius
    """
    center_x = x + np.cos(theta)*ospace_dist
    center_y = y + np.sin(theta)*ospace_dist
    radius = ospace_dist
    return (center_x, center_y), radius


def fit_circle_pair(points_1: np.ndarray, points_2: np.ndarray,
                    ospace_dist: float=0.72) -> Tuple[Tuple[float, float], float]:
    """
    Fits circle between a pair of people.
    :param x: x coordinate for the individual
    :param y: y coordinate for the individual
    :return: center (xc,yc), radius
    """
    (x_1, y_1, theta_1) = points_1
    (x_2, y_2, theta_2) = points_2

    # fallback: if thetas are equal, set radius to default O-space parameter
    if theta_1 == theta_2:
        radius = ospace_dist
    else:
        # find radius of circular space between pair
        if x_1 == x_2:
            radius = (y_2 - y_1) / (np.sin(theta_1) - np.sin(theta_2))
        else:
            radius = (x_2 - x_1) / (np.cos(theta_1) - np.cos(theta_2))

    # find center of pair
    center_x = x_1 + radius * np.cos(theta_1)
    center_y = y_1 + radius * np.sin(theta_1)

    return (center_x, center_y), radius


def fit_circle_multiple(points: np.ndarray,
                        verbose: bool=False) -> Tuple[Tuple[float, float], float, float]:
    """
    Fit circle for multiple people. The circle is centered among them.
    :param points: 2xN matrix with N 2D points (and N >= 3)
    :return: center (x,y), inner_radius, outer_radius
    """
    N = len(points)
    # find the center between each possible pair in the context
    sum_center = [0,0]
    for i, p_1 in enumerate(points):
        for p_2 in points[i+1:]:
            # print("pairing:", p1,p2)
            # get this center
            center, radius = fit_circle_pair(p_1, p_2)
            # print("this center", center)
            if verbose:
                print(f'Pairing: {p_1, p_2}, pair center: {center}, pair radius: {radius}')
            # update center accumulation for average
            sum_center[0] += center[0]
            sum_center[1] += center[1]

    # divide by N to get average
    n_centers = special.binom(N, 2)
    center = (sum_center[0] / n_centers, sum_center[1] / n_centers)

    # initialize radii for finding max/min
    inner_radius = float('inf')
    outer_radius = float('-inf')
    # get distance between each person and center;
    # minimum distance will be inner_radius, max distance will be outer_radius
    for person in points:
        # assuming person[0] = x, person[1] = y
        distance = np.sqrt((person[0]-center[0])**2 + (person[1]-center[1])**2)

        if distance < inner_radius:
            inner_radius = distance
        if distance > outer_radius:
            outer_radius = distance

    return center, inner_radius, outer_radius
