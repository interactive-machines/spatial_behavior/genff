"""
Functions to implement the F-formation estimation and pose generation method proposed by
Yang et al. in a 2017 paper:
    @INPROCEEDINGS{8206105,
        author={S. {Yang} and E. {Gamborino} and C. {Yang} and L. {Fu}},
        booktitle={2017 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
        title={A study on the social acceptance of a robot in a multi-human interaction
               using an F-formation based motion model},
        year={2017},
        volume={},
        number={},
        pages={2766-2771},
        doi={10.1109/IROS.2017.8206105}}
"""

import math
from typing import Tuple
import numpy as np
import torch
from genff.yang_method.fit_circle import fit_circle_individual, fit_circle_multiple
from genff.opt_method.opt_generator import torch_to_numpy


def fit_circle_to_context(context: np.ndarray,
                          x_index: int=0, y_index: int=1, angle_index: int=2,
                          ospace_dist: float=0.72) -> Tuple[Tuple[float, float], float, float]:
    """
    Helper function to fit circle based on the number of people in the context
    :param context: NxD valid context
    :param x_index: : index of the x coordinates
    :param y_index: index of the y coordinates
    :param angle_index: index of the person's orientation
    :param ospace_dist: default distance for o-space in case the number of people is 1
    return circle center, radius
    """
    N = context.shape[0]
    if N == 1:
        # There's a single individual in the group. We fit a circle
        center, radius = fit_circle_individual(context[0, x_index],
                                               context[0, y_index],
                                               context[0, angle_index],
                                               ospace_dist=ospace_dist)
        # inner and outer radii will be equal
        inner_radius = radius
        outer_radius = radius
    else:
        # Fit a circular area with inner and outer radii
        center, inner_radius, outer_radius = fit_circle_multiple(context)

    return center, inner_radius, outer_radius


def find_neighbors(context: np.ndarray, center: np.ndarray) -> Tuple[np.ndarray, np.ndarray, float]:
    """
    Helper function to find the greatest gap between two individuals in a valid context
    :param context: Nx3 matrix of the individuals in a context
    :return: left and right neighbors and angle between them
    """
    N = context.shape[0]
    left_idx = 0
    right_idx = 0
    if N == 1:
        greatest_angle = 2 * math.pi
    else:
        for member in context:
            member[2] = math.atan2(member[1] - center[1], member[0] - center[0])

        context = context[np.argsort(context[:,2])]
        greatest_angle = 0
        for i in range(0, N):
            if context[(i + 1) % N][2] < context[i][2]: # case right value wraps around
                candidate_angle = (2 * math.pi + context[(i + 1) % N][2]) - context[i][2]
            else:
                candidate_angle = context[(i + 1) % N][2] - context[i][2]

            if candidate_angle > greatest_angle:
                left_idx = i
                right_idx = (i + 1) % N
                greatest_angle = candidate_angle

    return context[left_idx], context[right_idx], greatest_angle


def insertion(center: Tuple[float, float], radius: float,
              left: np.ndarray, right: np.ndarray,
              complement: bool) -> np.ndarray:
    """
    Helper function to determine the optimal placement for a new member within F-formation
    :param center: x and y coordinates for the center of the F-formation
    :param radius: distance of the new member from the center of the F-formation
    :param left: 1x3 vector of the group member to the new member's left
    :param right: 1x3 vector of the group member to the new member's right
    :param complement: Boolean value of whether to take the complement of the law of cosines
    """
    # special case: only one previous individual
    if np.array_equal(left, right):
        new_orient = left[2]
    else:
        # find radii of left and right, distance between left and right
        radius_l = math.sqrt((left[0] - center[0]) ** 2 + (left[1] - center[1]) ** 2)
        radius_r = math.sqrt((right[0] - center[0]) ** 2 + (right[1] - center[1]) ** 2)
        dist_lr = math.sqrt((left[0] - right[0]) ** 2 + (left[1] - right[1]) ** 2)

        # law of cosines
        angle_bisect = math.acos((radius_l ** 2 + radius_r ** 2 - \
            dist_lr ** 2) / (2 * radius_l * radius_r))
        if complement:
            angle_bisect = 2 * math.pi - angle_bisect
        angle_bisect /= 2
        new_orient = left[2] + angle_bisect

    # find Cartesian coordinates
    new_x = radius * math.cos(new_orient) + center[0]
    new_y = radius * math.sin(new_orient) + center[1]

    return np.array([[new_x, new_y, new_orient + math.pi]])


def yang_generator(context: torch.Tensor, mask: torch.Tensor=None,
                   ospace_dist: float=0.72) -> np.ndarray:
    """
    Add a new member to a context
    :param context: Nx3 matrix of the individuals in the context
    :param mask: 1xN vector of the context mask
    :param x_index: index of the x coordinate
    :param y_index: index of the y coordinate
    :param angle_index: index of the theta coordinate
    :param ospace_dist: default distance for the O-space radius
    :return: 1x3 vector of the agent's x and y coordinates and orientation
    """
    # apply mask to context, convert tensor to NumPy array
    if mask is not None:
        context_filtered = context[mask.bool(), :]
        context_filtered = torch_to_numpy(context_filtered)
    else:
        context_filtered = torch_to_numpy(context)

    # assumption: always place the new member in the middle of the existing p-space,
    (center, inner_radius, outer_radius) = fit_circle_to_context(context_filtered, ospace_dist=ospace_dist)
    radius = (inner_radius + outer_radius) / 2

    # assumption: always place the new member in the largest gap
    (left, right, gap) = find_neighbors(context_filtered.copy(), center)
    individual = insertion(center, radius, left, right, gap > math.pi)
    return individual


# for debugging
def test_circle_fit(context: np.ndarray) -> Tuple[Tuple[float, float], float, float]:
    """
    Dummy function to return the o-space center and p-space radii for a valid context
    :param context: Nx3 matrix representing a valid context
    :return: o-space center, p-space inner radius, p-space outer radius
    """
    center, inner_radius, outer_radius = fit_circle_to_context(context)
    return center, inner_radius, outer_radius
