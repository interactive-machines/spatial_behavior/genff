OUTPUT_DIR="."

SCENES=(
'Benevolence_0_int'
'Benevolence_1_int'
'Benevolence_2_int'
'Merom_0_int'
'Merom_1_int'
'Beechwood_0_int'
'Beechwood_1_int'
'Ihlen_0_int'
'Ihlen_1_int'
'Wainscott_0_int'
'Wainscott_1_int'
'Pomaria_0_int'
'Pomaria_1_int'
'Pomaria_2_int'
'Rs_int')

if [ $# -gt 0 ]; then
  OUTPUT_DIR=$1
fi
export OUTPUT_DIR

echo "The traversability maps will be saved in '$OUTPUT_DIR'"

process_scene () {
  scene=$1
  #OUTPUT_DIR=$2

  SCENE_DIR=${OUTPUT_DIR}/${scene}
  mkdir -p $SCENE_DIR

  LOG_DIR="${SCENE_DIR}/trav.log"

  echo "Processing '$SCENE_DIR'..."
  python generate_trav_maps.py --output-folder $SCENE_DIR --source IG $scene 1>$LOG_DIR 2>&1
}

export -f process_scene
printf '%s\n' "${SCENES[@]}" | xargs -P 5 -I {} bash -c 'process_scene {}'
