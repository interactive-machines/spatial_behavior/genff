# Open iGibson scene (useful for exploring the environment)
from gibson2.scenes.igibson_indoor_scene import InteractiveIndoorScene
from gibson2.simulator import Simulator
import numpy as np
import signal
import sys

def main():

    if len(sys.argv) < 2:
        print("Run as: python visualize_scene.py <scene_name>")
        sys.exit(1)
    scene_name = sys.argv[1]
    print("Opening {}".format(scene_name))

    s = Simulator(mode='gui', image_width=512, image_height=512, device_idx=0)
    scene = InteractiveIndoorScene(scene_name, texture_randomization=False, object_randomization=False)
    s.import_ig_scene(scene)

    np.random.seed(0)
    for _ in range(10):
        pt = scene.get_random_point_by_room_type('living_room')[1]
        print('random point in living_room', pt)

    signal.signal(signal.SIGINT, signal.default_int_handler)

    try:
        while True:
            s.step()
    except KeyboardInterrupt:
        s.disconnect()

    sys.exit(0)

if __name__ == '__main__':
    main()
