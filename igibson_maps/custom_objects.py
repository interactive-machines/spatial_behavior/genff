import xml.etree.ElementTree as ET
import time
import os
import random

from gibson2.utils import assets_utils
from gibson2.scenes.igibson_indoor_scene import InteractiveIndoorScene, SCENE_SOURCE
import gibson2
from gibson2.scenes.gibson_indoor_scene import StaticIndoorScene
import logging
import numpy as np

class CustomInteractiveIndoorScene(InteractiveIndoorScene):
    def __init__(self,
                 scene_id,
                 trav_map_resolution=0.1,
                 trav_map_erosion=2,
                 trav_map_type='with_obj',
                 build_graph=True,
                 num_waypoints=10,
                 waypoint_resolution=0.2,
                 pybullet_load_texture=False,
                 texture_randomization=False,
                 link_collision_tolerance=0.03,
                 object_randomization=False,
                 object_randomization_idx=None,
                 should_open_all_doors=False,
                 load_object_categories=None,
                 load_room_types=None,
                 load_room_instances=None,
                 seg_map_resolution=0.1,
                 scene_source="IG",
                 no_walls=False
                 ):
        """
        :param scene_id: Scene id
        :param trav_map_resolution: traversability map resolution
        :param trav_map_erosion: erosion radius of traversability areas, should be robot footprint radius
        :param trav_map_type: type of traversability map, with_obj | no_obj
        :param build_graph: build connectivity graph
        :param num_waypoints: number of way points returned
        :param waypoint_resolution: resolution of adjacent way points
        :param pybullet_load_texture: whether to load texture into pybullet. This is for debugging purpose only and does not affect robot's observations
        :param texture_randomization: whether to randomize material/texture
        :param link_collision_tolerance: tolerance of the percentage of links that cannot be fully extended after object randomization
        :param object_randomization: whether to randomize object
        :param object_randomization_idx: index of a pre-computed object randomization model that guarantees good scene quality
        :param should_open_all_doors: whether to open all doors after episode reset (usually required for navigation tasks)
        :param load_object_categories: only load these object categories into the scene (a list of str)
        :param load_room_types: only load objects in these room types into the scene (a list of str)
        :param load_room_instances: only load objects in these room instances into the scene (a list of str)
        :param seg_map_resolution: room segmentation map resolution
        :param scene_source: source of scene data; among IG, CUBICASA, THREEDFRONT
        """

        StaticIndoorScene.__init__(self,
            scene_id,
            trav_map_resolution,
            trav_map_erosion,
            trav_map_type,
            build_graph,
            num_waypoints,
            waypoint_resolution,
            pybullet_load_texture
        )
        self.texture_randomization = texture_randomization
        self.object_randomization = object_randomization
        self.should_open_all_doors = should_open_all_doors
        if object_randomization:
            if object_randomization_idx is None:
                fname = scene_id
            else:
                fname = '{}_random_{}'.format(scene_id,
                                              object_randomization_idx)
        else:
            fname = '{}_best'.format(scene_id)
        if scene_source not in SCENE_SOURCE:
            raise ValueError(
                'Unsupported scene source: {}'.format(scene_source))
        if scene_source == "IG":
            scene_dir = assets_utils.get_ig_scene_path(scene_id)
        elif scene_source == "CUBICASA":
            scene_dir = assets_utils.get_cubicasa_scene_path(scene_id)
        else:
            scene_dir = assets_utils.get_3dfront_scene_path(scene_id)
        self.scene_source = scene_source
        self.scene_dir = scene_dir
        self.scene_file = os.path.join(
            scene_dir, "urdf", "{}.urdf".format(fname))
        self.scene_tree = ET.parse(self.scene_file)
        self.first_n_objects = np.inf
        self.random_groups = {}
        self.objects_by_category = {}
        self.objects_by_name = {}
        self.objects_by_id = {}
        self.category_ids = assets_utils.get_ig_category_ids()

        # Current time string to use to save the temporal urdfs
        timestr = time.strftime("%Y%m%d-%H%M%S")
        # Create the subfolder
        self.scene_instance_folder = os.path.join(
            gibson2.ig_dataset_path, "scene_instances",
            '{}_{}_{}'.format(timestr, random.getrandbits(64), os.getpid()))
        os.makedirs(self.scene_instance_folder, exist_ok=True)

        # Load room semantic and instance segmentation map
        self.load_room_sem_ins_seg_map(seg_map_resolution)

        # Decide which room(s) and object categories to load
        self.filter_rooms_and_object_categories(
            load_object_categories, load_room_types, load_room_instances)

        # Load average object density if exists
        self.avg_obj_dims = self.load_avg_obj_dims()

        # load overlapping bboxes in scene annotation
        self.overlapped_bboxes = self.load_overlapped_bboxes()

        # percentage of objects allowed that CANNOT extend their joints by >66%
        self.link_collision_tolerance = link_collision_tolerance

        # Parse all the special link entries in the root URDF that defines the scene
        for link in self.scene_tree.findall('link'):
            if 'category' in link.attrib:
                # Extract category and model from the link entry
                category = link.attrib["category"]
                model = link.attrib["model"]

                # An object can in multiple rooms, seperated by commas,
                # or None if the object is one of the walls, floors or ceilings
                in_rooms = link.attrib.get('room', None)
                if in_rooms is not None:
                    in_rooms = in_rooms.split(',')

                # Find the urdf file that defines this object
                if category in ["walls", "floors", "ceilings"]:
                    if no_walls:
                        continue

                    model_path = self.scene_dir
                    filename = os.path.join(
                        model_path, "urdf", model + "_" + category + ".urdf")

                # For other objects
                else:
                    # This object does not belong to one of the selected object categories, skip
                    if self.load_object_categories is not None and \
                            category not in self.load_object_categories:
                        continue
                    # This object is not located in one of the selected rooms, skip
                    if self.load_room_instances is not None and \
                            len(set(self.load_room_instances) & set(in_rooms)) == 0:
                        continue

                    category_path = assets_utils.get_ig_category_path(category)
                    assert len(os.listdir(category_path)) != 0, \
                        "There are no models in category folder {}".format(
                            category_path)

                    if model == 'random':
                        # Using random group to assign the same model to a group of objects
                        # E.g. we want to use the same model for a group of chairs around the same dining table
                        if "random_group" in link.attrib:
                            # random_group is a unique integer within the category
                            random_group = link.attrib["random_group"]
                            random_group_key = (category, random_group)

                            # if the model of this random group has already been selected
                            # use that model.
                            if random_group_key in self.random_groups:
                                model = self.random_groups[random_group_key]

                            # otherwise, this is the first instance of this random group
                            # select a random model and cache it
                            else:
                                model = random.choice(
                                    os.listdir(category_path))
                                self.random_groups[random_group_key] = model
                        else:
                            # Using a random instance
                            model = random.choice(os.listdir(category_path))
                    else:
                        model = link.attrib['model']

                    model_path = assets_utils.get_ig_model_path(category, model)
                    filename = os.path.join(model_path, model + ".urdf")

                if "bounding_box" in link.keys() and "scale" in link.keys():
                    logging.error(
                        "You cannot define both scale and bounding box size to embed a URDF")
                    exit(-1)

                bounding_box = None
                scale = None
                if "bounding_box" in link.keys():
                    bounding_box = np.array(
                        [float(val) for val in link.attrib["bounding_box"].split(" ")])
                elif "scale" in link.keys():
                    scale = np.array([float(val)
                                      for val in link.attrib["scale"].split(" ")])
                else:
                    scale = np.array([1., 1., 1.])

                object_name = link.attrib['name']

                # The joint location is given wrt the bounding box center but we need it wrt to the base_link frame
                joint_connecting_embedded_link = \
                    [joint for joint in self.scene_tree.findall("joint")
                     if joint.find("child").attrib["link"]
                     == object_name][0]

                joint_xyz = np.array([float(val) for val in joint_connecting_embedded_link.find(
                    "origin").attrib["xyz"].split(" ")])
                joint_type = joint_connecting_embedded_link.attrib['type']
                if 'rpy' in joint_connecting_embedded_link.find("origin").attrib:
                    joint_rpy = \
                        np.array([float(val) for val in
                                  joint_connecting_embedded_link.find("origin").attrib["rpy"].split(" ")])
                else:
                    joint_rpy = np.array([0., 0., 0.])

                joint_name = joint_connecting_embedded_link.attrib['name']
                joint_parent = joint_connecting_embedded_link.find(
                    "parent").attrib["link"]

                self.add_object(category,
                                model=model,
                                model_path=model_path,
                                filename=filename,
                                bounding_box=bounding_box,
                                scale=scale,
                                object_name=object_name,
                                joint_type=joint_type,
                                position=joint_xyz,
                                orientation_rpy=joint_rpy,
                                joint_name=joint_name,
                                joint_parent=joint_parent,
                                in_rooms=in_rooms)
            elif link.attrib["name"] != "world":
                logging.error(
                    "iGSDF should only contain links that represent embedded URDF objects")


class Plane(object):
    def __init__(self, orig, normal):
        self.orig = orig
        self.n = normal / np.linalg.norm(normal)

    def __str__(self):
        return 'plane(o=%s, n=%s)' % (self.orig, self.n)
