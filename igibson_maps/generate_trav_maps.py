#!/usr/bin/env python
"""
script to generate all traversability maps:

for file in ../../gibson2/ig_dataset/scenes/*
  python generate_trav_map.py $(basename $file)

to generate traversability maps for cubicasa5k or 3dfront:
pass in additional flag --source CUBICASA or --source THREEDFRONT

NOTE: This is a modified version of https://github.com/StanfordVL/iGibson/blob/master/gibson2/utils/generate_trav_map.py
"""

import os
import sys
import random
import argparse

from gibson2.simulator import Simulator
from gibson2.utils import assets_utils
from gibson2.scenes.igibson_indoor_scene import InteractiveIndoorScene, SCENE_SOURCE

import numpy as np
from tqdm import tqdm
import cv2
from PIL import Image
from scipy.spatial import ConvexHull

from custom_objects import Plane, CustomInteractiveIndoorScene

# from IPython import embed


def get_xy_floors(vertices, faces, dist_threshold=-0.98):
    z_faces = []
    z = np.array([0, 0, 1])
    faces_selected = []
    for face in tqdm(faces):
        normal = np.cross(
            vertices[face[2]] - vertices[face[1]], vertices[face[1]] - vertices[face[0]])
        dist = np.dot(normal, z) / np.linalg.norm(normal)
        if (dist_threshold is None) or ((dist_threshold is not None) and (dist < dist_threshold)):
            z_faces.append(vertices[face[0]][2])  # z value for first point in triangle
            faces_selected.append(face)

    return np.array(z_faces), vertices, faces_selected


def gen_trav_map(vertices, faces, output_folder, objects_only=False, add_clutter=False,
                 trav_map_filename_format='floor_trav_{}.png',
                 obstacle_map_filename_format='floor_{}.png', max_length=None):
    """
    Generate traversability maps.
    """
    floors = [0.0]
    planes = list([x / 100.0 for x in range(20, 150, 4)])

    z_faces, vertices, faces_selected = get_xy_floors(vertices, faces, dist_threshold=None)

    if max_length is None:
        xmin, ymin, _ = vertices.min(axis=0)
        xmax, ymax, _ = vertices.max(axis=0)

        max_length = np.max([np.abs(xmin), np.abs(ymin),
                             np.abs(xmax), np.abs(ymax)])
        max_length = np.ceil(max_length).astype(int)

    print("gen_trav_map MAXLENGTH={}".format(max_length))

    wall_maps = gen_map(vertices, faces_selected, output_folder,
                        img_filename_format=obstacle_map_filename_format,
                        floors=planes, max_length=max_length)  # plane heights to intersect map against

    for i_floor in range(len(floors)):

        if objects_only:
            wall_map = wall_maps[0] / 255
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
            wall_map = cv2.erode(wall_map, kernel, iterations=3)
            wall_map = cv2.dilate(wall_map, kernel, iterations=3)
            wall_map = cv2.erode(wall_map, kernel, iterations=3)
            wall_map = cv2.dilate(wall_map, kernel, iterations=3)
            wall_map = cv2.erode(wall_map, kernel, iterations=3)  # do this twice to try to fill in objects
            erosion = cv2.dilate(wall_map, kernel, iterations=2)
            # if cv2.__version__.startswith("3."):
            #     _, contours, _ = cv2.findContours((wall_map == 0).astype(np.uint8), cv2.RETR_EXTERNAL,
            #                                       cv2.CHAIN_APPROX_SIMPLE)
            # else:
            #     contours, _ = cv2.findContours((wall_map == 0).astype(np.uint8), cv2.RETR_EXTERNAL,
            #                                    cv2.CHAIN_APPROX_SIMPLE)
            # draw the contours on the empty image
            # cv2.fillPoly(wall_map_hull, contours, 255)
            # cv2.drawContours(wall_map_hull, contours, -1, 255, cv2.FILLED)
            # cv2.namedWindow("wall_map_hull", cv2.WINDOW_NORMAL)
            # cv2.imshow("wall_map_hull", wall_map_hull)
            # cv2.waitKey(0)

        else:
            wall_pts = np.array(np.where(wall_maps[i_floor] == 0)).T
            wall_convex_hull = ConvexHull(wall_pts)
            wall_map_hull = np.zeros(wall_maps[i_floor].shape).astype(np.uint8)
            cv2.fillPoly(wall_map_hull,
                         [wall_convex_hull.points[wall_convex_hull.vertices][:, ::-1].reshape((-1, 1, 2)).astype(
                             np.int32)], 255)

            floor = floors[i_floor]
            mask = (np.abs(z_faces - floor) < 0.001)
            faces_new = np.array(faces_selected)[mask, :]

            t = (vertices[faces_new][:, :, :2] + max_length) * 100
            t = t.astype(np.int32)

            floor_map = np.zeros((2 * max_length * 100, 2 * max_length * 100))  # same size as wall map

            cv2.fillPoly(floor_map, t, 1)

            if add_clutter is True:  # Build clutter map
                mask1 = np.logical_and(((z_faces - floor) < 2.0), ((z_faces - floor) > 0.05))  # used to be 0.05
                faces_new1 = np.array(faces_selected)[mask1, :]

                t1 = (vertices[faces_new1][:, :, :2] + max_length) * 100
                t1 = t1.astype(np.int32)

                clutter_map = np.zeros(
                    (2 * max_length * 100, 2 * max_length * 100))
                cv2.fillPoly(clutter_map, t1, 1)
                floor_map = np.float32((clutter_map == 0) * (floor_map == 1))

            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
            erosion = cv2.erode(floor_map, kernel, iterations=2)
            erosion = cv2.dilate(erosion, kernel, iterations=2)
            wall_map = wall_maps[i_floor] / 255
            wall_map = cv2.erode(wall_map, kernel, iterations=2)
            wall_map = cv2.dilate(wall_map, kernel, iterations=2)
            wall_map = cv2.erode(wall_map, kernel, iterations=2)
            wall_map = cv2.dilate(wall_map, kernel, iterations=2)
            wall_map = cv2.erode(wall_map, kernel, iterations=2)  # do this three times to try to close gaps
            wall_map = cv2.dilate(wall_map, kernel, iterations=1)
            erosion[wall_map == 0] = 0
            erosion[wall_map_hull == 0] = 0  # crop using convex hull

        cur_img = Image.fromarray((erosion * 255).astype(np.uint8))
        cur_img.save(os.path.join(output_folder, trav_map_filename_format.format(i_floor)))

    return max_length

def point_to_plane_dist(p, plane):
    return np.dot((p - plane.orig), plane.n)


INTERSECT_EDGE = 0
INTERSECT_VERTEX = 1


def compute_triangle_plane_intersections(vertices, faces, tid, plane, dists, dist_tol=1e-8):
    """
    Compute the intersection between a triangle and a plane
    Returns a list of intersections in the form
    (INTERSECT_EDGE, <intersection point>, <edge>) for edges intersection
    (INTERSECT_VERTEX, <intersection point>, <vertex index>) for vertices
    This return between 0 and 2 intersections :
    - 0 : the plane does not intersect the plane
    - 1 : one of the triangle's vertices lies on the plane (so it just
    "touches" the plane without really intersecting)
    - 2 : the plane slice the triangle in two parts (either vertex-edge,
    vertex-vertex or edge-edge)
    """

    # TODO: Use an edge intersection cache (we currently compute each edge
    # intersection twice : once for each tri)

    # This is to avoid registering the same vertex intersection twice
    # from two different edges
    vert_intersect = {vid: False for vid in faces[tid]}

    # Iterate through the edges, cutting the ones that intersect
    intersections = []
    for e in ((faces[tid][0], faces[tid][1]),
              (faces[tid][0], faces[tid][2]),
              (faces[tid][1], faces[tid][2])):
        v1 = vertices[e[0]]
        d1 = dists[e[0]]
        v2 = vertices[e[1]]
        d2 = dists[e[1]]

        if np.fabs(d1) < dist_tol:
            # Avoid creating the vertex intersection twice
            if not vert_intersect[e[0]]:
                # point on plane
                intersections.append((INTERSECT_VERTEX, v1, e[0]))
                vert_intersect[e[0]] = True
        if np.fabs(d2) < dist_tol:
            if not vert_intersect[e[1]]:
                # point on plane
                intersections.append((INTERSECT_VERTEX, v2, e[1]))
                vert_intersect[e[1]] = True

        # If vertices are on opposite sides of the plane, we have an edge
        # intersection
        if d1 * d2 < 0:
            # Due to numerical accuracy, we could have both a vertex intersect
            # and an edge intersect on the same vertex, which is impossible
            if not vert_intersect[e[0]] and not vert_intersect[e[1]]:
                # intersection factor (between 0 and 1)
                # here is a nice drawing :
                # https://ravehgonen.files.wordpress.com/2013/02/slide8.png
                # keep in mind d1, d2 are *signed* distances (=> d1 - d2)
                s = d1 / (d1 - d2)
                vdir = v2 - v1
                ipos = v1 + vdir * s
                intersections.append((INTERSECT_EDGE, ipos, e))

    return intersections


def gen_map(vertices, faces, output_folder, img_filename_format='floor_{}.png', floors=[0.0], max_length=None):

    if max_length is None:
        xmin, ymin, _ = vertices.min(axis=0)
        xmax, ymax, _ = vertices.max(axis=0)

        max_length = np.max([np.abs(xmin), np.abs(ymin),
                             np.abs(xmax), np.abs(ymax)])
        max_length = np.ceil(max_length).astype(np.int)

    print("gen_map MAXLENGTH={}".format(max_length))
    # floors = [0.0]
    print(floors)

    floor_maps = []

    for i_floor, floor in enumerate(floors):
        dists = []
        z = float(floor)  # + 0.5
        cross_section = []
        plane = Plane(np.array([0, 0, z]), np.array([0, 0, 1]))  # origin and normal

        for v in vertices:
            dists.append(point_to_plane_dist(v, plane))

        for i in tqdm(range(len(faces))):
            res = compute_triangle_plane_intersections(vertices, faces,
                                                       i, plane, dists)
            if len(res) == 2:
                cross_section.append((res[0][1], res[1][1]))

        floor_map = np.ones((2 * max_length * 100, 2 * max_length * 100))

        for item in cross_section:
            x1, x2 = (item[0][0] + max_length) * \
                     100, (item[1][0] + max_length) * 100
            y1, y2 = (item[0][1] + max_length) * \
                     100, (item[1][1] + max_length) * 100

            cv2.line(floor_map, (int(x1), int(y1)),
                     (int(x2), int(y2)), color=(0, 0, 0), thickness=2)

        floor_maps.append((floor_map * 255))

    # print("len floor maps: {}".format(len(floor_maps)))
    floor_maps = np.stack(floor_maps, axis=0)
    # print("floor_maps shape: {}".format(floor_maps.shape))
    final_map = np.min(floor_maps, axis=0, keepdims=False)
    # print(final_map.shape)

    cur_img = Image.fromarray(final_map.astype(np.uint8))
    # cur_img = Image.fromarray(np.flipud(cur_img))
    img_filename = img_filename_format.format(0)
    cur_img.save(os.path.join(output_folder, img_filename))

    return [final_map]


def generate_trav_map(scene_name, scene_source, output_folder, objects_only=True, max_length=None):
    if scene_source not in SCENE_SOURCE:
        raise ValueError(
            'Unsupported scene source: {}'.format(scene_source))

    # full list can be obtained via print(assets_utils.get_ig_category_ids())
    small_objects = ['basket', 'bathtub', 'bed', 'bench', 'bottom_cabinet', 'bottom_cabinet_no_top', 'chair', 'chest',
                     'coffee_machine', 'coffee_table', 'cooktop', 'counter', 'console_table', 'crib',
                     'cushion', 'dishwasher', 'floor_lamp', 'guitar', 'laptop', 'loudspeaker',
                     'microwave', 'monitor', 'office_chair', 'oven', 'piano', 'plant', 'pool_table', 'sink', 'sofa',
                     'sofa_chair', 'stool', 'stove', 'table', 'table_lamp', 'toilet', 'trash_can']
    exclude_objects = ['carpet']

    cat_dict = assets_utils.get_ig_category_ids()
    all_categories = set(cat_dict.keys())
    all_categories = all_categories.difference(set(exclude_objects))  # exclude undesired objects
    if objects_only:
        # try only objects
        categories = small_objects
    else:
        categories = list(all_categories.difference(set(small_objects)))

    random.seed(0)
    scene = CustomInteractiveIndoorScene(scene_name,
                                         build_graph=False,
                                         texture_randomization=False,
                                         load_object_categories=categories,
                                         scene_source=scene_source,
                                         no_walls=objects_only)

    s = Simulator(mode='headless', image_width=512,
                  image_height=512, device_idx=0)
    s.import_ig_scene(scene)

    # scene.open_all_doors()

    for i in range(20):
        s.step()

    vertices_info, faces_info = s.renderer.dump()
    s.disconnect()

    if objects_only:
        trav_map_filename_format = 'objects_trav_{}.png'
        obstacle_map_filename_format = 'objects_{}.png'
    else:
        trav_map_filename_format = 'floor_trav_{}.png'
        obstacle_map_filename_format = 'floor_{}.png'

    max_length = gen_trav_map(vertices_info, faces_info,
                              output_folder=os.path.abspath(output_folder),
                              objects_only=objects_only,
                              trav_map_filename_format=trav_map_filename_format,
                              obstacle_map_filename_format=obstacle_map_filename_format,
                              max_length=max_length)

    return max_length


def main():
    parser = argparse.ArgumentParser(
        description='Generate Traversability Map')
    parser.add_argument('scene_names', metavar='s', type=str,
                        nargs='+', help='The name of the scene to process')
    parser.add_argument('--source', dest='source',
                        help='Source of the scene, should be among [CUBICASA, IG, THREEDFRONT]')
    parser.add_argument('--output-folder', default='.',
                        help='Output folder (default: the current directory)')

    args = parser.parse_args()
    for scene_name in args.scene_names:
        # draw full environment without objects
        max_length = generate_trav_map(scene_name, args.source, args.output_folder, objects_only=False)
        # draw objects only
        generate_trav_map(scene_name, args.source, args.output_folder, objects_only=True, max_length=max_length)


if __name__ == "__main__":
    main()
    sys.exit(0)